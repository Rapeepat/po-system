<?php

namespace app\commands;

use yii\console\Controller;
use Yii;
use app\models\User;

class RbacController extends Controller
{

    public function actionAssign($roleName, $username)
    {
        $user = User::find()->where(['username' => $username])->one();
        if (!$user) {
            throw new InvalidParamException("There is no user \"$username\".");
        }

        $auth = Yii::$app->authManager;
        $role = $auth->getRole($roleName);
        if (!$role) {
            throw new InvalidParamException("There is no role \"$roleName\".");
        }

        $auth->assign($role, $user->id);
    }
}
