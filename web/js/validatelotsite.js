function validate_lotsite()
{
    validate=false;
    $('input.form-control').each(function(){
        if($(this).attr('name').indexOf('main_budget') !== -1)
        {
            val=$(this).val();
            if($.isNumeric(val))
            {
                if(val>0) validate = true;
            }
        }
    });
    if(!validate) window.alert('กรุณาใส่งบประมาณอย่างน้อย 1 หน่วยงาน');
    return validate;
}