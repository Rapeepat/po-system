//Calculate vat and price-after-vat for any element with class "gen-vat"
//The value of each element must be numeric.
//Then the function will transform into 3 lines.
//1. original value (price-wo-VAT)
//2. 7% of original value (VAT)
//3. {1.+2.} - (price-after-VAT)
$(window).load(function(){
    $('.gen-vat').each(function(){
        //console.log ($(this).val());
        value = parseFloat($(this).html().replace(',',''));
        vat = value * .07;
        sum = value + vat;
        display = numberWithCommas(value.toFixed(2).toLocaleString()) + '<br>' + 
                  numberWithCommas(vat.toFixed(2).toLocaleString()) + '<br>' + 
                  numberWithCommas(sum.toFixed(2).toLocaleString());
        $(this).html(display);
    });
});

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}