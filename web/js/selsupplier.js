$(".select-supplier").click(function() {
    
    //for each row/item
    $('tr').each(function(){
        
        hasMinPrice = false;
        hasInput = false;
        minPrice = -1;
        minSupplier = -1;
        //for each col - find lowest price
        $('input', $(this)).each(function(){
            hasInput = true;
            value = parseInt($(this).val());

            if (!hasMinPrice)
            {
                
                if (!isNaN(value) && value!=0)
                {
                    minPrice = value;
                    minSupplier = $(this).parent('div').attr('supplier'); //attribute supplier is set in the div (parent of input)
                    hasMinPrice = true;                
                }
            }
            else
            {
                if(!isNaN(value) && value!=0 && value < minPrice)
                {
                    minPrice = value;
                    minSupplier = $(this).parent('div').attr('supplier');
                }
            }
        });
        
        if(hasInput)//is going to select supplier for that row
        {
            targetValue='';
            if(hasMinPrice)targetValue = minSupplier;
            $(this).find('select').last().val(targetValue).change();
        }
        
        
    });
});