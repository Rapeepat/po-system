<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
    'on afterOpen' => function($event)
    {
        $event->sender->createCommand("SET time_zone = '" . "+07:00" . "'")->execute();
//        $event->sender->createCommand("SET time_zone = '" . date('P') . "'")->execute();
    }
];
