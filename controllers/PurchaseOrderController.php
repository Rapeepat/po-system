<?php

namespace app\controllers;

use Yii;
use app\models\PurchaseOrder;
use app\models\PurchaseOrderItemPrice;
use app\models\PurchaseOrderSiteAmount;
use app\models\OfficeSite;
use app\models\Offer;
use app\models\OfferItem;
use app\models\OfferPrice;
use app\models\OfferContact;
use app\models\OfferSiteList;
use app\models\Request;
use app\models\Supplier;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;

/**
 * PurchaseOrderController implements the CRUD actions for PurchaseOrder model.
 */
class PurchaseOrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'void' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
    * Get all values from specific key in a multidimensional array
    *
    * @param $key string
    * @param $arr array
    * @return null|string|array
    */
    private function array_value_recursive($key, array $arr){
        $val = array();
        array_walk_recursive($arr, function($v, $k) use($key, &$val){
            if($k == $key) array_push($val, $v);
        });
        return $val;
    }
    
    /**
     * Lists all PurchaseOrder models.
     * @return mixed
     */
    public function actionIndex($lot_id)
    {
        $models = $this->findModelsByLot($lot_id);
        
        return $this->render('index', [
            'models' => $models,
            'lot_id' => $lot_id,
        ]);
    }
    
    /**
     * Void or Devoid this purchase order.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionVoid($id)
    {
        $model = $this->findModel($id);
        $model->void = !$model->void;
        $model->save();      

        return $this->redirect(['view', 'id' => $id]);
    }
    
    /**
     * Displays a single PurchaseOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $purchaseOrder = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => PurchaseOrderItemPrice::find()->where(['purchase_order_id'=>$id]),
            'sort'=>false,
        ]);
        $dataProvider->setPagination(['pageSize'=>-1]);
        
        $siteList = $this->array_value_recursive(
            'site_id',
            PurchaseOrderSiteAmount::find()
                ->select('site_id')
                ->where(['purchase_order_id'=>$id])
                ->distinct()
                ->asArray()
                ->all()
        );

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'purchaseOrder'=>$purchaseOrder,
            'lot_id' => $purchaseOrder->offer->lot_id,
            'siteList' => $siteList,
        ]);
    }

    /**
     * Updates an existing PurchaseOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $purchaseOrder = $this->findModel($id);
        $purchaseOrder->scenario = PurchaseOrder::SCENARIO_UPDATE;
        
        if ($purchaseOrder->load(Yii::$app->request->post()) && $purchaseOrder->save()) {
            return $this->redirect(['view', 'id' => $purchaseOrder->id]);
        } else {
            return $this->render('update', [
                'purchaseOrder' => $purchaseOrder,
                'lot_id' => $purchaseOrder->offer->lot_id,
            ]);
        }
    }
    
    /**
     * Creates a new PurchaseOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($offer_id, $supplier_id)
    {
        //create purchase order for every supplier under this offer
        if($supplier_id==-1)
        {
            $supplierList = $this->array_value_recursive(
                'selected_supplier_id',
                OfferItem::find()
                    ->select('selected_supplier_id')
                    ->where('selected_supplier_id is not null')
                    ->andWhere(['offer_id'=>$offer_id])
                    ->distinct()
                    ->asArray()
                    ->all()
            );
            $supplierIsExist = true;
        }        
        //create purchase order for only this offer
        else
        {
            $supplierList = [$supplier_id];
            $supplierIsExist = Supplier::findOne($supplier_id)!==null;
        }
        
        //get offer model
        $offer = Offer::findOne($offer_id);
        
        //if Supplier and Offer are correct
        if(count($supplierList)>0  && $offer!==null  && $supplierIsExist)
        {
            $models = [];
            $supplierModels = [];
            foreach($supplierList as $supplier_id)
            {
                $models[] = new PurchaseOrder(['scenario'=>  PurchaseOrder::SCENARIO_CREATE]);
                $supplierModels[] = Supplier::findOne($supplier_id);
            }
                
            $post = Yii::$app->request->post();
            $po_ids = ArrayHelper::getValue($post, 'PurchaseOrder');
            $po_ids = $po_ids==null?[]:ArrayHelper::getColumn($po_ids, 'id');
                        
            
            if (count($po_ids) != count(array_unique($po_ids))) throw new \yii\web\BadRequestHttpException('กรุณาใส่เลขที่ใบสั่งซื้อที่แตกต่างกัน');
            if (PurchaseOrder::loadMultiple($models, $post) 
                && PurchaseOrder::validateMultiple($models))
            {
                return $this->createPurchaseOrder($offer->lot_id, $offer_id, $supplierList, $po_ids);
            } 
            else 
            {
                return $this->render('create', [
                    'models' => $models,
                    'supplierModels' => $supplierModels,
                    'lot_id' => $offer->lot_id,
                ]);
            }
        }
        
        throw new \yii\web\BadRequestHttpException('กรุณาเลือกใบเสนอราคาที่มีร้านค้า อย่างน้อย 1 รายการ');
    }

    /**
     * Creates a new PurchaseOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    private function createPurchaseOrder($lot_id, $offer_id, $supplierList, $poIdList)
    {                
        $date_created = date('Y-m-d H:i:s');//current timestamp db2 format
        //Foreach supplier to have purchase order created.
        foreach($supplierList as $i=>$selectedSupplier)
        {
            $purchaseOrder = $this->LoadPurchaseOrderContact($offer_id, $selectedSupplier);
            $purchaseOrder->date_created = $date_created;
            $purchaseOrder->id = $poIdList[$i];
            $purchaseOrder->save();

            //Get Site List for this offer
            $siteList = ArrayHelper::getColumn(OfferSiteList::find()->where(['offer_id'=>$offer_id])->all(), 'site_id');

            //Get each item under this selected supplier and create po_item_price
            $offerItems = OfferItem::find()->where(['offer_id'=>$offer_id, 'selected_supplier_id' => $selectedSupplier])->all();
            foreach($offerItems as $offerItem)
            {
                $purchaseOrderItemPrice = new PurchaseOrderItemPrice();
                $purchaseOrderItemPrice->purchase_order_id = $purchaseOrder->id;
                $purchaseOrderItemPrice->item_id = $offerItem->item_id;
                $purchaseOrderItemPrice->price = OfferPrice::findOne([
                        'offer_id' => $offer_id, 
                        'item_id' => $offerItem->item_id, 
                        'supplier_id' => $selectedSupplier
                    ])->price;

                $purchaseOrder->link('purchaseOrderItemPrices',$purchaseOrderItemPrice);

                //Get each Request under this offerItem then create po_site_amount
                $requests = Request::find()->where([
                        'lot_id' =>$lot_id,
                        'item_id'=>$offerItem->item_id,
                        'site_id'=>$siteList,
                    ])->andWhere('amount>0')->all();
                foreach($requests as $request)
                {
                    $purchaseOrderSiteAmount = new PurchaseOrderSiteAmount();
                    $purchaseOrderSiteAmount->purchase_order_id = $purchaseOrder->id;
                    $purchaseOrderSiteAmount->item_id = $offerItem->item_id;
                    $purchaseOrderSiteAmount->site_id = $request->site_id;
                    $purchaseOrderSiteAmount->amount  = $request->amount;

                    $purchaseOrder->link('purchaseOrderSiteAmounts',$purchaseOrderSiteAmount);
                }
            }
        }
        return $this->redirect(['index','lot_id'=>$lot_id]);
    }
    
    private function LoadPurchaseOrderContact($offer_id, $supplier_id)
    {
        $contact = OfferContact::findOne(['offer_id'=>$offer_id,'supplier_id'=>$supplier_id]);
        $purchaseOrder = new PurchaseOrder();
        
        $purchaseOrder->offer_id = $offer_id;
        $purchaseOrder->supplier_id = $supplier_id;
        
        $purchaseOrder->supplier_address_1    = $contact->supplier_address_1;
        $purchaseOrder->supplier_tel          = $contact->supplier_tel;
        $purchaseOrder->supplier_fax          = $contact->supplier_fax;
        $purchaseOrder->supplier_contact_name = $contact->supplier_contact_name;

        $purchaseOrder->shipping_address_1    = $contact->shipping_address_1;
        $purchaseOrder->shipping_contact_name = $contact->shipping_contact_name;
        $purchaseOrder->shipping_tel          = $contact->shipping_tel;
        
        return $purchaseOrder;
    }

    private function thai_date_fullmonth($time)
    {   // 19 ธันวาคม 2556
        $thai_month_arr=array(   
            "0"=>"",   
            "1"=>"มกราคม",   
            "2"=>"กุมภาพันธ์",   
            "3"=>"มีนาคม",   
            "4"=>"เมษายน",   
            "5"=>"พฤษภาคม",   
            "6"=>"มิถุนายน",    
            "7"=>"กรกฎาคม",   
            "8"=>"สิงหาคม",   
            "9"=>"กันยายน",   
            "10"=>"ตุลาคม",   
            "11"=>"พฤศจิกายน",   
            "12"=>"ธันวาคม"                    
        );   
        $thai_date_return='';
        $thai_date_return.=date("j",$time);   
        $thai_date_return.=" ".$thai_month_arr[date("n",$time)];   
        $thai_date_return.= " ".(date("Y",$time)+543);   
        return $thai_date_return;   
    } 
    private function insertLogo(&$objPHPExcel, $row_to)
    {
        //add logo to destination 
        $objDrawing = new \PHPExcel_Worksheet_Drawing();    //create object for Worksheet drawing
        $objDrawing->setName('Logo');        //set name to image
        $objDrawing->setDescription('Logo'); //set description to image
        $objDrawing->setPath('template/PFM logo.jpg');
        $objDrawing->setCoordinates('A'.$row_to);        //set image to cell
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());  //save
        $objDrawing->setOffsetX(10);                       //setOffsetX works properly
        $objDrawing->setOffsetY(10); 
        //$objDrawing->setWidth(32);                 //set width, height
        $objDrawing->setHeight(75);  
    }
    private function startNewPage(&$objPHPExcel, $row_to, $for_number_of_rows, $row_from = 1) 
    {
        $work_sheet = $objPHPExcel->getActiveSheet();
        for($i=0;$i<$for_number_of_rows;$i++)
        {
            $work_sheet->getRowDimension($row_to+$i)->setRowHeight($work_sheet->getRowDimension($row_from+$i)->getRowHeight());
            $lastColumn = $work_sheet->getHighestColumn();
            ++$lastColumn;
            for ($c = 'A'; $c != $lastColumn; ++$c) {
                $cell_from = $work_sheet->getCell($c.($row_from+$i));
                $cell_to = $work_sheet->getCell($c.($row_to+$i));
                $cell_to->setXfIndex($cell_from->getXfIndex()); // black magic here
                $cell_to->setValue($cell_from->getValue());
            }
        }
    }
    
    /*
     * Creates the xls files
     */
    private function buildxls($POModels, $POUniqueSites)
    {        
        $maxRow = 45;
        $maxItem = 15;
        $maxSite = 8;
        
        $tmpfname = "template/PurchaseOrder.xls";
        $objPHPExcel = \PHPExcel_IOFactory::createReader('Excel5')->load($tmpfname);
        $objPHPExcel->setActiveSheetIndex(0);
        $workSheet = $objPHPExcel->getActiveSheet();
        
        //Set page template
        //Start new page immediately and we can use it as a template. Then, just delete it
        //The reason is we can copy the template with "static" row number ($maxRow), 
        //since we're inserting new rows when items number exceeds $maxItem
        $pageNumber = 2;
        $AllExtraItems = 0;
        foreach($POModels  as $po_key =>  $purchaseOrder)
        {            
            $itemsCount = count($purchaseOrder->purchaseOrderItemPrices);
            $extraItem = $itemsCount>$maxItem?$itemsCount-$maxItem:0;
            $pageOffset = ($pageNumber-1)*$maxRow + $AllExtraItems;
            $AllExtraItems+=$extraItem;
            
            $this->insertLogo($objPHPExcel, $pageOffset+1);
            $this->startNewPage($objPHPExcel, $pageOffset+1, $maxRow);
            
            for($i=0;$i<$extraItem;$i++)
            {
                $workSheet->insertNewRowBefore($pageOffset+19);
            }
            
            // !!!!!!!!!  N O T E  !!!!!!!! N O T E !!!!!!!!!!!!!!!
            // Any excel range referenced by +35 or more - must be added by extraitem
            
            //Set literals/function/cell value on each page
            $workSheet
                ->setCellValue('N'.($pageOffset+ 8), $purchaseOrder->id.'/'.substr(date('Y',strtotime($purchaseOrder->date_created))+543,2,2))
                ->setCellValue('N'.($pageOffset+10), $this->thai_date_fullmonth(time()))
                ->setCellValue('B'.($pageOffset+12), 'ร้าน '.$purchaseOrder->supplier->name)
                ->setCellValue('F'.($pageOffset+12), 'แฟกซ์ '.$purchaseOrder->supplier_fax)
                ->setCellValue('N'.($pageOffset+12), $purchaseOrder->supplier_contact_name)
                ->setCellValue('B'.($pageOffset+13), $purchaseOrder->supplier_address_1)
                ->setCellValue('N'.($pageOffset+13), $purchaseOrder->supplier_tel)
                //I can't set the offer number because I used the same offer id for every supplier
                //If needed, may need a running number on OfferSite to do this 
                //->setCellValue('C'.($pageOffset+17), 'ตามเลขที่ใบเสอราคา '.$purchaseOrder->offer_id.'/'.substr(date('Y',strtotime($purchaseOrder->offer->date_created))+543,2,2))
                ->setCellValue('A'.($pageOffset+35+$extraItem), '=BAHTTEXT(O'.($pageOffset+35+$extraItem).')')
                ->setCellValue('O'.($pageOffset+33+$extraItem), '=SUM(O'.($pageOffset+18).':O'.($pageOffset+$maxItem+$extraItem+18-1).')')  //total
                ->setCellValue('O'.($pageOffset+34+$extraItem), '=0.07*O'.($pageOffset+33+$extraItem))  //vat
                ->setCellValue('O'.($pageOffset+35+$extraItem), '=SUM(O'.($pageOffset+33+$extraItem).':O'.($pageOffset+34+$extraItem).')'); //vat+total
            
            //Shipping annotation
            if($itemsCount<$maxItem)
            {
                $rowOffset = $pageOffset + (int)((18+$itemsCount + 32) / 2) ;
                $workSheet
                    ->setCellValue('A'.$rowOffset, 'หมายเหตุ')
                    ->setCellValue('C'.$rowOffset, 'ให้จัดส่งที่บริษัท โพรเกรส ฟาซิลิตีส์ แมเนจเมนต์ จำกัด')
                    ->setCellValue('C'.($rowOffset+1), $purchaseOrder->shipping_address_1)
                    ->setCellValue('C'.($rowOffset+2), 'ติดต่อ '.$purchaseOrder->shipping_contact_name.' '.$purchaseOrder->shipping_tel)
                    ->mergeCells('A'.$rowOffset.':B'.$rowOffset)
                    ->mergeCells('C'.$rowOffset.':K'.$rowOffset)
                    ->mergeCells('C'.($rowOffset+1).':K'.($rowOffset+1))
                    ->mergeCells('C'.($rowOffset+2).':K'.($rowOffset+2));
                $style = $workSheet->getStyle('A'.$rowOffset.':K'.($rowOffset+2));
                $style->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                $style->getFont()->setBold(true);
                $style->getFont()->setSize(20);
                $workSheet->getRowDimension($rowOffset)->setRowHeight(-1);
                $workSheet->getRowDimension($rowOffset+1)->setRowHeight(-1);
                $workSheet->getRowDimension($rowOffset+2)->setRowHeight(-1);
            }
            
            //Site level
            $col = 'D';
            foreach($POUniqueSites[$po_key] as $site_id)
            {
                if(is_array($site_id)) $name = 'อื่นๆ';
                else $name = OfficeSite::findOne($site_id)->name;
                $workSheet->setCellValue($col.($pageOffset+16), $name);
                $col++;
            }

            //Merge cells
            $lastOffet = $pageOffset+40+$extraItem; //This var is being used a couple of times so I defined it.
            $workSheet
                ->mergeCells('A'.($pageOffset+ 9).':O'.($pageOffset+ 9))
                ->mergeCells('D'.($pageOffset+15).':K'.($pageOffset+15))
                ->mergeCells('A'.($pageOffset+35+$extraItem).':L'.($pageOffset+35+$extraItem))
                ->mergeCells('A'.$lastOffet.':C'.$lastOffet)
                ->mergeCells('D'.$lastOffet.':J'.$lastOffet)
                ->mergeCells('K'.$lastOffet.':O'.$lastOffet);
            
            //office site name
            for($i=0,$col='D';$i<$maxSite;$i++,$col++)
            {
                $cellRange = $col.($pageOffset+16).':'.$col.($pageOffset+17);
                $workSheet->mergeCells($cellRange);
                //set word wrap
                $workSheet->getStyle($cellRange)->getAlignment()->setWrapText(true); 
            }
            
            $pageNumber += 1;
        }
        
        //Set item/row
        $pageNumber = 2;
        $AllExtraItems = 0;
        foreach($POModels as $po_key => $purchaseOrder)
        {   
            $poItems = $purchaseOrder->purchaseOrderItemPrices;
            $itemsCount = count($poItems);
            $extraItem = $itemsCount>$maxItem?$itemsCount-$maxItem:0;
            $pageOffset = ($pageNumber-1)*$maxRow + $AllExtraItems;
            $AllExtraItems+=$extraItem;
            
                   
            $rowNumber = 1;
            $total_price = 0;
            foreach($poItems as $poItem)
            {
                $rowOffset = $pageOffset + 17 + $rowNumber;
                
                //Site level
                $col = 'D';
                $total_amount = 0;
                foreach($POUniqueSites[$po_key] as $site_id)
                {
                    $amount = PurchaseOrderSiteAmount::find()
                        ->select('sum(amount) as sum_amount')
                        ->where([
                            'site_id'=>$site_id,
                            'purchase_order_id'=>$purchaseOrder->id,
                            'item_id'=>$poItem->item_id])
                        ->asArray()
                        ->all()[0]['sum_amount'];
                    $workSheet->setCellValue($col.$rowOffset, $amount==0?'-':$amount);  
                    $total_amount+=$amount;
                    $col++;
                }
                
                //Item level information
                $workSheet
                    ->setCellValue('A'.$rowOffset, $rowNumber)
                    ->setCellValue('C'.$rowOffset, $poItem->item->name)
                    ->setCellValue('M'.$rowOffset, $poItem->price)
                    ->setCellValue('N'.$rowOffset, $poItem->item->unit)
                    ->setCellValue('L'.$rowOffset, $total_amount)
                    ->setCellValue('O'.$rowOffset, $total_amount*$poItem->price);
                $total_price+=$total_amount*$poItem->price;
                $rowNumber += 1;
            }
            
            $pageNumber += 1;
        }
        $workSheet->removeRow(1, $maxRow); //Delete first page template
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('Purchase Order PFM.xls');
    }
    
    /**
     * Print purchase order for the date input
     * @return mixed
     */
    public function actionPrintPo($date_created)
    {
        $POModels = PurchaseOrder::find()->where(['date_created'=>$date_created, 'void'=>0])->all();
        if (count($POModels)==0) throw new NotFoundHttpException('ไม่มีใบสั่งซื้อที่ทำงานอยู่');
        
        $maxSite = 8;
        $POUniqueSites=[];
        foreach($POModels as $purchaseOrder)
        {   
            //Get unique sites for this po. If sites has more than max, pool all amount to the $maxsite'th one.
            $uniqueSites = ArrayHelper::getColumn(PurchaseOrderSiteAmount::find()
                    ->select('site_id')
                    ->distinct()
                    ->where(['purchase_order_id'=>$purchaseOrder->id])
                    ->orderBy('site_id')
                    ->asArray()
                    ->all(),'site_id');
            if(count($uniqueSites)>$maxSite)
            {
                $lastGroup = array_slice($uniqueSites, $maxSite - 1, count($uniqueSites) - $maxSite + 1);
                $uniqueSites = array_slice($uniqueSites, 0, $maxSite - 1);
                $uniqueSites[] = $lastGroup;
            }
            $POUniqueSites[] = $uniqueSites;
        }
        
        $this->buildxls($POModels, $POUniqueSites);
        
        return Yii::$app->response->sendFile('Purchase Order PFM.xls');
    }
    
    /**
     * Finds the PurchaseOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PurchaseOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PurchaseOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('ไม่พบหน้าที่ต้องการ');
        }
    }
    
    protected function findModelsByLot($lot_id)
    {
        $models = PurchaseOrder::find()
                ->joinWith('offer')
                ->where(['lot_id'=>$lot_id])
                ->orderBy([
                    'date_created'=>SORT_DESC,
                    'void'=>SORT_ASC,
                    'id'=>SORT_DESC,
                ])
                ->all();
        if (count($models) > 0) {
            return $models;
        } else {
            throw new NotFoundHttpException('ไม่มีใบสั่งซื้อ กรุณาสร้างใบสั่งซื้อ');
        }
    }
}
