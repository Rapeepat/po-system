<?php

namespace app\controllers;

use Yii;
use app\models\Item;
//use app\models\Request;
use app\models\Offer;
use app\models\OfferSiteList;
use app\models\OfferItem;
use app\models\OfferPrice;
use app\models\OfferContact;
use app\models\OfficeSite;
use app\models\Supplier;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
//use yii\filters\VerbFilter;

/**
 * OfferController implements the CRUD actions for Offer model.
 */
class OfferController extends Controller
{
    /**
     * @inheritdoc
     */
    /*public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'offer-print' => ['POST'],
                    'offer-compare-print' => ['POST'],
                ],
            ],
        ];
    }*/

    /**
     * Lists all Offer models.
     * @return mixed
     */
    public function actionIndex($lot_id)
    {
        $models = $this->findLotModels($lot_id);

        if(count($models)==1)
        {
            return $this->redirect(['view', 'id'=>$models[0]->id]);
        }
        
        return $this->render('index', [
            'models' => $models,
            'lot_id' => $lot_id,
        ]);
    }

    /**
    * Get all values from specific key in a multidimensional array
    *
    * @param $key string
    * @param $arr array
    * @return null|string|array
    */
    private function array_value_recursive($key, array $arr){
        $val = array();
        array_walk_recursive($arr, function($v, $k) use($key, &$val){
            if($k == $key) array_push($val, $v);
        });
        return $val;
    }
    
    protected function getSupplierList($id)
    {
        return $this->array_value_recursive(
                'supplier_id',
                OfferPrice::find()->where(['offer_id'=>$id])->select('supplier_id')->distinct()->asArray()->all()
            );
    }


    /**
     * Displays a single Offer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => OfferItem::find()->where(['offer_id'=>$id]),
        ]);
        $dataProvider->setPagination(['pageSize'=>-1]);

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'supplierList' => $this->getSupplierList($id),
            'offer'=>$this->findModel($id),
        ]);
    }

    /**
     * Creates a new Offer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($lot_id)
    {
        $post = Yii::$app->request->post();
        $supplierList = [];
        
        $requestExist = false;
        foreach($post['Request'] as $requestChkBox)
        {
            if(in_array('1', $requestChkBox, true)) $requestExist = true;
        }
        if(!$requestExist) throw new \yii\web\BadRequestHttpException('กรุณาเลือกร้านค้าอย่างน้อย 1 ร้านค้า');
        //return '<pre>'.print_r( $post['Request'],true).'</pre>';
        
        //Check if post was sent from Request correctly
        if (array_key_exists('Request', $post) && array_key_exists('Site', $post) && array_key_exists('shipping_site_id', $post)) 
        {
            //Build list of OfferSite
            $offerSites = [];
            foreach($post['Site'] as $site)
            {
                $offerSite = new OfferSiteList();
                $offerSite->site_id = $site;
                $offerSites[] = $offerSite;
            }
            
            //Build list of OfferItem & OfferPrice (OfferPrice is child of OfferItem)
            $offerItems = [];          
            $offerPrices = [];         
            foreach($post['Request'] as $item_id => $suppliers)
            {
                $offerItem = new OfferItem();
                $offerItem->item_id = $item_id;
                $offerItems[] = $offerItem;
                
                //Build list of OfferPrice
                foreach($suppliers as $supplier_id => $selected)
                {
                    if($selected)
                    {
                        $offerPrice = new OfferPrice();
                        $offerPrice->item_id = $item_id;
                        $offerPrice->supplier_id = $supplier_id;
                        $offerPrices[] = $offerPrice;
                        
                        //create list of all suppliers
                        $supplierList[$supplier_id] = true;
                    }
                }
            }
            
            $model = new Offer();
            $model->lot_id = $lot_id;
            //Validation
            if( OfferItem::validateMultiple($offerItems)
                && OfferPrice::validateMultiple($offerPrices)
                && OfferSiteList::validateMultiple($offerSites)
                && $model->validate()
              )
            {
                $model->save();
                foreach($offerItems as $offerItem)
                {
                    $model->link('offerItems',$offerItem);
                }
                foreach($offerSites as $offerSite)
                {
                    $model->link('offerSiteLists',$offerSite);
                }
                foreach($offerPrices as $offerPrice)
                {
                    $model->link('offerPrices',$offerPrice);
                }
            }
            
            //create OfferContacts
            $shipping_site_id = $post['shipping_site_id'];
            $this->CreateContact( $model->id, $shipping_site_id, array_keys($supplierList));
            return $this->redirect(['view','id'=>$model->id]);
        }
        throw new \yii\web\BadRequestHttpException('กรุณาสร้างคำขอสั่งซื้ออย่างน้อย 1 รายการ');
    }
    
    protected function CreateContact($offer_id, $site_id, $supplierList)
    {   
        foreach($supplierList as $supplier_id)
        {
            $contact = new OfferContact();
            $contact->offer_id = $offer_id;
            $contact->supplier_id = $supplier_id;
            
            $supplier = Supplier::findOne($supplier_id);
            $site = OfficeSite::findOne($site_id);
             
            $contact->supplier_address_1 = $supplier->address_1;
            $contact->supplier_tel       = $supplier->tel;
            $contact->supplier_fax       = $supplier->fax;
            $contact->supplier_contact_name = $supplier->contact_name;
            
            
            $contact->shipping_address_1    = $site->address_1;
            $contact->shipping_contact_name = $site->contact_name;
            $contact->shipping_tel          = $site->tel;
            
            $contact->save();
        }
    }

    public function actionSummary($id)
    {
        $supplierList = $this->array_value_recursive(
            'selected_supplier_id',
            OfferItem::find()
                ->select('selected_supplier_id')
                ->where('selected_supplier_id is not null')
                ->andWhere(['offer_id'=>$id])
                ->distinct()
                ->asArray()
                ->all()
        );
        
        if($supplierList==null) throw new NotFoundHttpException('กรุณาเลือกร้านค้า สำหรับสินค้าแต่ละชนิด');
        
        //Create array of dataprovider
        //Key = supplier_id
        //Val = dataprovider of offeritem group by selected supplier id
        $offerItemsBySupplier = []; 
        foreach($supplierList as $selectedSupplier)
        {
            $dataProvider = new ActiveDataProvider([
                'query' => OfferItem::find()->where(['offer_id'=>$id, 'selected_supplier_id' => $selectedSupplier]),
            ]);
            $offerItemsBySupplier[$selectedSupplier] = $dataProvider;
        }
        
        //return '<pre>'.print_r($supplierList, true).'</pre>';
        return $this->render('summary',[
            'offerItemBySupplier' => $offerItemsBySupplier, //
            'offer' => $this->findModel($id),
        ]);
    }
    
    
    private function thai_date_fullmonth($time)
    {   // 19 ธันวาคม 2556
        $thai_month_arr=array(   
            "0"=>"",   
            "1"=>"มกราคม",   
            "2"=>"กุมภาพันธ์",   
            "3"=>"มีนาคม",   
            "4"=>"เมษายน",   
            "5"=>"พฤษภาคม",   
            "6"=>"มิถุนายน",    
            "7"=>"กรกฎาคม",   
            "8"=>"สิงหาคม",   
            "9"=>"กันยายน",   
            "10"=>"ตุลาคม",   
            "11"=>"พฤศจิกายน",   
            "12"=>"ธันวาคม"                    
        );   
        $thai_date_return='วันที่ ';
        $thai_date_return.=date("j",$time);   
        $thai_date_return.=" ".$thai_month_arr[date("n",$time)];   
        $thai_date_return.= " ".(date("Y",$time)+543);   
        return $thai_date_return;   
    } 
    
    private function insertLogo(&$objPHPExcel, $row_to)
    {
        //add logo to destination 
        $objDrawing = new \PHPExcel_Worksheet_Drawing();    //create object for Worksheet drawing
        $objDrawing->setName('Logo');        //set name to image
        $objDrawing->setDescription('Logo'); //set description to image
        $objDrawing->setPath('template/PFM logo.jpg');
        $objDrawing->setCoordinates('A'.$row_to);        //set image to cell
        $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());  //save
    }
    private function startNewPage(&$objPHPExcel, $row_to, $for_number_of_rows, $row_from = 1) 
    {
        $work_sheet = $objPHPExcel->getActiveSheet();
        for($i=0;$i<$for_number_of_rows;$i++)
        {
            $work_sheet->getRowDimension($row_to+$i)->setRowHeight($work_sheet->getRowDimension($row_from+$i)->getRowHeight());
            $lastColumn = $work_sheet->getHighestColumn();
            ++$lastColumn;
            for ($c = 'A'; $c != $lastColumn; ++$c) {
                $cell_from = $work_sheet->getCell($c.($row_from+$i));
                $cell_to = $work_sheet->getCell($c.($row_to+$i));
                $cell_to->setXfIndex($cell_from->getXfIndex()); // black magic here
                $cell_to->setValue($cell_from->getValue());
            }
        }
    }
    
    /*
     * Creates the xls files
     */
    private function buildxls($exportArraySupplier)
    {        
        $maxRow = 38;
        $maxItem = 23;
        
        $tmpfname = "template/Offer.xls";
        $objPHPExcel = \PHPExcel_IOFactory::createReader('Excel5')->load($tmpfname);
        $objPHPExcel->setActiveSheetIndex(0);
        
        //Set pages
        $pageNumber = 1;
        foreach($exportArraySupplier as $supplier_id => $exportArray)
        {            
            $contact = $exportArray['contact'];
            $subPage = (int)((count($exportArray) - 2)/$maxItem);
            for($i=0;$i<=$subPage;$i++)
            {
                $pageOffset = ($pageNumber-1)*$maxRow;
                if($pageNumber>1)
                {
                    $this->insertLogo($objPHPExcel, $pageOffset+1);
                    $this->startNewPage($objPHPExcel, $pageOffset+1, $maxRow);
                }
                $objPHPExcel->getActiveSheet()
                    ->setCellValue('D'.($pageOffset+6), $this->thai_date_fullmonth(time()))
                    ->setCellValue('A'.($pageOffset+10),'เรียน       '.$contact['supplier_contact_name'].'                     '.$contact['supplier_name'])
                    ->setCellValue('A'.($pageOffset+12),'จาก        '.$contact['contact_name'])
                    ->setCellValue('D'.($pageOffset+10),'โทร  '.$contact['tel'])
                    ->setCellValue('D'.($pageOffset+11),'แฟกซ์ '.$contact['fax']);
                $pageNumber += 1;
            }
        }
        
        $pageNumber = 1;
        foreach($exportArraySupplier as $supplier_id => $exportArray)
        {   
            $pageOffset = ($pageNumber-1)*$maxRow;
            unset($exportArray['contact']);
            
            $rowNumber = 1;
            foreach($exportArray as $row)
            {
                $rowOffset = $pageOffset + 14 + (($rowNumber-1) % $maxItem) + 1;
                
                $objPHPExcel->getActiveSheet()
                    ->setCellValue('A'.$rowOffset, $rowNumber)
                    ->setCellValue('B'.$rowOffset, $row['display_code'])
                    ->setCellValue('C'.$rowOffset, $row['item_name'])
                    ->setCellValue('D'.$rowOffset, $row['amount'])
                    ->setCellValue('E'.$rowOffset, $row['item_unit'])
                    ->setCellValue('F'.$rowOffset, $row['price']);
                
                $rowNumber += 1;
                
                //         24 > 23                   24%23=1
                //         47 > 23                   47%23=1
                if($rowNumber > $maxItem && $rowNumber % $maxItem == 1)
                {
                    $pageNumber += 1;
                    $pageOffset = ($pageNumber-1)*$maxRow;
                }
            }
            $pageNumber += 1;
        }
        
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('Offer PFM.xls');
    }
    
    public function actionOfferPrint($id)
    {
        //Get the array to be able to export
        // Lvl1 dimension (KEY) = supplier_id
        // Lvl2 dimension = lvl1supplier item list
        // lvl3 dimension = item
        $offerModel = $this->findModel($id);
        $exportArraySupplier = ArrayHelper::index(
            OfferPrice::find()
                ->select(['supplier_id', 'item_id'])
                ->where(['offer_id'=>$id])
                ->orderBy('supplier_id,item_id')->asArray()->all(),
            null,
            'supplier_id'
        );         
        
        //For each "exportarray"
        foreach($exportArraySupplier as $supplier_id => $exportArray)
        {
            $offerContact = OfferContact::findOne(['offer_id'=>$id, 'supplier_id'=>$supplier_id]);
            $supplierModel = Supplier::findOne($supplier_id);
            $exportArraySupplier[$supplier_id]['contact'] = [
                'supplier_contact_name' => $offerContact->supplier_contact_name,
                'supplier_name' => $supplierModel->name,
                'tel'           => $offerContact->supplier_tel,
                'fax'           => $offerContact->supplier_fax,
                'contact_name'  => $offerContact->shipping_contact_name
            ];
            foreach($exportArray as $row_id=>$row)
            {
                $requestAmount = OfferItem::findOne(['offer_id'=>$id,'item_id'=>$row['item_id']])->getSumAmount();
                $itemModel = Item::findOne($row['item_id']);
                
                $exportArraySupplier[$supplier_id][$row_id]['display_code'] = $itemModel->display_code;
                $exportArraySupplier[$supplier_id][$row_id]['item_name'] = $itemModel->name;
                $exportArraySupplier[$supplier_id][$row_id]['amount'] = $requestAmount;
                $exportArraySupplier[$supplier_id][$row_id]['item_unit'] = $itemModel->unit;
                $exportArraySupplier[$supplier_id][$row_id]['price'] = '';
            }
        }
        
        $this->buildxls($exportArraySupplier);
        
        return Yii::$app->response->sendFile('Offer PFM.xls');
    }
    
    public function actionOfferComparePrint($id, $summarize)
    {
        $sheetOffset = 6;
        
        $offerItems = OfferItem::find()->where(['offer_id'=>$id])->all();
        $supplier_ids = ArrayHelper::map(OfferPrice::find()
                ->select('supplier_id')
                ->distinct()
                ->where(['offer_id' => $id])
                ->orderBy('supplier_id')
                ->all(),
            'supplier_id', 'supplier_id');
        $supplierModels = Supplier::find()->where(['id'=>$supplier_ids])->all();
        
        //later this array will be used in amount sum - the 'key' is supplier_id and the 'value' is summed amount        
        $supplier_ids = array_fill_keys($supplier_ids, 0);
        
        //build sheet header
        $sheet = [
            ['ลำดับ','รายการ','จำนวน','หน่วย'],
            ['','','','']
        ];
        foreach($supplierModels as $supplier)
        {
            $sheet[0][] = $supplier->name;
            if(!$summarize)
            {
                $sheet[0][] = '';
                $sheet[1][] = 'ราคา/หน่วย';
                $sheet[1][] = 'รวมเป็นเงิน';
            }
            
        }
        if(!$summarize)
        {
            $sheet[0][] = 'ร้านค้าที่ถูกเลือก';
            $sheet[0][] = 'หมายเหตุ';
            $sheet[1][] = '';
            $sheet[1][] = '';
        }
        
        //build sheet item detail
        $itemNumber = 1;
        foreach($offerItems as $offerItem)
        {
            $amount = $offerItem->getSumAmount();
            //item information
            $row = [
                $itemNumber,
                $offerItem->item->name,
                $amount,
                $offerItem->item->unit,
            ];
            
            //suppliers information
            foreach($supplierModels as $supplier)
            {
                $offerPrice = OfferPrice::findOne(['offer_id'=>$id, 'item_id'=>$offerItem->item_id, 'supplier_id'=>$supplier->id]);
                $price = ArrayHelper::getValue($offerPrice,'price',0);
                
                //Set price to zero if this is Summarize sheet and this supplier is not a selected supplier for this item
                if($summarize && $offerItem->selected_supplier_id != $supplier->id)
                {
                    $price = 0;
                }
                
                if($price>0)
                {
                    if(!$summarize) $row[] = $price;
                    $row[] = $price * $amount;
                }
                elseif($price==0)
                {
                    if(!$summarize) $row[] = '-'; 
                    $row[] = '-';
                }
                else
                {
                    if(!$summarize) $row[] = ' '; 
                    $row[] = ' ';
                }
                
                $supplier_ids[$supplier->id] += $price * $amount;
            }
            
            if(!$summarize)
            {
                $row[] = ArrayHelper::getValue($offerItem->selectedSupplier,'name','(ยังไม่ถูกเลือก)');
                $row[] = $offerItem->annotation;
            }
            
            //Write this row if - 1)this is compare sheet, or 2)user has selected a supplier
            if(!$summarize || $offerItem->selected_supplier_id != null)
            {
                $itemNumber += 1;
                $sheet[] = $row;
            }
        }
        
        //build sheet footer
        $sum_row = ['',$summarize?'รวม':'','',''];
        $vat_row = ['',$summarize?'VAT 7%':'','',''];
        $sumvat_row = ['',$summarize?'รวมทั้งสิ้น':'','',''];
        foreach($supplierModels as $supplier)
        {
            if(!$summarize)
            {
                $sum_row[] = 'รวม';
                $vat_row[] = 'VAT 7%';
                $sumvat_row[] = 'รวมทั้งสิ้น';
            }
            if($supplier_ids[$supplier->id]>0)
            {
                $sum_row[]    = $supplier_ids[$supplier->id];
                $vat_row[]    = $supplier_ids[$supplier->id]*0.07;
                $sumvat_row[] = $supplier_ids[$supplier->id]*1.07;
            }
            else
            {
                $sum_row[] = ' '; $vat_row[] = ' '; $sumvat_row[] = ' ';
            }
        }
        $sheet[] = $sum_row;
        $sheet[] = $vat_row;
        $sheet[] = $sumvat_row;
                
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $worksheet = $objPHPExcel->getActiveSheet();
        
        //Put array to worksheet
        $worksheet->fromArray($sheet, null, 'A'.$sheetOffset);   //Push data from array to worksheet
        $this->insertLogo($objPHPExcel, 1);
        $usedCol = $worksheet->getHighestColumn();
        $usedRow = $worksheet->getHighestRow();
        $prevCol = chr(ord($usedCol)-1); 
        
        //Set borders    
        $endRange = $usedCol.$worksheet->getHighestDataRow();
        $styleArray = ['borders' => ['allborders' => ['style' => \PHPExcel_Style_Border::BORDER_THIN]]];
        $worksheet->getStyle('A'.$sheetOffset.':'.$endRange)->applyFromArray($styleArray);
        $col='E';
        if(!$summarize)$col++;
        foreach($supplierModels as $supplier)
        {
            $worksheet->getStyle($col.$usedRow)
                ->getBorders()
                ->getBottom()
                ->setBorderStyle(\PHPExcel_Style_Border::BORDER_DOUBLE);
            if(!$summarize)$col++;
            $col++;
        }
        
        //Remove border from bottom left corner
        $styleArray = ['borders' => ['allborders' => ['style' => \PHPExcel_Style_Border::BORDER_NONE]]];
        $worksheet->getStyle('A'.($usedRow-2).':D'.$usedRow)->applyFromArray($styleArray);
        $worksheet->getStyle('B'.($usedRow-2).':B'.$usedRow)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    
        //Set number format
        $worksheet->getStyle('E'.($sheetOffset+2).':'.$endRange)->getNumberFormat()->setFormatCode('#,##0.00'); 
        
        //Bold first 2 rows
        $worksheet->getStyle('A'.$sheetOffset.':'.$usedCol.($sheetOffset+1))->getFont()->setBold(true);
        
        //Set grand total cell
        if($summarize)
        {
            $grandTotal = ++$usedCol.$usedRow;
            $worksheet->getStyle($grandTotal)->getFont()->setBold(true);
            $worksheet->getStyle($grandTotal)->getNumberFormat()->setFormatCode('#,##0.00'); 
            $worksheet->setCellValue($grandTotal, array_sum($supplier_ids));
        }
        
        $worksheet->setCellValue('A'.($sheetOffset-1), $summarize?'สรุปการขออนุมัติจัดซื้อ':'สรุปการเปรียบเทียบราคา');
        $worksheet->getStyle('A'.($sheetOffset-1))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $worksheet->mergeCells('A'.($sheetOffset-1).':'.$usedCol.($sheetOffset-1));
        
        //Set autosize for all the usedrange        
            //Try getting true type font to determine exact column width
        try {
            \PHPExcel_Shared_Font::setTrueTypeFontPath('C:/Windows/Fonts/');
            \PHPExcel_Shared_Font::setAutoSizeMethod(\PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
        } catch (Exception $ex) {}
        //turn auto width on, then "save/keep" current width, then turn off. To prevent the merged cell auto width problem.
        for ($i = 'A'; $i <=  $usedCol; $i++) $worksheet->getColumnDimension($i, true)->setAutoSize(true);
        $worksheet->calculateColumnWidths();
        for ($i = 'A'; $i <=  $usedCol; $i++) $worksheet->getColumnDimension($i, true)->setAutoSize(false);
        
        if($summarize)
        {
            $fileName = 'Summary PFM.xls';
            $worksheet->removeRow($sheetOffset+1, 1); //Delete first page template
        }
        else           
        {
            $fileName = 'Compare PFM.xls';
            
            //Merge and highlight cells //Merge header rows
            $worksheet
                ->mergeCells('A'.$sheetOffset.':A'.($sheetOffset+1))    //Merge 'ลำดับ'
                ->mergeCells('B'.$sheetOffset.':B'.($sheetOffset+1))    //Merge 'รายการ'
                ->mergeCells('C'.$sheetOffset.':C'.($sheetOffset+1))    //Merge 'จำนวน'
                ->mergeCells('D'.$sheetOffset.':D'.($sheetOffset+1))    //Merge 'หน่วย'
                ->mergeCells($usedCol.$sheetOffset.':'.$usedCol.($sheetOffset+1))    //Merge 'หมายเหตุ'
                ->mergeCells($prevCol.$sheetOffset.':'.$prevCol.($sheetOffset+1));   //Merge 'ร้านค้าที่ถูกเลือก'
            
            $left='E';
            $colIndex = 4;
            foreach($supplierModels as $supplier)
            {
                $right = $left;
                $right++;
                $worksheet->mergeCells($left.$sheetOffset.':'.$right.$sheetOffset); // Merge supplier name

                //Merge each no offer items and color item for selected supplier
                $prevIsEmpty = false;
                $detailOffset = $sheetOffset + 1; //+$sheetOffset+2 to skip header lines
                $firstEmptyIndex = $detailOffset;
                for($rowIndex=$detailOffset;$rowIndex <= count($offerItems)+$detailOffset;$rowIndex++) 
                {
                    //Color cells for selected supplier
                    if($rowIndex-$detailOffset<count($offerItems)
                        && $offerItems[$rowIndex-$detailOffset]->selected_supplier_id == $supplier->id)
                    {
                        $worksheet->getStyle($left.($rowIndex+1).':'.$right.($rowIndex+1))
                            ->getFill()
                            ->applyFromArray([
                                'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                                'startcolor' => ['rgb' => 'AAAAAA']
                            ]);
                    }

                    //Merge cells
                    if(trim($sheet[$rowIndex-$detailOffset+2][$colIndex])=='')
                    {
                        if(!$prevIsEmpty)
                        {
                            $firstEmptyIndex = $rowIndex;
                        }
                        $prevIsEmpty = true;
                    }
                    else //current row is not empty
                    {
                        //and prev row is empty, then merge
                        if($prevIsEmpty)
                        {
                            $worksheet->mergeCells($left.($firstEmptyIndex+1).':'.$right.$rowIndex); //actual cell row = array index + 1 , always
                        }
                        $prevIsEmpty = false;
                    }
                }

                $left++;
                $left++;
                $colIndex+=2;
            }
        }
        
        
        $writer = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $writer->save($fileName);
                
        //return '<pre>'.print_r($sheet,true).'</pre>';
        return Yii::$app->response->sendFile($fileName);
    }
    
    /**
     * Updates an existing OfferItem models under Offer = &id.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {        
        $offer = $this->findModel($id);
        
        //array of models. It's length = (item_count x site_count)
        $models = [];
        
        //Get set of OfferItem
        $itemList = $offer->offerItems;
        $itemCount = count($itemList);
        
        //Get set of Supplier
        $supplierList = $this->getSupplierList($id);
        $supplierCount = count($supplierList);
        
        //Loop below is to get a model of each OfferPrice {OfferItem x Supplier}.
        //If the OfferItem does not exit, then create new (but will DISABLE update in View).
        //Then the "update" action will be taken once models are loaded and validated
        
        //For each item (1 row = 1 item)
        for($row=0;$row<$itemCount;$row++)
        {
            //For each supplier of current item (1 col = 1 site)
            for($col=0;$col<$supplierCount;$col++)
            {
                $model = OfferPrice::findOne([
                    'offer_id'=>$id,
                    'item_id'=>$itemList[$row]->item_id,
                    'supplier_id'=>$supplierList[$col],
                ]);
                if($model==null) 
                {
                    $model=new OfferPrice();
                    $model->offer_id=$id;
                    $model->item_id=$itemList[$row]->item_id;
                    $model->supplier_id=$supplierList[$col];
                }
                $models[] = $model;
            }
        }
        
        //Load and Validate all the models. Then update if it is existing record
        $post = Yii::$app->request->post();
        if (OfferPrice::loadMultiple($models, $post)
            && OfferItem::loadMultiple($itemList, $post)
                ) 
        {
            if (OfferPrice::validateMultiple($models) && OfferItem::validateMultiple($itemList)) 
            {
                foreach($models as $model)
                {
                    if(!$model->isNewRecord)
                    {
                        if ($model->price == 0) $model->price = null;
                        if ($model->isAttributeChanged('price')) $model->save();
                    }
                }
                foreach($itemList as $offerItem) $offerItem->save();
                return $this->redirect(['view', 'id' => $id]);
            }
        } 
        return $this->render('update', [
            'offer'=>$offer,
            'models' => $models,
            'itemList' => $itemList,
            'supplierList' => $supplierList,
        ]);
    }

    /**
     * Updates an existing OfferContact model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $offer_id
     * @param integer $supplier_id
     * @return mixed
     */
    public function actionUpdateContact($id)
    {
        $models = OfferContact::find()->where(['offer_id'=>$id])->all();
        
        if(count($models)==0) throw new NotFoundHttpException('ไม่มีข้อมูลการติดต่อ');

        if (OfferContact::loadMultiple($models, Yii::$app->request->post()) &&
            OfferContact::validateMultiple($models))
        {
            foreach($models as $model)$model->save();
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('update-contact', [
                'models' => $models,
            ]);
        }
    }

    /**
     * Finds the Offer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Offer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Offer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('ไม่มีใบเปรียบเทียบราคาเลขที่นี้ กรุณาสร้างใบเปรียบเทียบราคา');
        }
    }
    
    protected function findLotModels($lot_id)
    {
        $models = Offer::find()
                ->where(['lot_id'=>$lot_id])
                ->orderBy(['id'=>SORT_DESC])
                ->all();
        if (count($models) > 0) {
            return $models;
        } else {
            throw new NotFoundHttpException('ไม่มีใบเปรียบเทียบราคา กรุณาสร้างใบเปรียบเทียบราคา');
        }
    }
}
