<?php

namespace app\controllers;

use Yii;
use app\models\Lot;
use app\models\LotSite;
use app\models\LotSearch;
use app\models\OfficeSite;
use app\models\Request;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LotController implements the CRUD actions for Lot model.
 */
class LotController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            /*'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['create', 'update', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['manageLot'],
                    ]
                ],
            ],*/
        ];
    }

    /**
     * Lists all Lot models.
     * @return mixed
     */
    public function actionList()
    {
        $searchModel = new LotSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setPagination(['pageSize'=>10]);
        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } 
    
    /**
     * Lists all Lot models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LotSearch();
        
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['LotSearch']['item_type_id'] = 1;
        $dataProvider1 = $searchModel->search($queryParams);
        $queryParams['LotSearch']['item_type_id'] = 2;
        $dataProvider2 = $searchModel->search($queryParams);
        
        return $this->render('index', [
            'dataProvider1' => $dataProvider1,
            'dataProvider2' => $dataProvider2,
        ]);
    }

    /**
     * Displays a single Lot model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Lot model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Lot();
        
        $siteModels = OfficeSite::find()->all();
        $lotSiteModels = [];
        foreach ($siteModels as $i=>$site)
        {
            $lotSiteModels[] = new LotSite();
            $lotSiteModels[$i]->site_id = $site->id;
        }
        
        $post = Yii::$app->request->post();
        if ($model->load($post) && LotSite::loadMultiple($lotSiteModels, $post)) 
        {
            if($model->validate() && LotSite::validateMultiple($lotSiteModels))
            {
                if($model->save())
                {   
                    foreach($lotSiteModels as $lotSiteModel)
                    { 
                        if($lotSiteModel->main_budget <> null && $lotSiteModel->main_budget <> 0)
                        {
                            if ($lotSiteModel->etc_budget == 0) $lotSiteModel->etc_budget = null;
                            $model->link('lotSites',$lotSiteModel);  
                        }
                    }
                    return $this->redirect(['view', 'id' => $model->id]);
                    //in case any error occured after saved the model, we may delete the newly created.
                    //Lot::deleteAll('id=:id',['id'=>$model->id]);
                }
            }
        } 
        //default return for all reject event
        return $this->render('create', [
            'model' => $model,
            'lotSiteModels' => $lotSiteModels,
        ]);
    }

    /**
     * Disabled update for Lot model.
     * Only the LotSite model is update-able 
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        $siteModels = OfficeSite::find()->all();
        $lotSiteModels = [];
        foreach ($siteModels as $i=>$site)
        {
            $existLotSite = LotSite::findOne(['lot_id'=>$model->id,'site_id'=>$site->id]);
            if ($existLotSite == null) 
            {
                $existLotSite = new LotSite();
                $existLotSite->site_id = $site->id;
            }
            $lotSiteModels[] = $existLotSite;
        }

        //No need to validate Lot model since it cannot be updated.
        if (LotSite::loadMultiple($lotSiteModels, Yii::$app->request->post())) 
        {
            if(LotSite::validateMultiple($lotSiteModels))
            {
                foreach($lotSiteModels as $lotSiteModel)
                {
                    //If main_budget has value
                    if ($lotSiteModel->main_budget <> null && $lotSiteModel->main_budget <> 0)
                    {
                        if ($lotSiteModel->etc_budget == 0) $lotSiteModel->etc_budget = null;
                            
                        if ($lotSiteModel->isNewRecord) $model->link('lotSites',$lotSiteModel);
                        else $lotSiteModel->save();
                    }
                    else //main_budget is empty
                    {   
                        //if existing record, then delete
                        if (!$lotSiteModel->isNewRecord) $model->unlink('lotSites',$lotSiteModel, true);  
                    }
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        //default return for all reject event
        return $this->render('update', [
            'model' => $model,
            'lotSiteModels' => $lotSiteModels,
        ]);
    }

    /**
     * Deletes an existing Lot model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model=$this->findModel($id);
        $model->id = -$model->id;
        $model->save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Lot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Lot::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
