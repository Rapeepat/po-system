<?php

namespace app\controllers;

use Yii;
use app\models\Request;
use app\models\RequestSearch;
use app\models\LotSite;
use app\models\Supplier;
use app\models\Lot;
use app\models\LotSearch;
use app\models\Item;
use app\models\PurchaseOrderItemPrice;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RequestController implements the CRUD actions for Request model.
 */
class RequestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    //'create-offer' => ['POST'],
                    'copy-request' => ['POST'],
                ],
            ],
        ];
    }

    /**
    * Get all values from specific key in a multidimensional array
    *
    * @param $key string
    * @param $arr array
    * @return null|string|array
    */
    private function array_value_recursive($key, array $arr){
        $val = array();
        array_walk_recursive($arr, function($v, $k) use($key, &$val){
            if($k == $key) array_push($val, $v);
        });
        return $val;
    }    
    
    /**
     * Lists all Request models.
     * @return mixed
     */
    public function actionIndex($lot_id)
    {
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $lot_id);
        $dataProvider->setPagination(['pageSize'=>-1]);
        $lot=$this->findLot($lot_id);
        
        $models = $dataProvider->models;
        $itemList = array_map(function($model){return $model->item_id;}, $models);
        
        $priceList = [];
        $supplierList = [];
        foreach($itemList as $item_id)
        {
            $itemPrice = PurchaseOrderItemPrice::find()
                    ->joinWith('purchaseOrder')
                    ->where('date_created < :lot_date',['lot_date'=>$lot->lot_date])
                    ->andWhere(['item_id'=>$item_id])
                    ->orderBy(['date_created'=>SORT_DESC,'purchase_order_id'=>SORT_DESC])
                    ->one();
            
            if($itemPrice==null || $itemPrice->price==null) 
            {
                $priceList[$item_id] = '';
                $supplierList[$item_id] = '';
            }
            else 
            {
                $priceList[$item_id] = $itemPrice->price;
                $supplierList[$item_id] = $itemPrice->purchaseOrder->supplier->name;
            }
        }
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'lot' => $lot,
            'priceList' => $priceList,
            'supplierList' => $supplierList,
        ]);
    }

    /**
     * Creates a new Request model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @param integer $lot_id
     * @return mixed
     */
    public function actionCreate($lot_id)
    {
        //Max row to be created in the "create" view 
        $maxRepeat = 10;
        
        //array of models. It's length = (item_count x site_count)
        $models = [];
        
        //Get set of sites
        $lotSites = $this->findLotSites($lot_id);
        $lotSitesCount = count($lotSites);

        $post = Yii::$app->request->post();
        if(Yii::$app->request->isPost)
        {
            $scenario=[];
            foreach($post['Request'] as $key=>$item)
            {
                //If new item is inputed
                if(isset($item['item_id']) && trim($item['item_id']) != '')
                {
                    //If cannot find item
                    if(Item::findOne($item['item_id']) == null)
                    {
                        $itemModel = new Item();
                        $itemModel->name = $item['item_id'];
                        $itemModel->save();
                        $post['Request'][$key]['item_id'] = $itemModel->id;
                    }
                }
            }
            //return '<pre>'.print_r($post,true).'</pre>';
        }
        else 
        {
            $scenario=['scenario' => Request::SCENARIO_CREATE];
        }
        
        //Loop below is to build an array of models of each {Item x Site}.
        //For each item (1 row = 1 item)        
        for($row=0;$row<$maxRepeat;$row++)
        {
            //For each site of current item (1 col = 1 site)
            for($col=0;$col<$lotSitesCount;$col++)
            {
                $model = new Request($scenario);
                $model->lot_id = $lot_id;
                $model->site_id = $lotSites[$col]->site_id;
                $models[] = $model;
            }
        }
        
        //Load and Validate all the models. 
        //Because there is only 1 item inputbox per row and it is input box of the first "site"
        //So, we need to manually load the item_id to the rest "site"s
        if (Request::loadMultiple($models, $post) )
        {
            //Manually load item_id 
            for($row=0;$row<$maxRepeat;$row++)
            {
                for($col=0;$col<$lotSitesCount;$col++)
                {
                    $basei = $row*$lotSitesCount;
                    $i = $basei + $col;
                    $models[$i]->item_id = $models[$basei]->item_id;
                }
            }
            
            //Validate and check if the model has "amount", if so create a new record.
            if (Request::validateMultiple($models))
            {
                foreach($models as $model)
                {
                    if($model->item_id<>null)
                    {
                        if($model->amount == null) $model->amount = 0;
                        $model->save();
                    }
                }
                return $this->redirect(['index', 'lot_id' => $lot_id]);
            }
        } 
        
        return $this->render('create', [
            'models' => $models,
            'maxRepeat' => $maxRepeat,
            'lotSitesCount' => $lotSitesCount,
        ]);
    }

    /**
     * Updates an existing Request model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $lot_id
     * @return mixed
     */
    public function actionUpdate($lot_id)
    {
        //array of models. It's length = (item_count x site_count)
        $models = [];
        
        //Get set of items
        $itemList = Request::find()
                ->select(['item_id','sum(amount) as sum_amount'])
                ->where(['lot_id'=>$lot_id])
                ->groupBy('item_id')
                ->orderBy('CASE
                    WHEN (sum(amount) > 0)
                        THEN 0
                    ELSE 1
                END, item_id')
                ->all();
        $itemCount = count($itemList);
        
        //Get set of sites
        $lotSites = $this->findLotSites($lot_id);
        $lotSitesCount = count($lotSites);
        
        //Loop below is to get a model of each {Item x Site}.
        //If the request does not exit, then create new request model.
        //Then the "update"/"delete"/"create" action will be taken once models are loaded and validated
        
        //For each item (1 row = 1 item)
        for($row=0;$row<$itemCount;$row++)
        {
            //For each site of current item (1 col = 1 site)
            for($col=0;$col<$lotSitesCount;$col++)
            {
                $model = Request::findOne([
                    'lot_id'=>$lot_id,
                    'site_id'=>$lotSites[$col]->site_id,
                    'item_id'=>$itemList[$row]->item_id,
                ]);
                if($model==null) 
                {
                    $model=new Request();
                    $model->lot_id=$lot_id;
                    $model->item_id=$itemList[$row]->item_id;
                    $model->site_id=$lotSites[$col]->site_id;
                }
                if ($model->amount==null)$model->amount=0;
                $models[] = $model;
            }
            
        }
        
        //Load and Validate all the models. Sets of actiops will be taken as below
        //1. Update current model     - if it is "existing record" 
        //3. Create new request model - if it is "new record"      
        if (Request::loadMultiple($models, Yii::$app->request->post())) 
        {
            if (Request::validateMultiple($models)) 
            {
                foreach($models as $model)
                {
                    if($model->amount == null) $model->amount = 0;
                    if($model->isNewRecord)
                    {
                        $model->save();
                    }
                    else if ($model->oldAttributes['amount'] !== $model->amount) 
                    {
                        $model->save();
                    }
                }
                return $this->redirect(['index', 'lot_id' => $lot_id]);
            }
        } 
        return $this->render('update', [
            'models' => $models,
            'maxRow' => $itemCount,
            'lotSites' => $lotSites,
        ]);
    }

    /**
     * Selections of existing Sites before create offers. It will redirect to Offer/Create

     * @param integer $lot_id
     * @return mixed
     */
    public function actionSelectSite($lot_id)
    {
        $models = Request::find()->select('site_id')->distinct()->where(['lot_id'=>$lot_id])->all();
        return $this->render('_select-site', [
            'models' => $models,
        ]);
    }
    
    /**
     * Selections of Suppliers before passing to Offer/Create. It will redirect to Offer/Create
     * with post parameters in array format of ["Request"][item_id][supplier_id]
     * @param integer $lot_id
     * @return mixed
     */
    public function actionCreateOffer($lot_id)
    {   
        //Get all selected Sites (in the modal form)
        $selectedSites = Yii::$app->request->get('Site'); //Site format is "site_id" => selected(1)/unselected(0)
        $selectedSites = array_diff($selectedSites, [0]);  //drop unselected. "site_id" => 0
        $selectedSites = array_keys($selectedSites);       //get all keys. Which is all sites that are selected
        
        $shipping_site_id = Yii::$app->request->get('shipping_site_id');
        
        //Get set of items
        $itemList = Request::find()
            ->select('item_id')->distinct()
            ->where(['lot_id'=>$lot_id, 'site_id'=>$selectedSites])
            ->andWhere('amount > 0')
            ->all();
        $itemCount = count($itemList);
        
        if($itemCount==0) throw new NotFoundHttpException("ไม่มีรายการสินค้าที่ถูกสั่งซื้อ กรุณาสร้างคำขอสั่งซื้ออย่างน้อย 1 รายการ");
        
        //Get set of sites
        $supplierList = Supplier::find()->all();
        $supplierCount = count($supplierList);
        
        return $this->render('create-offer', [
            'itemList' => $itemList,
            'supplierList' => $supplierList,
            'siteList' => $selectedSites,
            'shipping_site_id' => $shipping_site_id,
        ]);
    }

    /**
     * Deletes an existing Request model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $lot_id
     * @param integer $site_id
     * @param integer $item_id
     * @return mixed
     */
    public function actionDelete($lot_id, $item_id)
    {
        $this->findModel($lot_id, $item_id); //leave it as-is just to handle not found event
        
        Request::deleteAll(['lot_id' => $lot_id, 'item_id' => $item_id]);

        return $this->redirect(['index','lot_id'=>$lot_id]);
    }
    
    /**
     * Lists all Lot to be copy.
     * @return mixed
     */
    public function actionCopyRequestSelect($lot_id)
    {
        $lotModel = $this->findLot($lot_id);
        $searchModel = new LotSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['LotSearch']['item_type_id'] = $lotModel->item_type_id;
        $dataProvider = $searchModel->search($queryParams, $lot_id);
        $dataProvider->setPagination(['pageSize'=>-1]);
        
        return $this->render('copy-request-select', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'lot' => $lotModel,
        ]);
    }
    
      /**
     * Copy all request from to to lot id.
     * @return mixed
     */
    public function actionCopyRequest($from_lot_id, $to_lot_id)
    {
        $models = Request::find()->where(['lot_id'=>$from_lot_id])->all();
        foreach($models as $model)
        {
            //if to side does not exist.
            if(!Request::find()->where(['lot_id'=>$to_lot_id,'site_id'=>$model->site_id,'item_id'=>$model->item_id])->exists())
            {
                $newModel = new Request();
                $newModel->lot_id = $to_lot_id;
                $newModel->site_id = $model->site_id;
                $newModel->item_id = $model->item_id;
                $newModel->amount = 0;
                $newModel->save();
            }
        }
        return $this->redirect(['index','lot_id'=>$to_lot_id]);
    }

    /**
     * Finds the Request model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $lot_id 
     * @param integer $item_id
     * @return Request the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($lot_id, $item_id)
    {
        $model = Request::findAll(['lot_id' => $lot_id, 'item_id' => $item_id]);
        if (count($model)>0) {
            return $model;
        } else {
           throw new NotFoundHttpException('ไม่พบหน้าที่ต้องการ กรุณาลองใหม่อีกครั้ง');
        }
    }
    
    protected function findLotSites($lot_id)
    {
        $lotSites = LotSite::find()->where('lot_id = :lot_id',['lot_id'=>$lot_id])->all();
        if (count($lotSites)>0)
        {
            return $lotSites;
        }
        else
        {
            throw new NotFoundHttpException('ไม่พบงบประมาณ กรุณาเพิ่มงบประมาณรายหน่วยงาน อย่างน้อย 1 หน่วยงาน');
        }
    }
    
     /**
     * Finds the Lot model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Lot the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findLot($id)
    {
        if (($model = Lot::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException("ไม่พบหน้าที่ต้องการ กรุณาลองใหม่อีกครั้ง");
        }
    }
}
