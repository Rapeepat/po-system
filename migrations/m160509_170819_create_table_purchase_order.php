<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_purchase_order`.
 */
class m160509_170819_create_table_purchase_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if (Yii::$app->db->schema->getTableSchema('purchase_order') !== null)
        {
            $this->down();
        }

        $this->createTable('purchase_order', [
            'item_lot_id' => $this->integer()->notNull(),
            'supplier_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'printed_at' => $this->timestamp(),
            'printed_by' => $this->integer(),
            'po_number' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(0),
            'PRIMARY KEY(item_lot_id, supplier_id)'
        ]);

        $this->addForeignKey(
            'fk-purchase_order_created_by',
            'purchase_order',
            'created_by',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-purchase_order_printed_by',
            'purchase_order',
            'printed_by',
            'user',
            'id',
            'SET NULL'
        );

         $this->addForeignKey(
            'fk-purchase_order_item_lot_id',
            'purchase_order',
            'item_lot_id',
            'item_lot',
            'id',
            'RESTRICT'
        );

         $this->addForeignKey(
            'fk-purchase_order_supplier_id',
            'purchase_order',
            'supplier_id',
            'supplier',
            'id',
            'RESTRICT'
        );

         $this->addCommentOnColumn(
            'purchase_order',                     //table
            'status',                           //column
            'Later create lookup table. For now 0=Created 1=Printed'   //comment
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('purchase_order');
    }
}
