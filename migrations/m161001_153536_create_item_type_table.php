<?php

use yii\db\Migration;

/**
 * Handles the creation for table `item_type_table`.
 */
class m161001_153536_create_item_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->safeDown();
        
        
        $this->createTable('item_type', [
            'id' => $this->primaryKey(),
            'detail' => $this->string()
        ]);
        
        $this->batchInsert(
            'item_type',
            ['id','detail'],
            [
                ['1','ของใช้สำนักงาน'],
                ['2','ของใช้สิ้นเปลือง']
            ]
        );
        
        $this->addColumn('item', 'item_type_id', $this->integer());
        $this->addForeignKey('fk-item-item_type_id', 'item', 'item_type_id', 'item_type', 'id');
        
        $this->addColumn('lot', 'item_type_id', $this->integer()->notNull()->defaultValue(1));
        $this->addForeignKey('fk-lot-item_type_id', 'lot', 'item_type_id', 'item_type', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {   
        if (Yii::$app->db->schema->getTableSchema('item')->getColumn('item_type_id') !== null)
        {
            $this->dropForeignKey('fk-item-item_type_id','item');
            $this->dropIndex('fk-item-item_type_id','item');
            $this->dropColumn('item', 'item_type_id');
        }
        
             
        if (Yii::$app->db->schema->getTableSchema('lot')->getColumn('item_type_id') !== null)
        {
            $this->dropForeignKey('fk-lot-item_type_id','lot');
            $this->dropIndex('fk-lot-item_type_id','lot');   
            $this->dropColumn('lot', 'item_type_id');    
        }
        
        if (Yii::$app->db->schema->getTableSchema('item_type') !== null) 
        {
            $this->dropTable('item_type');
        }
    }
}
