<?php

use yii\db\Migration;

class m170205_142342_drop_address_2 extends Migration
{
    public function up()
    {
        $this->dropColumn('supplier', 'address_2');
        
        $this->dropColumn('purchase_order', 'supplier_address_2');
        $this->dropColumn('purchase_order', 'shipping_address_2');
        
        $this->dropColumn('office_site', 'address_2');
        
        $this->dropColumn('offer_contact', 'supplier_address_2');
        $this->dropColumn('offer_contact', 'shipping_address_2');
    }

    public function down()
    {
        echo "m170205_142342_drop_address_2 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
