<?php

use yii\db\Migration;

class m160514_151859_move_status_to_lot_item extends Migration
{
    public function up()
    {
    	
    	//drop fk of item_ request
    	$this->dropForeignKey('fk-item_request_requested_by', 'item_request');
    	$this->dropForeignKey('fk-item_request_accepted_by', 'item_request');
    	$this->dropForeignKey('fk-item_request_offer_created_by', 'item_request');

    	$this->dropIndex('fk-item_request_requested_by', 'item_request');
    	$this->dropIndex('fk-item_request_accepted_by', 'item_request');
    	$this->dropIndex('fk-item_request_offer_created_by', 'item_request');

    	//drop item_request columns
    	$this->dropColumn('item_request', 'requested_at');
    	$this->dropColumn('item_request', 'requested_by');
    	$this->dropColumn('item_request', 'accepted_at');
    	$this->dropColumn('item_request', 'accepted_by');
    	$this->dropColumn('item_request', 'offer_created_at');
    	$this->dropColumn('item_request', 'offer_created_by');

	$this->dropColumn('item_request', 'status');
    	
    	    	
    	//drop fk of item_offer
        	$this->dropForeignKey('fk-item_offer_created_by', 'item_offer');
    	$this->dropForeignKey('fk-item_offer_sent_by', 'item_offer');
    	$this->dropForeignKey('fk-item_offer_received_by', 'item_offer');
    	$this->dropForeignKey('fk-item_offer_accepted_by', 'item_offer');
    	$this->dropForeignKey('fk-item_offer_preapproved_by', 'item_offer');

    	$this->dropIndex('fk-item_offer_created_by', 'item_offer');
    	$this->dropIndex('fk-item_offer_sent_by', 'item_offer');
    	$this->dropIndex('fk-item_offer_received_by', 'item_offer');
    	$this->dropIndex('fk-item_offer_accepted_by', 'item_offer');
    	$this->dropIndex('fk-item_offer_preapproved_by', 'item_offer');

    	//drop item_offer columns
    	$this->dropColumn('item_offer', 'created_at');
    	$this->dropColumn('item_offer', 'created_by');
    	$this->dropColumn('item_offer', 'sent_at');
    	$this->dropColumn('item_offer', 'sent_by');
    	$this->dropColumn('item_offer', 'received_at');
    	$this->dropColumn('item_offer', 'received_by');
    	$this->dropColumn('item_offer', 'accepted_at');
    	$this->dropColumn('item_offer', 'accepted_by');
    	$this->dropColumn('item_offer', 'preapproved_at');
    	$this->dropColumn('item_offer', 'preapproved_by');

    	$this->dropColumn('item_offer', 'status');

            //drop fk of purchase_order
            $this->dropForeignKey('fk-purchase_order_created_by', 'purchase_order');
            $this->dropForeignKey('fk-purchase_order_printed_by', 'purchase_order');

            $this->dropIndex('fk-purchase_order_created_by', 'purchase_order');
            $this->dropIndex('fk-purchase_order_printed_by', 'purchase_order');

            //drop purchase_order columns
            $this->dropColumn('purchase_order', 'created_at');
            $this->dropColumn('purchase_order', 'created_by');
            $this->dropColumn('purchase_order', 'printed_at');
            $this->dropColumn('purchase_order', 'printed_by');

            $this->dropColumn('purchase_order', 'status');
        
    	//item lot
    	$this->dropColumn('item_lot','last_maint_dt');

	$this->addColumn('item_lot', 'status', $this->integer()->notNull()->defaultValue(0));

    	$this->addColumn('item_lot', 'requested_at', $this->timestamp());
	$this->addColumn('item_lot', 'requested_by', $this->integer());
	$this->addColumn('item_lot', 'request_accepted_at', $this->timestamp());
	$this->addColumn('item_lot', 'request_accepted_by', $this->integer());
	$this->addColumn('item_lot', 'offer_created_at', $this->timestamp());
	$this->addColumn('item_lot', 'offer_created_by', $this->integer());
	$this->addColumn('item_lot', 'offer_sent_at', $this->timestamp());
	$this->addColumn('item_lot', 'offer_sent_by', $this->integer());
	$this->addColumn('item_lot', 'offer_received_at', $this->timestamp());
	$this->addColumn('item_lot', 'offer_received_by', $this->integer());
	$this->addColumn('item_lot', 'offer_accepted_at', $this->timestamp());
	$this->addColumn('item_lot', 'offer_accepted_by', $this->integer());
	$this->addColumn('item_lot', 'preapproved_at', $this->timestamp());
	$this->addColumn('item_lot', 'preapproved_by', $this->integer());
             $this->addColumn('item_lot', 'po_created_at', $this->timestamp());
             $this->addColumn('item_lot', 'po_created_by', $this->integer());
             $this->addColumn('item_lot', 'po_printed_at', $this->timestamp());
             $this->addColumn('item_lot', 'po_printed_by', $this->integer());


	$this->addForeignKey(
		'fk-item_lot_requested_by',
		'item_lot','requested_by',
		'user','id',
		'SET NULL'
	);
	$this->addForeignKey(
		'fk-item_lot_request_accepted_by',
		'item_lot','request_accepted_by',
		'user','id',
		'SET NULL'
	);
	$this->addForeignKey(
		'fk-item_lot_offer_created_by',
		'item_lot','offer_created_by',
		'user','id',
		'SET NULL'
	);
	$this->addForeignKey(
		'fk-item_lot_offer_sent_by',
		'item_lot','offer_sent_by',
		'user','id',
		'SET NULL'
	);
	$this->addForeignKey(
		'fk-item_lot_offer_received_by',
		'item_lot','offer_received_by',
		'user','id',
		'SET NULL'
	);
	$this->addForeignKey(
		'fk-item_lot_offer_accepted_by',
		'item_lot','offer_accepted_by',
		'user','id',
		'SET NULL'
	);
	$this->addForeignKey(
		'fk-item_lot_preapproved_by',
		'item_lot','preapproved_by',
		'user','id',
		'SET NULL'
	);
        $this->addForeignKey(
            'fk-item_lot_po_created_by',
            'item_lot','po_created_by',
            'user','id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-item_lot_po_printed_by',
            'item_lot','po_printed_by',
            'user','id',
            'SET NULL'
        );
        
            $this->addCommentOnColumn(
                    'item_lot',                     //table
                    'request_accepted_at',                           //column
                    'timestamp for either accept or reject'   //comment
            );
            $this->addCommentOnColumn(
                    'item_lot',                     //table
                    'offer_accepted_at',                           //column
                    'timestamp for either accept or reject'   //comment
            );

            $this->addCommentOnColumn(
                    'item_lot',                     //table
                    'preapproved_at',                           //column
                    'timestamp for either approve or deny'   //comment
            );
    }

    public function down()
    {
            //item request
	$this->addColumn('item_request', 'requested_at', $this->timestamp());
       	$this->addColumn('item_request', 'requested_by', $this->integer());
	$this->addColumn('item_request', 'accepted_at', $this->timestamp());
	$this->addColumn('item_request', 'accepted_by', $this->integer());
	$this->addColumn('item_request', 'offer_created_at', $this->timestamp());
	$this->addColumn('item_request', 'offer_created_by', $this->integer());
	$this->addColumn('item_request', 'status', $this->integer()->notNull()->defaultValue(0));

	$this->addForeignKey(
		'fk-item_request_requested_by',
		'item_request','requested_by',
		'user','id',
		'SET NULL'
	);
	$this->addForeignKey(
		'fk-item_request_accepted_by',
		'item_request','accepted_by',
		'user','id',
		'SET NULL'
        	);
        	$this->addForeignKey(
            		'fk-item_request_offer_created_by',
            		'item_request','offer_created_by',
            		'user','id',
            		'SET NULL'
        	);
         	$this->addCommentOnColumn(
            		'item_request',                     //table
            		'accepted_at',                           //column
            		'timestamp for whether Accepted or Rejected'   //comment
        	);
        	$this->addCommentOnColumn(
            		'item_request',                     //table
            		'status',                           //column
            		'Later create lookup table. For now 0=Requested 1=Accepted 2=Rejected 3=Offer created'   //comment
        	);

         	//item offer
         	$this->addColumn('item_offer', 'created_at', $this->timestamp());
         	$this->addColumn('item_offer', 'created_by', $this->integer());
         	$this->addColumn('item_offer', 'sent_at', $this->timestamp());
         	$this->addColumn('item_offer', 'sent_by', $this->integer());
         	$this->addColumn('item_offer', 'received_at', $this->timestamp());
         	$this->addColumn('item_offer', 'received_by', $this->integer());
         	$this->addColumn('item_offer', 'accepted_at', $this->timestamp());
         	$this->addColumn('item_offer', 'accepted_by', $this->integer());
         	$this->addColumn('item_offer', 'preapproved_at', $this->timestamp());
         	$this->addColumn('item_offer', 'preapproved_by', $this->integer());
         	$this->addColumn('item_offer', 'status', $this->integer()->notNull()->defaultValue(0));
         
        	$this->addForeignKey(
	            	'fk-item_offer_created_by',
            		'item_offer','created_by',
            		'user','id',
            		'SET NULL'
        	);
        	$this->addForeignKey(
            		'fk-item_offer_sent_by',
            		'item_offer','sent_by',
            		'user','id',
            		'SET NULL'
        	);
        	$this->addForeignKey(
            		'fk-item_offer_received_by',
            		'item_offer','received_by',
            		'user','id',
	            	'SET NULL'
        	);
        	$this->addForeignKey(
            		'fk-item_offer_accepted_by',
	            	'item_offer','accepted_by',
            		'user','id',
            		'SET NULL'
        	);
	$this->addForeignKey(
		'fk-item_offer_preapproved_by',
		'item_offer','preapproved_by',
		'user','id',
		'SET NULL'
	);
	$this->addCommentOnColumn(
		'item_offer',                     //table
		'accepted_at',                           //column
		'timestamp for whether Accepted or Rejected'   //comment
	);
	$this->addCommentOnColumn(
		'item_offer',                     //table
		'status',                           //column
		'Later create lookup table. For now 0=Created 1=Sent 2=Received 3=Accepted 4=Rejected 5=PreApproved'   //comment
	);

              //purchase order
             $this->addColumn('purchase_order', 'created_at', $this->timestamp());
             $this->addColumn('purchase_order', 'created_by', $this->integer());
             $this->addColumn('purchase_order', 'printed_at', $this->timestamp());
             $this->addColumn('purchase_order', 'printed_by', $this->integer());
             $this->addColumn('purchase_order', 'status', $this->integer()->notNull()->defaultValue(0));
         
             $this->addForeignKey(
                    'fk-purchase_order_created_by',
                    'purchase_order','created_by',
                    'user','id',
                    'SET NULL'
             );
             $this->addForeignKey(
                    'fk-purchase_order_printed_by',
                    'purchase_order','printed_by',
                    'user','id',
                    'SET NULL'
             );
             $this->addCommentOnColumn(
                    'purchase_order',                     //table
                    'status',                           //column
                    'Later create lookup table. For now 0=Created 1=Printed'   //comment
              );



	//item lot
        	$this->dropForeignKey('fk-item_lot_requested_by', 'item_lot');
    	$this->dropForeignKey('fk-item_lot_request_accepted_by', 'item_lot');
    	$this->dropForeignKey('fk-item_lot_offer_created_by', 'item_lot');
    	$this->dropForeignKey('fk-item_lot_offer_sent_by', 'item_lot');
    	$this->dropForeignKey('fk-item_lot_offer_received_by', 'item_lot');
    	$this->dropForeignKey('fk-item_lot_offer_accepted_by', 'item_lot');
    	$this->dropForeignKey('fk-item_lot_preapproved_by', 'item_lot');
            $this->dropForeignKey('fk-item_lot_po_created_by', 'item_lot');
            $this->dropForeignKey('fk-item_lot_po_printed_by', 'item_lot');

    	$this->dropIndex('fk-item_lot_requested_by', 'item_lot');
    	$this->dropIndex('fk-item_lot_request_accepted_by', 'item_lot');
    	$this->dropIndex('fk-item_lot_offer_created_by', 'item_lot');
    	$this->dropIndex('fk-item_lot_offer_sent_by', 'item_lot');
    	$this->dropIndex('fk-item_lot_offer_received_by', 'item_lot');
    	$this->dropIndex('fk-item_lot_offer_accepted_by', 'item_lot');
    	$this->dropIndex('fk-item_lot_preapproved_by', 'item_lot');
            $this->dropIndex('fk-item_lot_po_created_by', 'item_lot');
            $this->dropIndex('fk-item_lot_po_printed_by', 'item_lot');

	$this->addColumn('item_lot','last_maint_dt',$this->timestamp());

	$this->dropColumn('item_lot', 'status', $this->integer()->notNull()->defaultValue(0));

	$this->dropColumn('item_lot', 'requested_at');
	$this->dropColumn('item_lot', 'requested_by');
	$this->dropColumn('item_lot', 'request_accepted_at');
	$this->dropColumn('item_lot', 'request_accepted_by');
	$this->dropColumn('item_lot', 'offer_created_at');
	$this->dropColumn('item_lot', 'offer_created_by');
	$this->dropColumn('item_lot', 'offer_sent_at');
	$this->dropColumn('item_lot', 'offer_sent_by');
	$this->dropColumn('item_lot', 'offer_received_at');
	$this->dropColumn('item_lot', 'offer_received_by');
	$this->dropColumn('item_lot', 'offer_accepted_at');
	$this->dropColumn('item_lot', 'offer_accepted_by');
	$this->dropColumn('item_lot', 'preapproved_at');
	$this->dropColumn('item_lot', 'preapproved_by');
            $this->dropColumn('item_lot', 'po_created_at');
            $this->dropColumn('item_lot', 'po_created_by');
            $this->dropColumn('item_lot', 'po_printed_at');
            $this->dropColumn('item_lot', 'po_printed_by');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
