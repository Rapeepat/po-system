<?php

use yii\db\Migration;

class m160507_060703_create_table_item_lot extends Migration
{
    public function up()
    {
        if (Yii::$app->db->schema->getTableSchema('item_lot') !== null)
        {
            $this->down();
        }

        $this->createTable('item_lot', [
            'id' => $this->primaryKey(),
            'last_maint_dt' => 'timestamp on update current_timestamp',
            'item_id' => $this->integer()->notNull(),
            'lot_id' => $this->integer()->notNull()
        ]);

         $this->addForeignKey(
            'fk-item_lot_item_id',
            'item_lot',
            'item_id',
            'item',
            'id',
            'RESTRICT'
        );

         $this->addForeignKey(
            'fk-item_lot_lot_id',
            'item_lot',
            'lot_id',
            'lot',
            'id',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropTable('item_lot');
    }
}
