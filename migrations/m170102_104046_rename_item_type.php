<?php

use yii\db\Migration;

class m170102_104046_rename_item_type extends Migration
{
    public function up()
    {   
        $this->addColumn('item_type', 'main_type', $this->integer()->null()->after('id'));
        $this->addForeignKey('fk-item_type-main_type', 'item_type', 'main_type', 'item_type', 'id');

        $this->update('item_type', ['detail'=>'หมวดทรัพย์สิน'], ['id'=>1]);
        $this->update('item_type', ['detail'=>'หมวดค่าใช้จ่าย'], ['id'=>2]);
        
        $this->batchInsert(
            'item_type',
            ['id','main_type','detail'],
            [
                //[1, null, 'หมวดทรัพย์สิน'],
                [1830, 1, '1830 - เครื่องใช้สำนักงาน'],
                [1832, 1, '1832 - คอมพิวเตอร์และอุปกรณ์'],
                [1833, 1, '1833 - เครื่องมือเครื่องใช้'],
                [1834, 1, '1834 - ยานพาหนะ'],
                [1835, 1, '1835 - สินทรัพย์ไม่มีตัวตน'],
                //[2, null, 'หมวดค่าใช้จ่าย'],
                [5707, 2, '5707 ต้นทุนบริการ - ค่าสวัสดิการพนักงาน'],
                [5717, 2, '5717 ต้นทุนบริการ - วัสดุสิ้นเปลือง'],
                [5722, 2, '5722 ต้นทุนบริการ - ค่าใช้จ่ายเกี่ยวกับคอมพิวเตอร์'],
                [5723, 2, '5723 ต้นทุนบริการ - ค่าวัสดุสำนักงาน'],
                [5806, 2, '5806 การบริหาร - ค่าสวัสดิการพนักงาน'],
                [5829, 2, '5829 การบริหาร - ค่าใช้จ่ายเกี่ยวกับคอมพิวเตอร์'],
                [5848, 2, '5848 การบริหาร - วัสดุสำนักงาน'],
            ]
        );
    }

    public function down()
    {   
        $this->delete('item_type','main_type is not null');
        
        $this->dropForeignKey('fk-item_type-main_type', 'item_type');
        $this->dropIndex('fk-item_type-main_type', 'item_type');
        $this->dropColumn('item_type', 'main_type');
        
        
        $this->update('item_type', ['detail'=>'ของใช้สำนักงาน'], ['id'=>1]);
        $this->update('item_type', ['detail'=>'ของใช้สิ้นเปลือง'], ['id'=>2]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
