<?php

use yii\db\Migration;

class m160526_152511_add_column_for_item_offer extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'item_lot', 
            'selected_supplier', 
            $this->integer()
        );
        $this->addColumn(
            'item_offer', 
            'last_maint_by', 
            $this->integer()
        );
        $this->addColumn(
            'item_offer',
            'last_maint_at',
            $this->timestamp()
        );
        
        $this->addForeignKey(
            'fk-item_lot_selected_supplier', 
            'item_lot', 
            'selected_supplier', 
            'supplier', 
            'id', 
            'RESTRICT', 
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-item_offer_last_maint_by',
            'item_offer',
            'last_maint_by',
            'user',
            'id',
            'SET NULL'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-item_lot_selected_supplier', 'item_lot');
        $this->dropIndex('fk-item_lot_selected_supplier', 'item_lot');
        $this->dropColumn('item_lot', 'selected_supplier');
        
        $this->dropForeignKey('fk-item_offer_last_maint_by', 'item_offer');
        $this->dropIndex('fk-item_offer_last_maint_by', 'item_offer');
        $this->dropColumn('item_offer', 'last_maint_by');
        $this->dropColumn('item_offer', 'last_maint_at');
    }

}
