<?php

use yii\db\Migration;

class m170102_101607_add_column_etc_budget_to_lot extends Migration
{
    public function up()
    {
        $this->addColumn(
            'lot_site', 
            'etc_budget', 
            $this->decimal(10,2)
        );
        
        $this->renameColumn('lot_site','max_budget','main_budget');
    }

    public function down()
    {
        $this->dropColumn('lot_site', 'etc_budget');
        $this->renameColumn('lot_site','main_budget','max_budget');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
