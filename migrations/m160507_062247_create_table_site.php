<?php

use yii\db\Migration;

class m160507_062247_create_table_site extends Migration
{
    public function up()
    {
        if (Yii::$app->db->schema->getTableSchema('office_site') !== null)
        {
            $this->down();
        }

        $this->createTable('office_site', [
            'id' => $this->primaryKey(),
            'last_maint_dt' => 'timestamp on update current_timestamp',
            'last_maint_id' => $this->integer(),
            'head_site_id'=> $this->integer(),
            'name' => $this->string()->notNull(),
            'address_1' => $this->string()->notNull(),
            'address_2' => $this->string(),
            'tel' => $this->string(10),
            'fax' => $this->string(10),
            'shipping_flag' => $this->integer()->defaultValue(0)->notNull(),
            'contact_name' => $this->string(),
            'contact_tel' => $this->string(10),
            'contact_email' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-site_maint_id',
            'office_site',
            'last_maint_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->addForeignKey(
            'fk-head_site_site_id',
            'office_site',
            'head_site_id',
            'office_site',
            'id',
            'RESTRICT'
        );

        //only Yii 2.08
        $this->addCommentOnColumn(
            'office_site',                     //table
            'shipping_flag',            //column
            '0=site addr, 1=head addr'  //comment
        );
    }

    public function down()
    {
        $this->dropTable('office_site');
    }
}
