<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_item_request`.
 */
class m160507_070450_create_table_item_request extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if (Yii::$app->db->schema->getTableSchema('item_request') !== null)
        {
            $this->down();
        }

        $this->createTable('item_request', [
            'item_lot_id' => $this->integer()->notNull(),
            'site_id' => $this->integer()->notNull(),
            'requested_at' => $this->timestamp(),
            'requested_by' => $this->integer(),
            'accepted_at' => $this->timestamp(),
            'accepted_by' => $this->integer(),
            'offer_created_at' => $this->timestamp(),
            'offer_created_by' => $this->integer(),
            'qty' => $this->integer(),
            'status' => $this->integer()->notNull()->defaultValue(0),
            'PRIMARY KEY(item_lot_id, site_id)'
        ]);

        // not sure do we need to create index for compound key
        /*$this->createIndex(
            'idx-item_request-item_lot_id',
            'item_request',
            'item_lot_id'
        );
        //and so on for site_id
        */

         $this->addForeignKey(
            'fk-item_request_requested_by',
            'item_request',
            'requested_by',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-item_request_accepted_by',
            'item_request',
            'accepted_by',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-item_request_offer_created_by',
            'item_request',
            'offer_created_by',
            'user',
            'id',
            'SET NULL'
        );

         $this->addForeignKey(
            'fk-item_request_item_lot_id',
            'item_request',
            'item_lot_id',
            'item_lot',
            'id',
            'RESTRICT'
        );

         $this->addForeignKey(
            'fk-item_request_site_id',
            'item_request',
            'site_id',
            'office_site',
            'id',
            'RESTRICT'
        );

         $this->addCommentOnColumn(
            'item_request',                     //table
            'status',                           //column
            'Later create lookup table. For now 0=Requested 1=Accepted 2=Rejected 3=Offer created'   //comment
        );

         $this->addCommentOnColumn(
            'item_request',                     //table
            'accepted_at',                           //column
            'timestamp for whether Accepted or Rejected'   //comment
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('item_request');
    }
}
