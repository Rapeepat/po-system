<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_offer_contact`.
 */
class m161108_152802_create_table_offer_contact extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if (Yii::$app->db->schema->getTableSchema('offer_contact') !== null) 
        {
            $this->dropTable('offer_contact');
        }
        
        $this->createTable('offer_contact', [
            'offer_id' => $this->integer()->notNull(),
            'supplier_id' => $this->integer()->notNull(),
            
            'supplier_address_1' => $this->string()->notNull(),
            'supplier_address_2' => $this->string(),
            'supplier_tel' => $this->string(10),
            'supplier_fax' => $this->string(10),
            'supplier_contact_name' => $this->string(),
            
            'shipping_address_1' => $this->string()->notNull(),
            'shipping_address_2' => $this->string(),
            'shipping_contact_name' => $this->string(),
            'shipping_tel' => $this->string(10),
            
            'PRIMARY KEY(offer_id, supplier_id)'
        ]);
        
        $this->addForeignKey('fk-offer_contact-offer_id', 'offer_contact', 'offer_id', 'offer', 'id');
        $this->addForeignKey('fk-offer_contact-supplier_id', 'offer_contact', 'supplier_id', 'supplier', 'id');
        
        $this->dropColumn('office_site', 'contact_tel');
        $this->dropColumn('office_site', 'contact_email');
        
        $this->dropColumn('supplier', 'contact_tel');
        $this->dropColumn('supplier', 'contact_email');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        if (Yii::$app->db->schema->getTableSchema('offer_contact') !== null) 
        {
            $this->dropTable('offer_contact');
        }
        
        $this->addColumn('office_site', 'contact_tel', $this->string(10));
        $this->addColumn('office_site', 'contact_email', $this->string());
        
        $this->addColumn('supplier', 'contact_tel', $this->string(10));
        $this->addColumn('supplier', 'contact_email', $this->string());
    }
}
