<?php

use yii\db\Migration;

class m160428_121314_create_table_item extends Migration
{
    public function up()
    {
        if (Yii::$app->db->schema->getTableSchema('item') !== null)
        {
            $this->down();
        }

        $this->createTable('item', [
            'id' => $this->primaryKey(),
            'last_maint_dt' => 'timestamp on update current_timestamp',
            'last_maint_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'general_name' => $this->string(),
            'detail' => $this->string(),
            'display_code' => $this->string(),
            'unit' => $this->string(),
            'brand' => $this->string(),
            'model' => $this->string(),
            'color' => $this->string(),
            'dim_1' => $this->decimal(10,2),
            'dim_2' => $this->decimal(10,2),
            'dim_3' => $this->decimal(10,2),
            'dim_unit' => $this->string()
        ]);

         $this->addForeignKey(
            'fk-item_maint_id',
            'item',
            'last_maint_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    public function down()
    {
        $this->dropTable('item');
    }
}
