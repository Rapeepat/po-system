<?php

use yii\db\Migration;

class m170202_141407_add_po_void extends Migration
{
    public function up()
    {
        $this->addColumn('purchase_order', 'void', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('purchase_order', 'void');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
