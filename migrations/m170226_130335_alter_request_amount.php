<?php

use yii\db\Migration;

class m170226_130335_alter_request_amount extends Migration
{
    public function up()
    {
        $this->alterColumn('request', 'amount', $this->integer()->defaultValue(0)->notNull());
    }

    public function down()
    {
        echo "m170226_130335_alter_request_amount cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
