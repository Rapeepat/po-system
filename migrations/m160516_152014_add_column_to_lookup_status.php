<?php

use yii\db\Migration;

/**
 * Handles adding column to table `lookup_status`.
 */
class m160516_152014_add_column_to_lookup_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-item_lot_lookup_status', 'item_lot');
        $this->dropIndex('fk-item_lot_lookup_status', 'item_lot');
        $this->delete('lookup_status');
        
        $this->addColumn('lookup_status','item_lot_column',$this->string()->notNull());
               
        $this->batchInsert(
            'lookup_status', 
            ['id','table','value','item_lot_column'], 
            [
                [0,'item_request',   'คำสั่งซื้อถูกสร้าง',     'requested'],
                [1,'item_request',   'คำสั่งซื้อถูกยอมรับ',         'request_accepted'],
                [2,'item_request',   'คำสั่งซื้อถูกปฏิเสธ',          'request_accepted'],
                [3,'item_offer',     'คำขอเสนอราคาถูกสร้าง',     'offer_created'],
                [4,'item_offer',     'ส่งคำขอไปยังร้านค้า','offer_sent'],
                [5,'item_offer',     'ร้านค้าตอบรับคำขอ', 'offer_received'],
                [6,'item_offer',     'การเสนอราคาถูกยอมรับ',          'offer_accepted'],
                [7,'item_offer',     'การเสนอราคาถูกปฏิเสธ',          'offer_accepted'],
                [8,'item_offer',     'อนุมัติ',          'preapproved'],
                [9,'item_offer',     'ไม่อนุมัติ',        'preapproved'],
                [10,'purchase_order','ใบสั่งซื้อ ถูกสร้าง',  'po_created'],
                [11,'purchase_order','ใบสั่งซื้อ ถูกพิมพ์',  'po_printed'],
            ]
        );
        
        $this->addForeignKey('fk-item_lot_lookup_status',
                'item_lot','status',
                'lookup_status','id', 
                'RESTRICT', 'CASCADE'
            );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('lookup_status', 'item_lot_column');
    }
}
