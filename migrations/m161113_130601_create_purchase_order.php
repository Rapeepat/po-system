<?php

use yii\db\Migration;

/**
 * Handles the creation for table `purchase_order`.
 */
class m161113_130601_create_purchase_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if (Yii::$app->db->schema->getTableSchema('purchase_order_site_amount') !== null) 
        {
            $this->dropTable('purchase_order_site_amount');
        }
        if (Yii::$app->db->schema->getTableSchema('purchase_order_item_price') !== null) 
        {
            $this->dropTable('purchase_order_item_price');
        }
        if (Yii::$app->db->schema->getTableSchema('purchase_order') !== null) 
        {
            $this->dropTable('purchase_order');
        }
        
        $this->createTable('purchase_order', [
            'id' => $this->primaryKey(),
            
            'date_created' => $this->timestamp(), 
            
            /* copied from OfferContact*/
            'offer_id' => $this->integer()->notNull(),
            'supplier_id' => $this->integer()->notNull(),
            
            'supplier_address_1' => $this->string()->notNull(),
            'supplier_address_2' => $this->string(),
            'supplier_tel' => $this->string(10),
            'supplier_fax' => $this->string(10),
            'supplier_contact_name' => $this->string(),
            
            'shipping_address_1' => $this->string()->notNull(),
            'shipping_address_2' => $this->string(),
            'shipping_contact_name' => $this->string(),
            'shipping_tel' => $this->string(10),
            /* end of OfferContact */
        ]);
        
        $this->addForeignKey('fk-purchase_order-offer_id', 'purchase_order', 'offer_id', 'offer', 'id');
        $this->addForeignKey('fk-purchase_order-supplier_id', 'purchase_order', 'supplier_id', 'supplier', 'id');
        
        
        $this->createTable('purchase_order_item_price', [
            'purchase_order_id' => $this->integer()->notNull(),
            'item_id' => $this->integer()->notNull(),
            
            'price' => $this->decimal(10,2),
            
            'PRIMARY KEY(purchase_order_id, item_id)'
        ]);
        $this->addForeignKey(
            'fk-purchase_order_item_price-purchase_order_id', 
            'purchase_order_item_price', 
            'purchase_order_id', 
            'purchase_order', 
            'id'
        );
        $this->addForeignKey(
            'fk-purchase_order_item_price-item_id', 
            'purchase_order_item_price', 
            'item_id', 
            'item', 
            'id'
        );
              
        $this->createTable('purchase_order_site_amount', [
            'purchase_order_id' => $this->integer()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'site_id' => $this->integer()->notNull(),
            
            'amount' => $this->decimal(10,2),
            
            'PRIMARY KEY(purchase_order_id, site_id, item_id)'
        ]);
        $this->addForeignKey(
            'fk-purchase_order_site_amount-purchase_order_id', 
            'purchase_order_site_amount', 
            'purchase_order_id', 
            'purchase_order', 
            'id'
        );
        $this->addForeignKey(
            'fk-purchase_order_site_amount-item_id', 
            'purchase_order_site_amount', 
            'item_id', 
            'item', 
            'id'
        );
        $this->addForeignKey(
            'fk-purchase_order_site_amount-site_id', 
            'purchase_order_site_amount', 
            'site_id', 
            'office_site', 
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        
        if (Yii::$app->db->schema->getTableSchema('purchase_order_site_amount') !== null) 
        {
            $this->dropTable('purchase_order_site_amount');
        }
        if (Yii::$app->db->schema->getTableSchema('purchase_order_item_price') !== null) 
        {
            $this->dropTable('purchase_order_item_price');
        }
        if (Yii::$app->db->schema->getTableSchema('purchase_order') !== null) 
        {
            $this->dropTable('purchase_order');
        }
    }
}
