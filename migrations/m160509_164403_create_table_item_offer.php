<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_item_offer`.
 */
class m160509_164403_create_table_item_offer extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        if (Yii::$app->db->schema->getTableSchema('item_offer') !== null)
        {
            $this->down();
        }

        $this->createTable('item_offer', [
            'item_lot_id' => $this->integer()->notNull(),
            'supplier_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'sent_at' => $this->timestamp(),
            'sent_by' => $this->integer(),
            'received_at' => $this->timestamp(),
            'received_by' => $this->integer(),
            'accepted_at' => $this->timestamp(),
            'accepted_by' => $this->integer(),
            'preapproved_at' => $this->timestamp(),
            'preapproved_by' => $this->integer(),
            'unit_cost' => $this->integer(),
            'status' => $this->integer()->notNull()->defaultValue(0),
            'PRIMARY KEY(item_lot_id, supplier_id)'
        ]);

        // not sure do we need to create index for compound key
        /*$this->createIndex(
            'idx-item_offer-item_lot_id',
            'item_offer',
            'item_lot_id'
        );
        //and so on for supplier_id
        */

         $this->addForeignKey(
            'fk-item_offer_created_by',
            'item_offer',
            'created_by',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-item_offer_sent_by',
            'item_offer',
            'sent_by',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-item_offer_received_by',
            'item_offer',
            'received_by',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-item_offer_accepted_by',
            'item_offer',
            'accepted_by',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-item_offer_preapproved_by',
            'item_offer',
            'preapproved_by',
            'user',
            'id',
            'SET NULL'
        );

         $this->addForeignKey(
            'fk-item_offer_item_lot_id',
            'item_offer',
            'item_lot_id',
            'item_lot',
            'id',
            'RESTRICT'
        );

         $this->addForeignKey(
            'fk-item_offer_supplier_id',
            'item_offer',
            'supplier_id',
            'supplier',
            'id',
            'RESTRICT'
        );

         $this->addCommentOnColumn(
            'item_offer',                     //table
            'status',                           //column
            'Later create lookup table. For now 0=Created 1=Sent 2=Received 3=Accepted 4=Rejected 5=PreApproved'   //comment
        );

         $this->addCommentOnColumn(
            'item_offer',                     //table
            'accepted_at',                           //column
            'timestamp for whether Accepted or Rejected'   //comment
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('item_offer');
    }
}
