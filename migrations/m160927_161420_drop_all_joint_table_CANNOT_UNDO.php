<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `all_joint_table_cannot_undo`.
 */
class m160927_161420_drop_all_joint_table_CANNOT_UNDO extends Migration
{
    /**
     * @inheritdoc
     * Drop all the joint table, before adding new database design
     * 
     * Tan: I backed up the current database before drop named "yii2basic_old_po_db"
     */
    public function SafeUp()
    {
        $this->dropTable('item_offer');
        $this->dropTable('purchase_order');
        $this->dropTable('item_request');
        $this->dropTable('item_lot');
        $this->dropTable('lookup_status');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        return false;
    }
}
