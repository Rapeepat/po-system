<?php

use yii\db\Migration;

class m170418_145524_change_purchase_order_fk extends Migration
{
    public function up()
    {
        $this->dropForeignKey(
            'fk-purchase_order_item_price-purchase_order_id', 
            'purchase_order_item_price'
        );
        $this->dropForeignKey(
            'fk-purchase_order_site_amount-purchase_order_id', 
            'purchase_order_site_amount'
        );
        
        $this->addForeignKey(
            'fk-purchase_order_item_price-purchase_order_id', 
            'purchase_order_item_price', 
            'purchase_order_id', 
            'purchase_order', 
            'id',
            'RESTRICT',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-purchase_order_site_amount-purchase_order_id', 
            'purchase_order_site_amount', 
            'purchase_order_id', 
            'purchase_order', 
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-purchase_order_item_price-purchase_order_id', 
            'purchase_order_item_price'
        );
        $this->dropForeignKey(
            'fk-purchase_order_site_amount-purchase_order_id', 
            'purchase_order_site_amount'
        );
        
        $this->addForeignKey(
            'fk-purchase_order_item_price-purchase_order_id', 
            'purchase_order_item_price', 
            'purchase_order_id', 
            'purchase_order', 
            'id'
        );
        $this->addForeignKey(
            'fk-purchase_order_site_amount-purchase_order_id', 
            'purchase_order_site_amount', 
            'purchase_order_id', 
            'purchase_order', 
            'id'
        );
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
