<?php

use yii\db\Migration;

class m161009_063008_remove_lot_fields extends Migration
{
    
    public function safeUp()
    {
        $this->dropForeignKey('fk-lot_maint_id','lot');
        $this->dropIndex('fk-lot_maint_id','lot');
        $this->dropColumn('lot', 'last_maint_id');
        
        $this->dropColumn('lot', 'last_maint_dt');
        
        $this->dropForeignKey('fk-lot_approver_id','lot');
        $this->dropIndex('fk-lot_approver_id','lot');
        $this->dropColumn('lot', 'approver_id');
        
        $this->dropForeignKey('fk-lot_accountant_id','lot');
        $this->dropIndex('fk-lot_accountant_id','lot');
        $this->dropColumn('lot', 'accountant_id');
    }
    
    public function safeDown()
    {
        $this->addColumn('lot', 'last_maint_id', $this->integer());
        $this->addForeignKey('fk-lot_maint_id', 'lot', 'last_maint_id', 'user', 'id');
        
        $this->addColumn('lot', 'last_maint_dt', $this->timestamp());
        
        $this->addColumn('lot', 'approver_id', $this->integer());
        $this->addForeignKey('fk-lot_approver_id', 'lot', 'approver_id', 'user', 'id');
        
        $this->addColumn('lot', 'accountant_id', $this->integer());
        $this->addForeignKey('fk-lot_accountant_id', 'lot', 'accountant_id', 'user', 'id');
    }

    
}
