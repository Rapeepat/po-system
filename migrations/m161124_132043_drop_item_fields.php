<?php

use yii\db\Migration;

class m161124_132043_drop_item_fields extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk-item_maint_id','item');
        $this->dropIndex('fk-item_maint_id','item');
        $this->dropColumn('item', 'last_maint_id');
        
        $this->dropColumn('item', 'last_maint_dt');
        $this->dropColumn('item', 'general_name');
        $this->dropColumn('item', 'model');
        $this->dropColumn('item', 'color');
        $this->dropColumn('item', 'dim_1');
        $this->dropColumn('item', 'dim_2');
        $this->dropColumn('item', 'dim_3');
        $this->dropColumn('item', 'dim_unit');
    }

    public function down()
    {
        echo "m161124_132043_drop_item_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
