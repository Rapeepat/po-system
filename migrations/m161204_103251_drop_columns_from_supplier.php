<?php

use yii\db\Migration;

class m161204_103251_drop_columns_from_supplier extends Migration
{
    public function up()
    {
        
        $this->dropForeignKey('fk-supplier_maint_id','supplier');
        $this->dropIndex('fk-supplier_maint_id','supplier');
        $this->dropColumn('supplier', 'last_maint_id');
        
        $this->dropColumn('supplier', 'last_maint_dt');
    }

    public function down()
    {
        echo "m161204_103251_drop_columns_from_supplier cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
