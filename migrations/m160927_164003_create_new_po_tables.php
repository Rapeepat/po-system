<?php

use yii\db\Migration;

/**
 * Handles the creation for table `new_po_tables`.
 */
class m160927_164003_create_new_po_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function SafeUp()
    {
        $this->SafeDown();

        $this->CreateLotSiteTable();
        $this->CreateRequestTable();
        $this->CreateOfferTable();
        $this->CreateOfferItemTable();
        $this->CreateOfferPriceTable();
        $this->CreateOfferSiteListTable();
    }

    //Create LotSite table
    private function CreateLotSiteTable()
    {
        $this->createTable('lot_site', [
            'lot_id' => $this->integer()->notNull(),
            'site_id' => $this->integer()->notNull(),
            'max_budget' => $this->decimal(10,2),
            'PRIMARY KEY(lot_id, site_id)'
        ]);

        $this->addForeignKey(
            'fk-lot_site-lot_id',
            'lot_site',
            'lot_id',
            'lot',
            'id',
            'RESTRICT',
            'CASCADE'
        );
         
        $this->addForeignKey(
            'fk-lot_site-site_id',
            'lot_site',
            'site_id',
            'office_site',
            'id',
            'RESTRICT',
            'CASCADE'
        );
        
        return;
    }

    //Create Request table
    private function CreateRequestTable()
    {
        $this->createTable('request', [
            'lot_id' => $this->integer()->notNull(),
            'site_id' => $this->integer()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'amount' => $this->integer(),
            'PRIMARY KEY(lot_id, site_id, item_id)'
        ]);
 
        $this->addForeignKey(
            'fk-request-lot_id',
            'request',
            'lot_id',
            'lot',
            'id',
            'RESTRICT',
            'CASCADE'
        );
         
        $this->addForeignKey(
            'fk-request-site_id',
            'request',
            'site_id',
            'office_site',
            'id',
            'RESTRICT',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-request-item_id',
            'request',
            'item_id',
            'item',
            'id',
            'RESTRICT',
            'CASCADE'
        );
        
        return;
    }
    
    //Create Offer table
    private function CreateOfferTable()
    {    
        $this->createTable('offer', [
            'id' => $this->primaryKey(),
            'lot_id' => $this->integer(),
        ]);
        
        $this->addForeignKey(
            'fk-offer-lot_id',
            'offer',
            'lot_id',
            'lot',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }
    
    //Create OfferItem table
    private function CreateOfferItemTable()
    {
        $this->createTable('offer_item', [
            'offer_id' => $this->integer()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'refer_offer_id' => $this->integer(),
            'selected_supplier_id' => $this->integer(),
            'PRIMARY KEY(offer_id, item_id)'
        ]);
       
        $this->addForeignKey(
            'fk-offer_item-item_id',
            'offer_item',
            'item_id',
            'item',
            'id',
            'RESTRICT',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-offer_item-offer_id',
            'offer_item',
            'offer_id',
            'offer',
            'id',
            'RESTRICT',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-offer_item-refer_offer_id',
            'offer_item',
            'refer_offer_id',
            'offer',
            'id',
            'SET NULL',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-offer_item-selected_supplier_id',
            'offer_item',
            'selected_supplier_id',
            'supplier',
            'id',
            'SET NULL',
            'CASCADE'
        );
        
        return;
    }
    
    //Create OfferSiteList table
    private function CreateOfferSiteListTable()
    {
        $this->createTable('offer_site_list', [
            'offer_id' => $this->integer()->notNull(),
            'site_id' => $this->integer()->notNull(),
            'PRIMARY KEY(offer_id, site_id)'
        ]);

        $this->addForeignKey(
            'fk-offer_site_list-offer_id',
            'offer_site_list',
            'offer_id',
            'offer',
            'id',
            'RESTRICT',
            'CASCADE'
        );
         
        $this->addForeignKey(
            'fk-offer_site_list-site_id',
            'offer_site_list',
            'site_id',
            'office_site',
            'id',
            'RESTRICT',
            'CASCADE'
        );
        
        return;
    }
    
    //Create OfferPrice table
    private function CreateOfferPriceTable()
    {
        $this->createTable('offer_price', [
            'offer_id' => $this->integer()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'supplier_id' => $this->integer()->notNull(),
            'price' => $this->decimal(10,2),
            'PRIMARY KEY(offer_id, item_id, supplier_id)'
        ]);

        $this->addForeignKey(
            'fk-offer_price-offer_id',
            'offer_price',
            'offer_id',
            'offer',
            'id',
            'RESTRICT',
            'CASCADE'
        );
        
        $this->addForeignKey(
            'fk-offer_price-item_id',
            'offer_price',
            'item_id',
            'item',
            'id',
            'RESTRICT',
            'CASCADE'
        );
         
        $this->addForeignKey(
            'fk-offer_price-supplier_id',
            'offer_price',
            'supplier_id',
            'supplier',
            'id',
            'RESTRICT',
            'CASCADE'
        );
        
        return;
    }
    
    /**
     * @inheritdoc
     */
    public function SafeDown()
    {
        if (Yii::$app->db->schema->getTableSchema('lot_site') !== null)
        {
            $this->dropTable('lot_site');
        }
        if (Yii::$app->db->schema->getTableSchema('request') !== null)
        {
            $this->dropTable('request');
        }
        if (Yii::$app->db->schema->getTableSchema('offer_item') !== null)
        {
            $this->dropTable('offer_item');
        }
        if (Yii::$app->db->schema->getTableSchema('offer_site_list') !== null)
        {
            $this->dropTable('offer_site_list');
        }
        if (Yii::$app->db->schema->getTableSchema('offer_price') !== null)
        {
            $this->dropTable('offer_price');
        }
        if (Yii::$app->db->schema->getTableSchema('offer') !== null)
        {
            $this->dropTable('offer');
        }
    }
           
}
