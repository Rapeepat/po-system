<?php

use yii\db\Migration;

class m161204_094535_drop_columns_from_office_site extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk-site_maint_id','office_site');
        $this->dropIndex('fk-site_maint_id','office_site');
        $this->dropColumn('office_site', 'last_maint_id');
        
        $this->dropColumn('office_site', 'last_maint_dt');
    }

    public function down()
    {
        echo "m161204_094535_drop_columns_from_office_site cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
