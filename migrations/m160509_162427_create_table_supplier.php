<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_supplier`.
 */
class m160509_162427_create_table_supplier extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
    	if (Yii::$app->db->schema->getTableSchema('supplier') !== null)
        {
            $this->down();
        }

        $this->createTable('supplier', [
            'id' => $this->primaryKey(),
            'last_maint_dt' => 'timestamp on update current_timestamp',
            'last_maint_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'address_1' => $this->string()->notNull(),
            'address_2' => $this->string(),
            'tel' => $this->string(10),
            'fax' => $this->string(10),
            'contact_name' => $this->string(),
            'contact_tel' => $this->string(10),
            'contact_email' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-supplier_maint_id',
            'supplier',
            'last_maint_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('supplier');
    }
}
