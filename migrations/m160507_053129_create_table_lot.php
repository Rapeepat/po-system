<?php

use yii\db\Migration;

class m160507_053129_create_table_lot extends Migration
{
    public function up()
    {
    	if (Yii::$app->db->schema->getTableSchema('lot') !== null)
    	{
    		$this->down();
    	}

        $this->createTable('lot', [
            'id' => $this->primaryKey(),
            'last_maint_dt' => 'timestamp on update current_timestamp',
            'last_maint_id' => $this->integer(),
            'lot_date' => $this->date()->notNull(),
            'approver_id' => $this->integer(),
            'accountant_id' => $this->integer()->notNull()
        ]);

         $this->addForeignKey(
            'fk-lot_maint_id',
            'lot',
            'last_maint_id',
            'user',
            'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'fk-lot_approver_id',
            'lot',
            'approver_id',
            'user',
            'id',
            'SET NULL'
        );
         $this->addForeignKey(
            'fk-lot_accountant_id',
            'lot',
            'accountant_id',
            'user',
            'id',
            'RESTRICT'
        );
    }

    public function down()
    {
        $this->dropTable('lot');
    }
}
