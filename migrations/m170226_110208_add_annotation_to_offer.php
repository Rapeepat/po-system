<?php

use yii\db\Migration;

class m170226_110208_add_annotation_to_offer extends Migration
{
    
    public function safeUp()
    {
        $this->addColumn(
            'offer_item', 
            'annotation', 
            $this->string(512)->defaultValue('')->notNull()
        );
    }

    public function safeDown()
    {
        $this->dropColumn('offer_item', 'annotation');
    }
}
