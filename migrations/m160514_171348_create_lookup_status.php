<?php

use yii\db\Migration;

/**
 * Handles the creation for table `lookup_status`.
 */
class m160514_171348_create_lookup_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (Yii::$app->db->schema->getTableSchema('lookup_status') !== null)
        {
            $this->down();
        }
        $this->createTable('lookup_status', [
            'id' => $this->integer().' PRIMARY KEY',
            'table' => $this->string()->notNull(),
            'value' => $this->string()->notNull(),
        ]);

        $this->batchInsert(
            'lookup_status', 
            ['id','table','value'], 
            [
                [0,'item_request','คำขอถูกสร้าง'],
                [1,'item_request','ยอมรับ'],
                [2,'item_request','ปฏิเสธ'],
                [3,'item_offer','คำขอถูกสร้าง'],
                [4,'item_offer','ส่งคำขอไปยังร้านค้า'],
                [5,'item_offer','ร้านค้าตอบรับคำขอ'],
                [6,'item_offer','ยอมรับ'],
                [7,'item_offer','ปฏิเสธ'],
                [8,'item_offer','อนุมัติ'],
                [9,'item_offer','ไม่อนุมัติ'],
                [10,'purchase_order','คำขอถูกสร้าง'],
                [11,'purchase_order','ใบสั่งซื้อ ถูกพิมพ์'],
            ]
        );

        $this->addForeignKey('fk-item_lot_lookup_status',
                'item_lot','status',
                'lookup_status','id', 
                'RESTRICT', 'CASCADE'
            );
    }
    
    //fk

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-item_lot_lookup_status', 'item_lot');
        $this->dropIndex('fk-item_lot_lookup_status', 'item_lot');
        $this->dropTable('lookup_status');
    }
}
