<?php

use yii\db\Migration;

class m160515_120723_rbac_init extends Migration
{

    public function up()
    {
        $auth = Yii::$app->authManager;
        
        // completely new initilize 
        $auth->removeAll();

        // create permissions //
        $manageUsers = $auth->createPermission('manageUsers');
        $auth->add($manageUsers);

        $manageItem = $auth->createPermission('manageItem');
        $auth->add($manageItem);

        $manageLot = $auth->createPermission('manageLot');
        $auth->add($manageLot);

        $manageItemLot = $auth->createPermission('manageLotItem');
        $auth->add($manageItemLot);

        $createUpdateItemLot = $auth->createPermission('createUpdateItemLot');
        $auth->add($createUpdateItemLot);

        $manageItemRequest = $auth->createPermission('manageItemRequest');
        $auth->add($manageItemRequest);

        $createUpdateItemRequest = $auth->createPermission('createUpdateItemRequest');
        $auth->add($createUpdateItemRequest);

        $manageItemOffer = $auth->createPermission('manageItemOffer');
        $auth->add($manageItemOffer);

        $updateItemOffer = $auth->createPermission('updateItemOffer');
        $auth->add($updateItemOffer);
         
        $managePurchaseOrder = $auth->createPermission('managePurchaseOrder');
        $auth->add($managePurchaseOrder);

        $createPurchaseOrder = $auth->createPermission('createPurchaseOrder');
        $auth->add($createPurchaseOrder);
        
        $manageOfficeSite = $auth->createPermission('manageOfficeSite');
        $auth->add($manageOfficeSite);
        
        $manageSupplier = $auth->createPermission('manageSupplier');
        $auth->add($manageSupplier);
        
        $manageCatalog = $auth->createPermission('manageCatalog');
        $auth->add($manageCatalog);
        
        // create roles with permissions //

        $accountant = $auth->createRole('accountant');
        $auth->add($accountant);
        $auth->addChild($accountant, $manageItem);
        $auth->addChild($accountant, $manageLot);
        $auth->addChild($accountant, $manageItemLot);
        $auth->addChild($accountant, $manageItemRequest);
        $auth->addChild($accountant, $createPurchaseOrder);
        $auth->addChild($accountant, $manageOfficeSite);
        $auth->addChild($accountant, $manageSupplier);
        $auth->addChild($accountant, $manageCatalog);

        $committee = $auth->createRole('committee');
        $auth->add($committee);
        $auth->addChild($committee, $updateItemOffer);

        $requestor = $auth->createRole('requestor');
        $auth->add($requestor);
        $auth->addChild($requestor, $createUpdateItemLot);
        $auth->addChild($requestor, $createUpdateItemRequest);

        $supplier = $auth->createRole('supplier');
        $auth->add($supplier);
        $auth->addChild($supplier, $updateItemOffer);

        // add "admin" role and give this role the "manageUsers" permission
        // as well as the permissions of the "author" role

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $manageUsers);
        $auth->addChild($admin, $accountant);
        $auth->addChild($admin, $committee);
        $auth->addChild($admin, $requestor);
        $auth->addChild($admin, $supplier);
        $auth->addChild($admin, $managePurchaseOrder); // not applied to any roles yet
        // todo: better add permissions rather than add roles to admin

        // Assign roles to users. 1,2,.. are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
//        $auth->assign($supplier, 5);
//        $auth->assign($requestor, 4);
//        $auth->assign($committee, 3);
//        $auth->assign($accountant, 2);
//        $auth->assign($admin, 1);
    }

    public function down()
    {
        Yii::$app->authManager->removeAll();
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
