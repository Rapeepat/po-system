<?php

use yii\db\Migration;

class m161205_161144_drop_head_site_id_column_from_office_site extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk-head_site_site_id','office_site');
        $this->dropIndex('fk-head_site_site_id','office_site');
        $this->dropColumn('office_site', 'head_site_id');
    }

    public function down()
    {
        echo "m161205_161144_drop_head_site_id_column_from_office_site cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
