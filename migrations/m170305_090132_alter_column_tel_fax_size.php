<?php

use yii\db\Migration;

class m170305_090132_alter_column_tel_fax_size extends Migration
{
    public function up()
    {
        $this->alterColumn('offer_contact',     'supplier_tel'   , $this->string(50));
	$this->alterColumn('offer_contact',     'supplier_fax'   , $this->string(50));
	$this->alterColumn('offer_contact',     'shipping_tel'   , $this->string(50));
	$this->alterColumn('office_site',       'tel' 	         , $this->string(50));
	$this->alterColumn('office_site',       'fax' 	         , $this->string(50));
	$this->alterColumn('purchase_order',    'supplier_tel' 	 , $this->string(50));
	$this->alterColumn('purchase_order',    'supplier_fax' 	 , $this->string(50));
        $this->alterColumn('purchase_order',    'shipping_tel' 	 , $this->string(50));
	$this->alterColumn('supplier',          'tel' 	         , $this->string(50));
	$this->alterColumn('supplier',          'fax' 	         , $this->string(50));
    }

    public function down()
    {
        $this->alterColumn('offer_contact',     'supplier_tel'   , $this->string(10));
	$this->alterColumn('offer_contact',     'supplier_fax'   , $this->string(10));
	$this->alterColumn('offer_contact',     'shipping_tel'   , $this->string(10));
	$this->alterColumn('office_site',       'tel' 	         , $this->string(10));
	$this->alterColumn('office_site',       'fax' 	         , $this->string(10));
	$this->alterColumn('purchase_order',    'supplier_tel' 	 , $this->string(10));
	$this->alterColumn('purchase_order',    'supplier_fax'   , $this->string(10));
        $this->alterColumn('purchase_order',    'shipping_tel'   , $this->string(10));
	$this->alterColumn('supplier',          'tel' 	         , $this->string(10));
	$this->alterColumn('supplier',          'fax' 	         , $this->string(10));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
