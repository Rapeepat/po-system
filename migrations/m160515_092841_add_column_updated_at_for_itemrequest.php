<?php

use yii\db\Migration;

class m160515_092841_add_column_updated_at_for_itemrequest extends Migration
{
    public function up()
    {
        $this->addColumn('item_request', 'last_maint_at', $this->timestamp());
        $this->addColumn('item_request', 'last_maint_by', $this->integer());

        $this->addForeignKey('fk-item_request_last_maint_by',
            'item_request', 'last_maint_by', 
            'user', 'id', 
            'SET NULL', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-item_request_last_maint_by', 'item_request');
        $this->dropIndex('fk-item_request_last_maint_by', 'item_request');
        $this->dropColumn('item_request', 'last_maint_at');
        $this->dropColumn('item_request', 'last_maint_by');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
