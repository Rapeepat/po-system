<?php

use yii\db\Migration;

class m170305_113115_alter_po_date_created_no_auto extends Migration
{
    public function up()
    {
        $this->alterColumn('purchase_order','date_created','timestamp NOT NULL default CURRENT_TIMESTAMP');
    }

    public function down()
    {
        $this->alterColumn('purchase_order','date_created',$this->timestamp());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
