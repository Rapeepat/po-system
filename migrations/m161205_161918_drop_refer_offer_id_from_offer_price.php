<?php

use yii\db\Migration;

class m161205_161918_drop_refer_offer_id_from_offer_price extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk-offer_item-refer_offer_id','offer_item');
        $this->dropIndex('fk-offer_item-refer_offer_id','offer_item');
        $this->dropColumn('offer_item', 'refer_offer_id');
    }

    public function down()
    {
        echo "m161205_161918_drop_refer_offer_id_from_offer_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
