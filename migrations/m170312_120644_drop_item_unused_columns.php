<?php

use yii\db\Migration;

class m170312_120644_drop_item_unused_columns extends Migration
{
    public function up()
    {
        $this->dropColumn('item', 'brand');
        $this->dropColumn('item', 'detail');
    }

    public function down()
    {
        echo "m170312_120644_drop_item_unused_columns cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
