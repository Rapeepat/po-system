<?php

use yii\db\Migration;

class m161204_100251_drop_shipping_flag_from_office_site extends Migration
{
    public function up()
    {
        $this->dropColumn('office_site', 'shipping_flag');
    }

    public function down()
    {
        echo "m161204_100251_drop_shipping_flag_from_office_site cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
