<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "offer_site_list".
 *
 * @property integer $offer_id
 * @property integer $site_id
 *
 * @property Offer $offer
 * @property OfficeSite $site
 */
class OfferSiteList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'offer_site_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id'], 'required'],
            [['offer_id', 'site_id'], 'integer'],
            [['offer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Offer::className(), 'targetAttribute' => ['offer_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfficeSite::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'offer_id' => 'Offer ID',
            'site_id' => 'Site ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffer()
    {
        return $this->hasOne(Offer::className(), ['id' => 'offer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(OfficeSite::className(), ['id' => 'site_id']);
    }
}
