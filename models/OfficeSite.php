<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "office_site".
 *
 * @property integer $id
 * @property string $name
 * @property string $address_1
 * @property string $tel
 * @property string $fax
 * @property integer $shipping_flag
 * @property string $contact_name
 *
 * @property LotSite[] $lotSites
 * @property Lot[] $lots
 * @property OfferSiteList[] $offerSiteLists
 * @property Offer[] $offers
 * @property OfficeSite[] $officeSites
 * @property PurchaseOrderSiteAmount[] $purchaseOrderSiteAmounts
 * @property Request[] $requests
 */
class OfficeSite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'office_site';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address_1'], 'required'],
            [['name', 'address_1', 'contact_name'], 'string', 'max' => 255],
            [['tel', 'fax'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ลำดับ',
            'name' => 'ชื่อหน่วยงาน',
            'address_1' => 'ที่อยู่ในการจัดส่งสินค้า',
            'tel'          => 'หมายเลขติดต่อหน่วยงาน',
            'fax'          => 'แฟกซ์หน่วยงาน',	     
            'contact_name' => 'ชื่อผู้ติดต่อ',
        ];
    }

    public function beforeDelete()
    {
        if (count($this->requests) > 0) return false; 
        return parent::beforeDelete();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLotSites()
    {
        return $this->hasMany(LotSite::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLots()
    {
        return $this->hasMany(Lot::className(), ['id' => 'lot_id'])->viaTable('lot_site', ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferSiteLists()
    {
        return $this->hasMany(OfferSiteList::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasMany(Offer::className(), ['id' => 'offer_id'])->viaTable('offer_site_list', ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderSiteAmounts()
    {
        return $this->hasMany(PurchaseOrderSiteAmount::className(), ['site_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['site_id' => 'id']);
    }
}
