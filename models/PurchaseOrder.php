<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase_order".
 *
 * @property integer $id
 * @property string $date_created
 * @property integer $offer_id
 * @property integer $supplier_id
 * @property string $supplier_address_1
 * @property string $supplier_tel
 * @property string $supplier_fax
 * @property string $supplier_contact_name
 * @property string $shipping_address_1
 * @property string $shipping_contact_name
 * @property string $shipping_tel
 *
 * @property Supplier $supplier
 * @property Offer $offer
 * @property PurchaseOrderItemPrice[] $purchaseOrderItemPrices
 * @property Item[] $items
 * @property PurchaseOrderSiteAmount[] $purchaseOrderSiteAmounts
 * @property OfficeSite[] $sites
 */
class PurchaseOrder extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created'], 'safe'],
            //[['offer_id', 'supplier_id', 'supplier_address_1', 'shipping_address_1'], 'required'],
            //[['offer_id', 'supplier_id'], 'integer'],
            [['supplier_address_1', 'supplier_contact_name', 'shipping_address_1', 'shipping_contact_name'], 'string', 'max' => 255],
            [['supplier_tel', 'supplier_fax', 'shipping_tel'], 'string', 'max' => 50],
            /*[['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplier_id' => 'id']],
            [['offer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Offer::className(), 'targetAttribute' => ['offer_id' => 'id']],*/
            [['id'], 'unique', 'message'=>'เลขที่ใบสั่งซื้อนี้มีการใช้งานแล้ว'],
            [['id'], 'required', 'message'=>'กรุณาใส่เลขที่ใบสั่งซื้อ'],
            [['id'], 'integer', 'message'=>'เลขที่ใบสั่งซื้อต้องเป็นตัวเลขเท่านั้น (ไม่ต้องมีปีงบประมาณ)'],
        ];
    }
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['id'];
        $scenarios[self::SCENARIO_UPDATE] = [
            'id','date_created','supplier_address_1', 'supplier_contact_name', 'shipping_address_1', 'shipping_contact_name','supplier_tel', 'supplier_fax', 'shipping_tel'
        ];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'เลขที่ใบสั่งซื้อ',
            'date_created' => 'เวลาที่สร้างใบสั่งซื้อ',
            'offer_id' => 'Offer ID',
            'supplier_id' => 'Supplier ID',
            'supplier_address_1' => 'ที่อยู่ร้านค้า',
            'supplier_tel' => 'เบอร์โทร. ร้านค้า',
            'supplier_fax' => 'แฟกซ์ ร้านค้า',
            'supplier_contact_name' => 'ชื่อผู้ติดต่อ ร้านค้า',
            'shipping_address_1' => 'ที่อยู่ สำหรับการจัดส่ง',
            'shipping_contact_name' => 'ชื่อผู้ติดต่อ สำหรับการจัดส่ง',
            'shipping_tel' => 'เบอร์โทร. สำหรับการจัดส่ง',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffer()
    {
        return $this->hasOne(Offer::className(), ['id' => 'offer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderItemPrices()
    {
        return $this->hasMany(PurchaseOrderItemPrice::className(), ['purchase_order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['id' => 'item_id'])->viaTable('purchase_order_item_price', ['purchase_order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderSiteAmounts()
    {
        return $this->hasMany(PurchaseOrderSiteAmount::className(), ['purchase_order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(OfficeSite::className(), ['id' => 'site_id'])->viaTable('purchase_order_site_amount', ['purchase_order_id' => 'id']);
    }
}
