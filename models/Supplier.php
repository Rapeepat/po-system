<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "supplier".
 *
 * @property integer $id
 * @property string $name
 * @property string $address_1
 * @property string $tel
 * @property string $fax
 * @property string $contact_name
 *
 * @property OfferContact[] $offerContacts
 * @property Offer[] $offers
 * @property OfferItem[] $offerItems
 * @property OfferPrice[] $offerPrices
 * @property PurchaseOrder[] $purchaseOrders
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supplier';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address_1'], 'required'],
            [['name', 'address_1', 'contact_name'], 'string', 'max' => 255],
            [['tel', 'fax'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ลำดับ',
            'name' => 'ชื่อร้านค้า',
            'address_1' => 'ที่อยู่',
            'tel' => 'หมายเลขติดต่อร้านค้า',
            'fax' => 'แฟกซ์ร้านค้า',
            'contact_name' => 'ชื่อผู้ติดต่อร้านค้า',
        ];
    }
    
    public function beforeDelete()
    {
        if (count($this->offers) > 0) return false; 
        return parent::beforeDelete();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferContacts()
    {
        return $this->hasMany(OfferContact::className(), ['supplier_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasMany(Offer::className(), ['id' => 'offer_id'])->viaTable('offer_contact', ['supplier_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferItems()
    {
        return $this->hasMany(OfferItem::className(), ['selected_supplier_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferPrices()
    {
        return $this->hasMany(OfferPrice::className(), ['supplier_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::className(), ['supplier_id' => 'id']);
    }
}
