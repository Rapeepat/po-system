<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase_order_site_amount".
 *
 * @property integer $purchase_order_id
 * @property integer $item_id
 * @property integer $site_id
 * @property string $amount
 *
 * @property OfficeSite $site
 * @property Item $item
 * @property PurchaseOrder $purchaseOrder
 */
class PurchaseOrderSiteAmount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'purchase_order_site_amount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['purchase_order_id', 'item_id', 'site_id'], 'required'],
            [['purchase_order_id', 'item_id', 'site_id'], 'integer'],
            [['amount'], 'number'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfficeSite::className(), 'targetAttribute' => ['site_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['purchase_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => PurchaseOrder::className(), 'targetAttribute' => ['purchase_order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'purchase_order_id' => 'Purchase Order ID',
            'item_id' => 'Item ID',
            'site_id' => 'Site ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(OfficeSite::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrder()
    {
        return $this->hasOne(PurchaseOrder::className(), ['id' => 'purchase_order_id']);
    }
}
