<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "item_type".
 *
 * @property integer $id
 * @property integer $main_type 
 *  @property string $detail
 *
 * @property Item[] $items
 * @property ItemType $mainType 
 * @property ItemType[] $itemTypes 
 * @property Lot[] $lots
 */
class ItemType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['main_type'], 'integer'], 
            [['detail'], 'string', 'max' => 255],
            [['main_type'], 'exist', 'skipOnError' => true, 'targetClass' => ItemType::className(), 'targetAttribute' => ['main_type' => 'id']], 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'main_type' => 'หมวดสินค้า',
            'detail' => 'เลขที่บัญชี',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
    */
    public function getMainType() 
    { 
        return $this->hasOne(ItemType::className(), ['id' => 'main_type']); 
    } 

    /** 
     * @return \yii\db\ActiveQuery 
    */ 
    public function getItemTypes() 
    { 
        return $this->hasMany(ItemType::className(), ['main_type' => 'id']); 
    } 
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['item_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLots()
    {
        return $this->hasMany(Lot::className(), ['item_type_id' => 'id']);
    }
    
    /**
     * @return array Array with optgroup to be used with Lot/Create dropdown list
     */
    public static function getDropDown($chooseMain = false)
    {
        $dropDownArray = [];
        //ArrayHelper::map(ItemType::find()->all(),'id','detail')
        $mainTypes = self::find()->where('main_type is null')->all();
        foreach($mainTypes as $mainType)
        {
            $subTypeArray = [];
            if($chooseMain) $subTypeArray[$mainType->id] = $mainType->detail;
            $subTypes = self::find()->where(['main_type'=>$mainType->id])->all();
            foreach($subTypes as $subType)
            {
                $subTypeArray[$subType->id] = $subType->detail;
            }
            $dropDownArray[$mainType->detail] = $subTypeArray;
        }
        return $dropDownArray;
    }
}
