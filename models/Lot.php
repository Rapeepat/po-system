<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lot".
 *
 * @property integer $id
 * @property string $lot_date
 * @property integer $item_type_id
 *
 * @property ItemType $itemType
 * @property LotSite[] $lotSites
 * @property OfficeSite[] $sites
 * @property Offer[] $offers
 * @property Request[] $requests
 */
class Lot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lot_date'], 'safe'],
            [['item_type_id'], 'integer'],
            [['item_type_id'], 'required', 'message' => 'กรุณาเลือกชนิดของสินค้า'],
            [['lot_date'], 'required', 'message' => 'กรุณาเลือกวันที่'],
            //[['item_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ItemType::className(), 'targetAttribute' => ['item_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'เลขที่งบประมาณ',
            'lot_date' => 'วันที่งบประมาณ',
            'item_type_id' => 'ชนิดของสินค้า',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemType()
    {
        return $this->hasOne(ItemType::className(), ['id' => 'item_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLotSites()
    {
        return $this->hasMany(LotSite::className(), ['lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(OfficeSite::className(), ['id' => 'site_id'])->viaTable('lot_site', ['lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasMany(Offer::className(), ['lot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['lot_id' => 'id']);
    }
}
