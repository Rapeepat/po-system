<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "offer_contact".
 *
 * @property integer $offer_id
 * @property integer $supplier_id
 * @property string $supplier_address_1
 * @property string $supplier_tel
 * @property string $supplier_fax
 * @property string $supplier_contact_name
 * @property string $shipping_address_1
 * @property string $shipping_contact_name
 * @property string $shipping_tel
 *
 * @property Supplier $supplier
 * @property Offer $offer
 */
class OfferContact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'offer_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['offer_id', 'supplier_id', 'supplier_address_1', 'shipping_address_1'], 'required'],
            [['offer_id', 'supplier_id'], 'integer'],
            [['supplier_address_1', 'supplier_contact_name', 'shipping_address_1', 'shipping_contact_name'], 'string', 'max' => 255],
            [['supplier_tel', 'supplier_fax', 'shipping_tel'], 'string', 'max' => 50],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplier_id' => 'id']],
            [['offer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Offer::className(), 'targetAttribute' => ['offer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'offer_id' => 'Offer ID',
            'supplier_id' => 'Supplier ID',
            'supplier_address_1' => 'ที่อยู่ร้านค้า',
            'supplier_tel' => 'เบอร์โทรศัพท์',
            'supplier_fax' => 'แฟกซ์',
            'supplier_contact_name' => 'ชื่อผู้ติดต่อ',
            'shipping_address_1' => 'ที่อยู่จัดส่ง',
            'shipping_contact_name' => 'ชื่อผู้คิดต่อ ในการจัดส่ง',
            'shipping_tel' => 'เบอร์โทรศัพท์',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffer()
    {
        return $this->hasOne(Offer::className(), ['id' => 'offer_id']);
    }
}
