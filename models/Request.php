<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "request".
 *
 * @property integer $lot_id
 * @property integer $site_id
 * @property integer $item_id
 * @property integer $amount
 *
 * @property Item $item
 * @property Lot $lot
 * @property OfficeSite $site
 */
class Request extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    public $sum_amount;
    public $selected;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['lot_id', 'site_id'], 'required'],
            [['amount'],'unique','targetAttribute'=>['lot_id', 'site_id', 'item_id'],'message'=>'มีคำขอสั่งซื้อสินค้านี้แล้ว'],
            //[['item_id'], 'required', 'message' => 'กรุณาเลือกสินค้า'],
            [['lot_id', 'site_id', 'item_id'], 'integer'],
            [['amount'],'integer','message'=>'กรุณาใส่จำนวนเป็นตัวเลข'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['lot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lot::className(), 'targetAttribute' => ['lot_id' => 'id']],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfficeSite::className(), 'targetAttribute' => ['site_id' => 'id']],
        ];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE ] = [['item_id'], 'safe'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lot_id' => 'Lot ID',
            'site_id' => 'Site ID',
            'item_id' => 'Item ID',
            'amount' => 'จำนวน',
            'sum_amount' => 'จำนวนรวม',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'lot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(OfficeSite::className(), ['id' => 'site_id']);
    }
}
