<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "lot_site".
 *
 * @property integer $lot_id
 * @property integer $site_id
 * @property string $main_budget
 * @property string $etc_budget
 * 
 *
 * @property OfficeSite $site
 * @property Lot $lot
 */
class LotSite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lot_site';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_id'], 'required'], //removed lot_id so the model can be validate first
            [['lot_id', 'site_id'], 'integer'],
            [['main_budget','etc_budget'], 'number', 'message' => 'กรุณาใส่จำนวนตัวเลขเท่านั้น'],
            [['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfficeSite::className(), 'targetAttribute' => ['site_id' => 'id']],
            [['lot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lot::className(), 'targetAttribute' => ['lot_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lot_id' => 'Lot ID',
            'site_id' => 'Site ID',
            'main_budget' => 'Main Budget',
            'etc_budget' => 'Etc Budget',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSite()
    {
        return $this->hasOne(OfficeSite::className(), ['id' => 'site_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'lot_id']);
    }
    
    public function getRemainingBudget()
    {
        $budget = $this->main_budget + $this->etc_budget;
        //return 'TBD';
        $query = new Query;
        // compose the query
        $expense = $query
            ->from('purchase_order')
            ->join('inner join','offer','purchase_order.offer_id = offer.id ')
            ->join('inner join','purchase_order_item_price','purchase_order.id = purchase_order_item_price.purchase_order_id')                
            ->join('inner join','purchase_order_site_amount','purchase_order.id = purchase_order_site_amount.purchase_order_id and purchase_order_item_price.item_id = purchase_order_site_amount.item_id')
            ->where([
                    'purchase_order.void'=>0,
                    'offer.lot_id'=>$this->lot_id,
                    'purchase_order_site_amount.site_id'=>$this->site_id
                ])
            ->sum('purchase_order_site_amount.amount * purchase_order_item_price.price');
            
        return $budget - $expense;
    }
    public function getRemaining12Budget()
    {
        $year = date_format(new \DateTime($this->lot->lot_date),'Y');
        
        $lotSites = LotSite::find()
                ->joinWith('lot')
                ->where('lot.id > 0')
                ->andWhere('lot.item_type_id = :item_type_id',['item_type_id'=>$this->lot->item_type_id])
                ->andwhere('lot.lot_date <= :lot_date and year(lot.lot_date) = :year', ['lot_date'=>$this->lot->lot_date, 'year'=>$year])
                ->andWhere('lot_site.site_id = :site_id',['site_id'=>$this->site_id])
                ->all();
        
        $remaining_budgets = [];
        foreach($lotSites as $lotSite)
        {
            $remaining_budgets[] = $lotSite->remainingBudget;
        }
        
        return array_sum($remaining_budgets);
    }
    
    //This function is almost the same as getRemaining12Budget
    //The only difference is criteria abount MONTH
    public function getRemaining6Budget()
    {
        $date = new \DateTime($this->lot->lot_date);
        $year = date_format($date,'Y');
        
        $month = intval(date_format($date,'n'));                 //MONTH CRIT
        if ($month >= 1 && $month <= 6) $monthCrit = '<= 6';     //MONTH CRIT
        else $monthCrit = '> 6';                                 //MONTH CRIT
        
        $lotSites = LotSite::find()
                ->joinWith('lot')
                ->where('lot.id > 0')
                ->andWhere('lot.item_type_id = :item_type_id',['item_type_id'=>$this->lot->item_type_id])
                ->andWhere('month(lot.lot_date) '.$monthCrit)    //MONTH CRIT
                ->andwhere('lot.lot_date <= :lot_date and year(lot.lot_date) = :year', ['lot_date'=>$this->lot->lot_date, 'year'=>$year])
                ->andWhere('lot_site.site_id = :site_id',['site_id'=>$this->site_id])
                ->all();
        
        $remaining_budgets = [];
        foreach($lotSites as $lotSite)
        {
            $remaining_budgets[] = $lotSite->remainingBudget;
        }
        
        return array_sum($remaining_budgets);
    }
}
