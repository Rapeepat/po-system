<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lot;

/**
 * LotSearch represents the model behind the search form about `app\models\Lot`.
 */
class LotSearch extends Lot
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'item_type_id'], 'integer'],
            [['lot_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $except_lot_id = null)
    {
        $query = Lot::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => ['pageSize'=>10],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->joinWith('itemType');
        
        $query->andWhere(
            'lot.id > 0'
        );
        
        if($except_lot_id !== null) $query->andWHere('lot.id != :lot_id',['lot_id'=>$except_lot_id]);
        
        // grid filtering conditions
        $query->andFilterWhere([
            'item_type.id' => $this->id,
            'lot_date' => $this->lot_date,
            //'item_type_id' => $this->item_type_id, 
        ]);
        
        $query->andFilterWhere([
            'or',
            ['item_type_id' => $this->item_type_id],
            ['item_type.main_type' => $this->item_type_id]
        ]);

        return $dataProvider;
    }
}
