<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Request;

/**
 * RequestSearch represents the model behind the search form about `app\models\Request`.
 */
class RequestSearch extends Request
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lot_id', 'site_id', 'item_id', 'amount'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $lot_id)
    {
        $query = Request::find()
                ->select(['lot_id','item_id','sum(amount) as sum_amount'])
                ->where(['lot_id'=>$lot_id])
                ->groupBy(['lot_id','item_id'])
                ->orderBy('CASE
                    WHEN (sum(amount) > 0)
                        THEN 0
                    ELSE 1
                END, item_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        /*$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
       
        // grid filtering conditions
        $query->andFilterWhere([
            'lot_id' => $this->lot_id,
            //'site_id' => $this->site_id,
            //'item_id' => $this->item_id,
            //'amount' => $this->amount,
        ]);*/

        return $dataProvider;
    }
}
