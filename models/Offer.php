<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "offer".
 *
 * @property integer $id
 * @property integer $lot_id
 *
 * @property Lot $lot
 * @property OfferItem[] $offerItems
 * @property OfferItem[] $referItems
 * @property Item[] $items
 * @property OfferPrice[] $offerPrices
 * @property OfferSiteList[] $offerSiteLists
 * @property OfficeSite[] $sites
 */
class Offer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'offer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lot_id'], 'integer'],
            [['lot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lot::className(), 'targetAttribute' => ['lot_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lot_id' => 'Lot ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLot()
    {
        return $this->hasOne(Lot::className(), ['id' => 'lot_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferItems()
    {
        return $this->hasMany(OfferItem::className(), ['offer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['id' => 'item_id'])->viaTable('offer_item', ['offer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferPrices()
    {
        return $this->hasMany(OfferPrice::className(), ['offer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferSiteLists()
    {
        return $this->hasMany(OfferSiteList::className(), ['offer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSites()
    {
        return $this->hasMany(OfficeSite::className(), ['id' => 'site_id'])->viaTable('offer_site_list', ['offer_id' => 'id']);
    }
    
    /**
    * @return \yii\db\ActiveQuery
    */
    public function getOfferContacts() 
    { 
        return $this->hasMany(OfferContact::className(), ['offer_id' => 'id']); 
    } 

    /** 
    * @return \yii\db\ActiveQuery 
    */ 
    public function getSuppliers() 
    { 
        return $this->hasMany(Supplier::className(), ['id' => 'supplier_id'])->viaTable('offer_contact', ['offer_id' => 'id']); 
    } 
    
    /** 
    * @return \yii\db\ActiveQuery 
    */ 
    public function getPurchaseOrders() 
    { 
        return $this->hasMany(PurchaseOrder::className(), ['offer_id' => 'id']); 
    }     
}
