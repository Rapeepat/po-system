<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "offer_item".
 *
 * @property integer $offer_id
 * @property integer $item_id
 * @property integer $selected_supplier_id
 * @property string $annotation
 *
 * @property Supplier $selectedSupplier
 * @property Item $item
 * @property Offer $offer
 */
class OfferItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'offer_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id'], 'required'],
            [['offer_id', 'item_id', 'selected_supplier_id'], 'integer'],
            [['annotation'],'safe'],
            [['selected_supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['selected_supplier_id' => 'id']],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['offer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Offer::className(), 'targetAttribute' => ['offer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'offer_id' => 'Offer ID',
            'item_id' => 'Item ID',
            'selected_supplier_id' => 'Selected Supplier ID',
            'annotation' => 'หมายเหตุ',
        ];
    }

    public function getSumAmount()
    {
        return Request::find()
                        ->select('sum(amount) as sum_amount')
                        ->where(['item_id' => $this->item_id, 'lot_id'=>$this->offer->lot_id])
                        ->one()
                        ->sum_amount;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSelectedSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'selected_supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffer()
    {
        return $this->hasOne(Offer::className(), ['id' => 'offer_id']);
    }
}
