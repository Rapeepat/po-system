<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "item".
 *
 * @property integer $id
 * @property string $name
 * @property string $display_code
 * @property string $unit
 * @property integer $item_type_id
 *
 * @property ItemType $itemType
 * @property OfferItem[] $offerItems
 * @property Offer[] $offers
 * @property OfferPrice[] $offerPrices
 * @property PurchaseOrderItemPrice[] $purchaseOrderItemPrices
 * @property PurchaseOrder[] $purchaseOrders
 * @property PurchaseOrderSiteAmount[] $purchaseOrderSiteAmounts
 * @property Request[] $requests
 */
class Item extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required','message'=>'กรุณาใส่ชื่อสินค้า'],
            [['item_type_id'], 'integer'],
            [['name', 'display_code', 'unit'], 'string', 'max' => 255],
            [['item_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ItemType::className(), 'targetAttribute' => ['item_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ลำดับ',
            'name' => 'ชื่อสินค้า',
            'display_code' => 'รหัสสินค้า',
            'unit' => 'หน่วย',
            'item_type_id' => 'ชนิดสินค้า',
        ];
    }

    public function beforeDelete()
    {
        if (count($this->requests) > 0) return false; 
        return parent::beforeDelete();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemType()
    {
        return $this->hasOne(ItemType::className(), ['id' => 'item_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferItems()
    {
        return $this->hasMany(OfferItem::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOffers()
    {
        return $this->hasMany(Offer::className(), ['id' => 'offer_id'])->viaTable('offer_item', ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferPrices()
    {
        return $this->hasMany(OfferPrice::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderItemPrices()
    {
        return $this->hasMany(PurchaseOrderItemPrice::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrders()
    {
        return $this->hasMany(PurchaseOrder::className(), ['id' => 'purchase_order_id'])->viaTable('purchase_order_item_price', ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseOrderSiteAmounts()
    {
        return $this->hasMany(PurchaseOrderSiteAmount::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['item_id' => 'id']);
    }
    
    public function getDropDown($item_type_id = null)
    {
        $where = '';
        
        if ($item_type_id!=null)
        {
            $where = 'item_type_id is null';
            $item_type = ItemType::findOne($item_type_id);
            if($item_type!=null)
            {
                $where = $where.' or item_type_id = '.$item_type_id;
                if($item_type->main_type!=null)
                {
                    $where = $where.' or item_type_id = '.$item_type->main_type;
                }
            }       
        }
        return ArrayHelper::map(
            Item::find()
                ->where($where)
                ->all(),
            'id',
            'name'
        );
    }
}
