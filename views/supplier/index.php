<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SupplierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'หน้าจัดการร้านค้า';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supplier-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-plus-sign"></span> เพิ่มร้านค้าใหม่', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => 'แสดง <strong>{begin}-{end}</strong> จากทั้งหมด <strong>{totalCount}</strong> รายการ',
        'formatter'=> [ 
            'class' => 'yii\i18n\Formatter',
            'nullDisplay'=>''
        ],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'name',
            'address_1',
            'tel',
            'fax',
            'contact_name',
            [
                'class' => 'kartik\grid\ActionColumn',
                'header' => 'จัดการ',
                'deleteOptions' => ['title'=>'ลบร้านค้า'],
                'viewOptions' => ['title'=>'ดูร้านค้า'],
                'updateOptions' => ['title'=>'แก้ไขร้านค้า'],
            ],
        ],
    ]); ?>
</div>
