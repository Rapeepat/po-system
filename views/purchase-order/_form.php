<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\PurchaseOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="purchase-order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>
    
    <?= $form->field($model, 'date_created')->widget(DateTimePicker::classname(), [
            'options' => ['placeholder' => '(เลือกวันที่ และเวลา)'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd hh:ii:ss',
            ],
        ]); ?>

    <?= $form->field($model, 'supplier_address_1')->textArea(['maxlength' => true]) ?>

    <?= $form->field($model, 'supplier_tel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'supplier_fax')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'supplier_contact_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipping_address_1')->textArea(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipping_contact_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipping_tel')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : '<span class="glyphicon glyphicon-floppy-save"></span> บันทึกการแก้ไข', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
