<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\models\Lot;
use app\models\PurchaseOrder;

/* @var $this yii\web\View */
/* @var $model app\models\PurchaseOrder */

$this->title = 'หน้าสร้างใบสั่งซื้อ';
$lot=Lot::findOne($lot_id);
$lotSites=$lot->lotSites;

$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = [
        'label' => $lotDesc,
        'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchase-order-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <h5>(เลขที่ใบสั่งซื้อล่าสุดที่มีการบันทึกในระบบ = <?= ArrayHelper::getValue(PurchaseOrder::find()->orderBy('id desc')->one(),'id', 'ไม่มี') ?>)</h5>
    <br>
    <?php $form = ActiveForm::begin(); ?>
    <table class="container">   
        <tr>
            <th>ชื่อร้านค้า</th>
            <th>เลขที่ใบสั่งซื้อ</th>
        </tr>

    <?php
    foreach($models as $i=>$model)    
    {?>
        <tr>
            <td>
                <?= $supplierModels[$i]->name ?>
            </td>
            <td>
                <?= $form->field($model, '['.$i.']id', ['enableLabel'=>false,'inputOptions' => [ 'autocomplete' => 'off'],])->textInput(['maxlength' => true])?>
            </td>
        </tr>
    <?php
    }
    ?>

    </table>        
    <div class="form-group">
        <?= Html::submitButton('<span class="glyphicon glyphicon-floppy-open"></span> สร้างใบสั่งซื้อ', ['class' => 'btn btn-success' ]) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
