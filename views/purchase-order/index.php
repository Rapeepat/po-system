<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Lot;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'หน้าเลือก เลขที่ใบสั่งซื้อ';
$lot=Lot::findOne($lot_id);
$lotSites=$lot->lotSites;

$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;

$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = [
        'label' => $lotDesc,
        'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purchase-order-index">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <div>
    <?php
    $prev_date = '';
    foreach ($models as $model)
    {
        if($model->date_created != $prev_date)
        {
            echo '</div>';
            echo '<br> 
            <div class="list-group">
                <div class="list-group-item active">
                    <h4 class="list-group-item-heading">
                        ใบสั่งซื้อ - สร้างวันที่ '.date('Y-m-d',strtotime($model->date_created)).' เวลา '.date('H:i:s',strtotime($model->date_created)).'&emsp;&emsp;'.
                        Html::a(
                            '<span class="glyphicon glyphicon-print"></span> ส่งออกไฟล์ Excel ใบสั่งซื้อ',
                            ['print-po','date_created'=>$model->date_created],
                            ['class'=>'btn btn-default']
                        ).
                    '</h4>
                </div>';
        }
        
        echo Html::a(
            ($model->void?'(ยกเลิก) ':'').'ใบสั่งซื้อเลขที่# '.$model->id.' (ร้านค้า'.$model->supplier->name.')',
            ['view','id'=>$model->id],
            ['class'=>'list-group-item','style'=>$model->void?'background-color:darkgrey;':'']
        );
        
        $prev_date = $model->date_created;
    }
    ?>
    </div>
</div>
