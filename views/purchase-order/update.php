<?php

use yii\helpers\Html;
use app\models\Lot;

/* @var $this yii\web\View */
/* @var $model app\models\PurchaseOrder */

$this->title = 'ใบสั่งซื้อเลขที่# '.Yii::$app->request->get('id');
$lot=Lot::findOne($lot_id);
$lotSites=$lot->lotSites;

$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;

$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = [
        'label' => $lotDesc,
        'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = ['label'=>'หน้าเลือก เลขที่ใบสั่งซื้อ','url'=>['index','lot_id'=>$lot_id]];
$this->params['breadcrumbs'][] = ['label'=>$this->title, 'url'=>['view','id'=>$purchaseOrder->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';

?>
<div class="purchase-order-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <h4>วันที่ <?= date('Y-m-d',strtotime($purchaseOrder->date_created))?></h4>

    <?= $this->render('_form', [
        'model' => $purchaseOrder,
    ]) ?>

</div>
