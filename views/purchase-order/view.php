<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Lot;
use app\models\OfficeSite;
use app\models\PurchaseOrderSiteAmount;
/* @var $this yii\web\View */
/* @var $model app\models\PurchaseOrder */

$this->title = 'ใบสั่งซื้อเลขที่# '.$purchaseOrder->id;
$lot=Lot::findOne($lot_id);
$lotSites=$lot->lotSites;

$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;

$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = [
        'label' => $lotDesc,
        'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = ['label'=>'หน้าเลือก เลขที่ใบสั่งซื้อ','url'=>['index','lot_id'=>$lot_id]];
$this->params['breadcrumbs'][] = $this->title;

$columns= [
    [
        'class' => 'kartik\grid\SerialColumn',
    ],
    [
        'attribute'=>'item_id',
        'label'=>'สินค้า',
        'value'=>'item.name',
        'pageSummary'=>'รวมเป็นเงิน<br>ภาษีมูลค่าเพิ่ม<br>ราคารวมภาษีมูลค่าเพิ่ม',
        'contentOptions'=>['style'=>'border-right-width:3px'],
        'headerOptions'=>['style'=>'border-right-width:3px']
    ],
];

$siteListCount = count($siteList);
foreach($siteList as $site_id)
{
    $site = OfficeSite::findOne($site_id);
    $columns [] =[
        'format'=>['decimal',0],
        'label'=>$site->name,
        'value'=>function ($model)use($site_id){
            $siteAmount = PurchaseOrderSiteAmount::findOne([
                'purchase_order_id'=>$model->purchase_order_id,
                'item_id'=>$model->item_id,
                'site_id'=>$site_id,
            ]);
            return $siteAmount==null? 0:$siteAmount->amount;
        },
    ];
}
$columns [] = [
    'class' => '\kartik\grid\FormulaColumn',
    'value' => function ($model, $key, $index, $widget) use ($siteListCount){
        $p = compact('model', 'key', 'index');
        $sum = 0;
        for($i=2;$i<2+$siteListCount;$i++) $sum=$sum+$widget->col($i, $p);
        return $sum;
    },   
    'contentOptions'=>['style'=>'border-left-width:3px'],
    'headerOptions'=>['style'=>'display:none']
];
$columns [] = [
    'attribute'=>'price',
    'headerOptions'=>['style'=>'display:none']
];
$columns [] = [
    'class' => '\kartik\grid\FormulaColumn',
    'label' => 'ราคารวม',
    'value' => function ($model, $key, $index, $widget) use ($siteListCount) {
        $p = compact('model', 'key', 'index');
        return $widget->col(2+$siteListCount, $p) * $widget->col(3+$siteListCount, $p);
    },
    'format'=>'decimal',
    'pageSummaryOptions'=>['class'=>'gen-vat text-right'],
    'pageSummary'=>true,
    'contentOptions'=>['class'=>'text-right'],
    'headerOptions'=>['style'=>'display:none']
];
$beforeHeader = [
    ['content'=>'รายการ', 'options'=>['colspan'=>2, 'class'=>'text-center success']],
    ['content'=>'จำนวน/หน่วยงาน', 'options'=>['colspan'=>$siteListCount, 'class'=>'text-center success ', 'style'=>'border-left-width:3px']],
    ['content'=>'รวมเป็นจำนวน', 'options'=>['rowspan'=>2, 'class'=>'text-center success', 'style'=>'border-left-width:3px']],
    ['content'=>'ราคา/หน่วย', 'options'=>['rowspan'=>2, 'class'=>'text-center success']],
    ['content'=>'รวมเป็นเงิน', 'options'=>['rowspan'=>2, 'class'=>'text-center success']]
];
?>
<style>
    .lit{
        padding:20px 0 0 10px;
    }
    .var{
        border-bottom:1pt solid black; padding-top:20px;
    }
</style>
<div class="purchase-order-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <h4>วันที่ <?= date('Y-m-d',strtotime($purchaseOrder->date_created))?></h4>
    <p>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> แก้ไขใบสั่งซื้อ', ['update', 'id' => $purchaseOrder->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(($purchaseOrder->void?'<span class="glyphicon glyphicon-repeat"></span> คืนสภาพใบสั่งซื้อ':'<span class="glyphicon glyphicon-remove"></span> ยกเลิกใบสั่งซื้อ'), ['void', 'id' => $purchaseOrder->id], [
            'class' => ($purchaseOrder->void?'btn btn-success':'btn btn-danger'),
            'data' => [
                'confirm' => ($purchaseOrder->void?'กรุณายืนยันการคืนสภาพใบสั่งซื้อ':'กรุณายืนยันการยกเลิกใบสั่งซื้อ').' (การยกเลิกหรือคืนสภาพใบสั่งซื้อ มีผลต่อการคำนวณงบประมาณคงเหลือเท่านั้น)',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <h4>
    <table width="100%">
        <tr>
            <td width="6%"  class="lit">ชื่อ</td>
            <td width="40%" class="var">ร้านค้า <?= $purchaseOrder->supplier->name ?></td>
            <td width="6%"  class="lit">แฟกซ์</td>
            <td width="21%" class="var"><?= $purchaseOrder->supplier_fax?></td>
            <td width="6%"  class="lit">ติดต่อ</td>
            <td width="21%" class="var"><?= $purchaseOrder->supplier_contact_name?></td>
        </tr>
        <tr>
            <td class="lit">ที่อยู่</td>
            <td colspan='3' class="var"><?= $purchaseOrder->supplier_address_1 ?></td>
            <td class="lit">โทร</td>
            <td class="var"><?= $purchaseOrder->supplier_tel?></td>
        </tr>
        <tr>
            <td class="lit">จัดส่ง</td>
            <td colspan='5' class="var"><?= $purchaseOrder->shipping_address_1 ?></td>
        </tr>
    </table>
    </h4>
    <br>
    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''], //set "(not set)" to space
            'summary' => 'แสดง <strong>{begin}-{end}</strong> จากทั้งหมด <strong>{totalCount}</strong> รายการ',
            'showPageSummary' => true,
            'columns' => $columns,
            'beforeHeader'=>[
                [
                    'columns'=>$beforeHeader,
                    //'options'=>['class'=>'skip-export'] // remove this row from export
                ]
            ],
    ]) ?>

</div>
<?php 
$this->registerJsFile('@web/js/genvat.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>