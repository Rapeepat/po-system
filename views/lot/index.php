<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider1 yii\data\ActiveDataProvider */
/* @var $dataProvider2 yii\data\ActiveDataProvider */

$this->title = 'หน้าจัดการงบประมาณ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lot-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-folder-open"> </span> สร้างงบประมาณใหม่', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <br> 
    <div>
        <div class='list-group'>
            <a class="list-group-item active">
                <h4 class="list-group-item-heading">งบประมาณ หมวดทรัพย์สิน</h4> 
                <p class="list-group-item-text">(รายการจะถูกแสดงไม่เกิน 10 รายการ)</p>
            </a>
            <?php
                
                $models = $dataProvider1->getModels();
                foreach ($models as $model)
                {
                    echo '<div class="list-group-item">';
                    echo Html::a(
                        '#'.$model->id.' '.$model->itemType->detail.' งบวันที่ '.$model->lot_date,
                        ['view','id'=>$model->id],
                        ['style'=>'width:40%;vertical-align:middle;display:inline-block;',]
                    );
                    echo '<div class="btn-group" role="group" style="width:60%" aria-label="...">';
                    echo Html::a(
                        'คำขอสั่งซื้อ',
                        ['/request','lot_id'=>$model->id],    
                        [
                            'style'=>'width:33%',
                            'class'=>"btn btn-default",
                        ]
                    );
                    echo Html::a(
                        'เปรียบเทียบราคา',
                        ['/offer','lot_id'=>$model->id],    
                        [
                            'style'=>'width:33%',
                            'class'=>"btn btn-default",
                        ]
                    );
                    echo Html::a(
                        'ใบสั่งซื้อ',
                        ['/purchase-order','lot_id'=>$model->id],    
                        [
                            'style'=>'width:33%',
                            'class'=>"btn btn-default",
                        ]
                    );
                    echo '</div></div>';
                }
                echo Html::a(
                        'ดูงบประมาณอื่นๆ ภายใต้งบประมาณหมวดทรัพย์สิน',
                        ['list','LotSearch[item_type_id]'=>1],
                        ['class' => 'list-group-item list-group-item-info']
                    );
            ?>
        </div>
        <br>
        <div class='list-group'>
            <a class="list-group-item active">
                <h4 class="list-group-item-heading">งบประมาณ หมวดค่าใช้จ่าย</h4> 
                <p class="list-group-item-text">(รายการจะถูกแสดงไม่เกิน 10 รายการ)</p>
            </a>
            <?php
                
                $models = $dataProvider2->getModels();
                foreach ($models as $model)
                {
                    echo '<div class="list-group-item">';
                    echo Html::a(
                        '#'.$model->id.' '.$model->itemType->detail.' งบวันที่ '.$model->lot_date,
                        ['view','id'=>$model->id],
                        ['style'=>'width:40%;vertical-align:middle;display:inline-block;',]
                    );
                    echo '<div class="btn-group" role="group" style="width:60%" aria-label="...">';
                    echo Html::a(
                        'คำขอสั่งซื้อ',
                        ['/request','lot_id'=>$model->id],    
                        [
                            'style'=>'width:33%',
                            'class'=>"btn btn-default",
                        ]
                    );
                    echo Html::a(
                        'เปรียบเทียบราคา',
                        ['/offer','lot_id'=>$model->id],    
                        [
                            'style'=>'width:33%',
                            'class'=>"btn btn-default",
                        ]
                    );
                    echo Html::a(
                        'ใบสั่งซื้อ',
                        ['/purchase-order','lot_id'=>$model->id],    
                        [
                            'style'=>'width:33%',
                            'class'=>"btn btn-default",
                        ]
                    );
                    echo '</div></div>';
                }
                echo Html::a(
                        'ดูงบประมาณอื่นๆ ภายใต้งบประมาณหมวดค่าใช้จ่าย',
                        ['list','LotSearch[item_type_id]'=>2],
                        ['class' => 'list-group-item list-group-item-info']
                    );
            ?>
        </div>
    </div>
</div>
