<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lot */

$this->title = 'แก้ไขงบประมาณ: #'.$model->id.' งบวันที่ '.$model->lot_date;
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => '#'.$model->id.' งบวันที่ '.$model->lot_date, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="lot-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'lotSiteModels' => $lotSiteModels,
    ]) ?>

</div>
