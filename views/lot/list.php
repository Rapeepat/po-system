<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\ItemType;
use \kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'งบประมาณทั้งหมด';
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lot-list">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-folder-open"> </span> สร้างงบประมาณใหม่', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'lot_date',
            [
                'attribute'=>'item_type_id',
                'value'=>'itemType.detail',
                'filter'=>ArrayHelper::map(ItemType::find()->all(), 'id', 'detail'),
                'filterInputOptions'=>['class' => 'form-control','prompt'=> '(เลือกชนิดของสินค้า)'],
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'header' => 'รายละเอียด',
                'template' => '{view} {update}',
            ],
        ],
        'summary' => 'แสดง <strong>{begin}-{end}</strong> จากทั้งหมด <strong>{totalCount}</strong> รายการ',
    ]); ?>
</div>