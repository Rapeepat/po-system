<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\OfficeSite;
use app\models\LotSite;
/* @var $this yii\web\View */
/* @var $model app\models\Lot */

$this->title = '#'.$model->id.' งบวันที่ '.$model->lot_date ;
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #corner {
        border: 2px solid #666666; 
        border-radius: 25px;
        padding:10px 20px 10px 20px;  
    }
</style>
<div class="lot-view">

    <h1><?= Html::encode($this->title)?></h1>

    <h3>เลือกรายการที่ต้องการจะทำ</h3>
    <div id='corner'>
        <h4><ul>
            <li><?=Html::a(
                    'คำขอสั่งซื้อ',
                    ['/request','lot_id'=>$model->id],
                    ['class'=>'btn btn-info',"style"=>"width: 50%;"])?>
            </li>
            <br>
            <li><?=Html::a(
                    'เปรียบเทียบราคา',
                    ['/offer','lot_id'=>$model->id],
                    ['class'=>'btn btn-info',"style"=>"width: 50%;"])?>
            </li>
            <br>
            <li><?=Html::a(
                    'ใบสั่งซื้อ',
                    ['/purchase-order','lot_id'=>$model->id],
                    ['class'=>'btn btn-info',"style"=>"width: 50%;"])?>
            </li>
        </ul></h4>
    </div>
    <br>
    
    
    <h3>รายละเอียดของงบประมาณ</h3>
    
    <div id='corner'>
        <p>
            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> แก้ไข', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('<span class="glyphicon glyphicon-remove"></span> ลบงบประมาณนี้', ['delete', 'id' => $model->id], 
                    [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'กรุณายืนยันหากต้องการลบงบประมาณนี้',
                            'method' => 'post',
                        ],
                    ]) ?>
        </p>
        <br>
        <h4>รายละเอียดทั่วไป</h4>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'lot_date',
                [
                    'label' => 'หมวดสินค้า',
                    'attribute'=>'itemType.mainType.detail',
                ],
                'itemType.detail',
            ],
            'template' => "<tr><th class='col-md-6'>{label}</th><td class='col-md-6'>{value}</td></tr>",
        ]) ?>

        <br>
        <h4>รายละเอียดงบประมาณ รายหน่วยงาน</h4>
        <table class="table table-striped table-bordered">
            <tr>
                <th class='col-md-2'>หน่วยงาน</th>
                <th class='col-md-2'>งบประมาณหลัก</th>
                <th class='col-md-2'>งบประมาณอื่นๆ</th>
                <th class='col-md-2' style="border-left-width: 4px;">งบคงเหลือ ของงบปัจจุบัน</th>
                <th class='col-md-2'>งบคงเหลือ 12 เดือน (ตั้งแต่ต้นปี)</th>
                <th class='col-md-2'>งบคงเหลือ 6 เดือน (ตั้งแต่มกราคม หรือกรกฎาคม)</th>
            </tr>
            <?php
                $sites = OfficeSite::find()->all();
                foreach($sites as $site)
                {
                    $lotSite = LotSite::findOne(['lot_id'=>$model->id,'site_id'=>$site->id]);
                    echo '<tr>';
                    echo '<td>'.$site->name.'</td>';
                    echo '<td>'.($lotSite==null?'ไม่มีการตั้งงบประมาณ':number_format($lotSite->main_budget,2)).'</td>';
                    echo '<td>'.($lotSite==null || $lotSite->etc_budget==null?'':number_format($lotSite->etc_budget,2)).'</td>';
                    echo '<td style="border-left-width: 4px;">'.($lotSite==null?'':number_format($lotSite->remainingBudget,2)).'</td>';
                    echo '<td>'.($lotSite==null?'':number_format($lotSite->remaining12Budget,2)).'</td>';
                    echo '<td>'.($lotSite==null?'':number_format($lotSite->remaining6Budget,2)).'</td>';
                    echo '</tr>';
                }
            ?>
        </table>
    </div>
</div>
