<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lot */

$this->title = 'สร้างงบประมาณใหม่';
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lot-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'lotSiteModels' => $lotSiteModels,
    ]) ?>

</div>
