<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\ItemType;
use kartik\widgets\ActiveField;
use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Lot */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lot-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'lot_date')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => '(เลือกวันที่งบประมาณ)'],
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd',
            ],
            'disabled' => ($model->isNewRecord ? false:true),
        ]);
    ?>

    <?= $form->field($model, 'item_type_id')->dropDownList(
            ItemType::getDropDown(),
            [
                'prompt' => '(เลือกชนิดของสินค้า สำหรับงบประมาณที่จะสร้าง)',
                'disabled' => ($model->isNewRecord ? false:true),
            ]
        ) 
    ?>

    <br><h3>งบประมาณ รายหน่วยงาน</h3><br>
    
    <div class="container">
        <div class="row">
            <div class="col-md-2"><b>หน่วยงาน</b></div>
            <div class="col-md-4"><b>งบประมาณหลัก</b></div>
            <div class="col-md-4"><b>งบประมาณอื่นๆ</b></div>
        </div>
        <br>
        <?php 
        $i = 0;
        foreach ($lotSiteModels as $lotSiteModel)
        {
            echo '<div class="row">';
            echo '<div class="col-md-2">'.$lotSiteModel->site->name.'</div>';
            echo $form->field($lotSiteModel, '['.$i.']main_budget', [              
                'options' => [ 'class' => 'col-md-4'],  
                'template' => '<div class="input-group"><span class="input-group-addon">฿</span>{input}</div>{error}',
            ]);
            echo $form->field($lotSiteModel, '['.$i.']etc_budget', [                
                'options' => [ 'class' => 'col-md-4'],
                'template' => '<div class="input-group"><span class="input-group-addon">฿</span>{input}</div>{error}',
            ]);
            echo '</div>';
            $i++;
        }?>
    </div>

    <br>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span class="glyphicon glyphicon-floppy-open"></span> สร้างงบประมาณ' : '<span class="glyphicon glyphicon-floppy-save"></span> บันทึกการแก้ไข', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'onclick'=>'return validate_lotsite();']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

</div>
<?php 
$this->registerJsFile('@web/js/validatelotsite.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
