<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Collapse;
/* @var $this yii\web\View */
/* @var $model app\models\OfferContact */

$model = $models[0];
$offer = $model->offer;
$this->title = 'แก้ไขข้อมูลการติดต่อกับร้านค้า';

$lot = $offer->lot;
$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;


$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = [
        'label' => $lotDesc,
        'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = ['label' => 'หน้าเลือก เลขที่ใบขอเสนอราคา', 'url' => ['index', 'lot_id'=>$lot->id]];
$this->params['breadcrumbs'][] = ['label' => 'ใบขอเสนอราคา #'.$offer->id, 'url' => ['view', 'id'=>$offer->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    #corner {
        border: 2px solid #666666; 
        border-radius: 25px;
        padding:10px 20px 10px 20px;  
    }
</style>

<div class="offer-contact-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="offer-contact-form">

        <?php $form = ActiveForm::begin(); 
        
            //For each contact detail of a supplier.
            foreach($models as $i=>$model)
            {
                $content = 
                    '<h3>ข้อมูลเกี่ยวกับการติดต่อ</h3>'.
                    '<div id="corner">'.
                    $form->field($model, '['.$i.']supplier_address_1')->textArea(['maxlength' => true]).
                    $form->field($model, '['.$i.']supplier_tel')->textInput(['maxlength' => true]).
                    $form->field($model, '['.$i.']supplier_fax')->textInput(['maxlength' => true]).
                    $form->field($model, '['.$i.']supplier_contact_name')->textInput(['maxlength' => true]).
                    '</div>'.
                    '<h3>ข้อมูลเกี่ยวกับการจัดส่งสินค้า</h3>'.    
                    '<div id="corner">'.
                    $form->field($model, '['.$i.']shipping_address_1')->textArea(['maxlength' => true]).
                    $form->field($model, '['.$i.']shipping_contact_name')->textInput(['maxlength' => true]).
                    $form->field($model, '['.$i.']shipping_tel')->textInput(['maxlength' => true]).
                    '</div>';
                        
                        
                echo Collapse::widget([
                    'items' => [
                        [
                            'label' => 'ข้อมูลการติดต่อ ร้านค้า'.$model->supplier->name.' <span class="caret"></span>',        
                            'content' => $content,
                            //'contentOptions' => ['class' => 'in'] //expand by default
                        ], 
                    ],
                    'encodeLabels'=>false,
                ]);
                
                
            }
            ?>

        <div class="form-group">
            <?= Html::submitButton('บันทึกการเปลี่ยนแปลง', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>