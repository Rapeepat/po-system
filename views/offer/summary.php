<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Supplier;
use app\models\OfferPrice;
use app\models\Request;
use yii\bootstrap\Collapse;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lot = $offer->lot;
$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;

$this->title = 'หน้าสรุป';
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = [
        'label' => $lotDesc,
        'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = ['label' => 'หน้าเลือก เลขที่ใบขอเสนอราคา', 'url' => ['index', 'lot_id'=>$lot->id]];
$this->params['breadcrumbs'][] = ['label' => 'ใบขอเสนอราคา #'.$offer->id, 'url' => ['view', 'id'=>$offer->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="offer-summary">
    
    <h1><?= Html::encode($this->title) ?></h1>
    
    <br>
    <?= Html::a('สร้างใบสั่งซื้อ ทุกร้านค้า', ['/purchase-order/create', 'offer_id'=>$offer->id, 'supplier_id'=>-1], ['class'=>'btn btn-success'])?>
    <br><br>
    <?php
    foreach($offerItemBySupplier as $supplier_id => $dataProvider)
    {
        $supplierName = Supplier::findOne($supplier_id)->name;
        $content = GridView::widget([
            'dataProvider' => $dataProvider,
            'summary' => 'แสดง <strong>{begin}-{end}</strong> จากทั้งหมด <strong>{totalCount}</strong> รายการ',
            'showPageSummary' => true,
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width'=>'5%',
                ],
                [
                    'attribute'=>'item',
                    'label'=>'สินค้า',
                    'value'=>'item.name',
                    'pageSummary'=>'รวมเป็นเงิน<br>ภาษีมูลค่าเพิ่ม<br>ราคารวมภาษีมูลค่าเพิ่ม',
                    'width'=>'35%',
                ],
                [
                    'label'=>'ราคาต่อหน่วย',
                    'value'=>function ($model/*, $key, $index, $column*/)use ($supplier_id){
                        return OfferPrice::findOne([
                                    'supplier_id'=>$supplier_id,
                                    'item_id'=>$model->item_id,
                                    'offer_id'=>$model->offer_id,
                                ])->price;
                    },
                    'width'=>'20%',
                ],
                [
                    'label'=>'จำนวน',
                    'value'=>function ($model/*, $key, $index, $column*/){
                        $sumAmount = 0;
                        foreach($model->offer->offerSiteLists as $offerSiteList)
                        {
                            $request = Request::findOne([
                                'lot_id' =>$model->offer->lot_id,
                                'site_id'=>$offerSiteList->site_id,
                                'item_id'=>$model->item_id,
                            ]);
                            $amount = $request==null?0:$request->amount;
                            $sumAmount += $amount;
                        }
                        return $sumAmount;
                    },
                    'width'=>'20%',
                ],
                [
                    'class' => '\kartik\grid\FormulaColumn',
                    'label' => 'ราคารวม',
                    'value' => function ($model, $key, $index, $widget) {
                        $p = compact('model', 'key', 'index');
                        return $widget->col(2, $p) * $widget->col(3, $p);
                    },
                    'format'=>'decimal',
                    'pageSummaryOptions'=>['class'=>'gen-vat text-right'],
                    'pageSummary'=>true,
                    'width'=>'20%',
                    'contentOptions'=>['class'=>'text-right']
                ],
            ],
        ]).
        Html::a('สร้างใบสั่งซื้อ ร้าน'.$supplierName, 
            ['/purchase-order/create', 'offer_id'=>$offer->id, 'supplier_id'=>$supplier_id], 
            ['class'=>'btn btn-success']
        );

        echo Collapse::widget(['encodeLabels'=>false,'items' => [[
            'label'=>'สรุปร้านค้า'.$supplierName.' <span class="caret"></span>',
            'content'=>$content,
        ]]]);
    }
    ?>
</div>

<?php 
$this->registerJsFile('@web/js/genvat.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>