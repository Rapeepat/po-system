<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\web\Request;
use yii\bootstrap\ActiveForm;
use app\models\Supplier;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Offer */

$lot = $offer->lot;
$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;

$this->title = 'แก้ไขใบขอเสนอราคา #' . $offer->id;
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = [
        'label' => $lotDesc,
        'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = ['label' => 'หน้าเลือก เลขที่ใบขอเสนอราคา', 'url' => ['index', 'lot_id'=>$lot->id]];
$this->params['breadcrumbs'][] = ['label'=>'ใบขอเสนอราคา #'.$offer->id,'url'=>['view','id'=>$offer->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';

$maxCol = count($supplierList);
$maxRow = count($itemList);

$supplierData = ArrayHelper::map(Supplier::find()->all(),'id','name');
        
?>

<style>
    td,th
    {
        padding:5px 10px 5px 10px;
    }
</style>
<div class="offer-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin(); ?>
    <br>
</div>
</div>
<div  class="container-fluid">
    <table>
        <tr>
            <th style="padding:5px 0px 5px 0px;">#</th><th>สินค้า</th>
            <?php
                for($col=0;$col<$maxCol;$col++)
                {
                    echo '<th style="min-width:100px">'.Supplier::findOne($supplierList[$col])->name.'</div>';
                }
            ?>
            <th>เลือกร้านค้า</th>
            <th>หมายเหตุ</th>
        </tr>
        <?php
            $i=0;
            for($row=0;$row<$maxRow;$row++)
            {
                echo '<tr><td style="padding:5px 0px 5px 0px;">'.($row+1).'</td>';
                echo '<td>';
                echo $form->field($models[$i], '['.$i.']item_id', ['enableLabel'=>false])->widget(
                        Select2::className(),
                        [
                            'data' => [$models[$i]->item_id => $models[$i]->item->name],
                            'disabled' => true,
                        ]
                    );
                echo '</td>';
                $selectableSupplier = [];
                for($col=0;$col<$maxCol;$col++)
                {
                    $supplier_id = $supplierList[$col];
                    if(!$models[$i]->isNewRecord)$selectableSupplier[$supplier_id]=$supplierData[$supplier_id];
                    echo '<td>';
                    echo $form->field($models[$i],'['.$i.']price',[
                            'enableLabel'=>false,
                            'inputOptions' => [ 'autocomplete' => 'off'],
                            'options'=>['supplier'=>$supplier_id],
                        ])->textInput([
                            'readOnly' => $models[$i]->isNewRecord?true:false,
                        ]);
                    $i++;
                    echo '</td>';
                }
                echo '<td>';
                echo $form->field($itemList[$row],'['.$row.']selected_supplier_id', ['enableLabel'=>false])->widget(
                        Select2::className(),
                        [
                            'data' => $selectableSupplier,
                            'options'=>['prompt'=>'เลือกร้านค้าที่จะซื้อสินค้านี้'],
                            'pluginOptions'=>['allowClear'=>true,],
                        ]
                    );
                echo '</td><td>';
                echo $form->field($itemList[$row],'['.$row.']annotation', ['enableLabel'=>false])->textarea(['style'=>'min-width:250px; height:34px']);
                echo '</td></tr>';
            }
        ?>
        <tr>
            <?php for($col=0;$col<$maxCol+2;$col++) echo '<td></td>'; //maxCol (supplier) + 1 num col + 1 item col
            ?>
            <td>
                <?= Html::button('เลือกร้านค้า โดยอัตโนมัติ', ['class'=>'btn btn-info select-supplier'])?>
            </td>
        </tr>
    </table>
    
    <div class="form-group">
        <?= Html::submitButton('บันทึกการแก้ไข', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    
</div>
<div>
<?php 
$this->registerJsFile('@web/js/selsupplier.js', ['depends' => [\yii\web\JqueryAsset::className()]]);