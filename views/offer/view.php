<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\OfferPrice;
use app\models\Supplier;
use app\models\Request;
/* @var $this yii\web\View */
/* @var $model app\models\Offer */

$lot = $offer->lot;
$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;

$this->title = 'หน้าสรุปเปรียบเทียบราคา #'.$offer->id;
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = [
        'label' => $lotDesc,
        'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = ['label' => 'หน้าเลือก การสรุปเปรียบเทียบราคา', 'url' => ['index', 'lot_id'=>$lot->id]];
$this->params['breadcrumbs'][] = $this->title;


$columns = [
    [   
        'class' => 'kartik\grid\SerialColumn',
        'headerOptions' => ['class'=>'success'],
        'pageSummaryOptions'=>['style'=>'display:none'],
    ],
    [
        'attribute' => 'item.name',
        'headerOptions' => ['class'=>'success'],
        'pageSummaryOptions'=>['style'=>'display:none'],
    ],
    [
        'label'=>'จำนวนรวม',
        'value'=>function($model){
            return $model->getSumAmount();
        },
        'pageSummary'=>'รวมเป็นเงิน<br>ภาษีมูลค่าเพิ่ม<br>ราคารวมภาษีมูลค่าเพิ่ม',
        'headerOptions' => ['class'=>'success'],
        'pageSummaryOptions'=>['class'=>'text-right','colspan'=>'4'],
    ],
    [
        'class'=>'kartik\grid\ExpandRowColumn',
        'expandOneOnly'=>true,
        'value'=>function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detailRowCssClass' => GridView::TYPE_DEFAULT,
        'expandAllTitle' => 'แสดงจำนวนรายหน่วยงาน',
        'expandTitle' => 'แสดงจำนวนรายหน่วยงาน',
        'collapseAllTitle' => 'ซ่อนทั้งหมด',
        'collapseTitle' => 'ซ่อน',
        'label'=>'หน่วยงานที่สั่งซื้อ (จำนวน)',
        'detail'=>function ($model, $key, $index, $column){
            $sites = '';
            foreach($model->offer->offerSiteLists as $offerSiteList) 
            {
                $request = Request::findOne([
                    'lot_id' =>$model->offer->lot_id,
                    'site_id'=>$offerSiteList->site_id,
                    'item_id'=>$model->item_id,
                ]);
                $amount = $request==null?0:$request->amount;
                if ($amount > 0)
                    $sites = $sites.$offerSiteList->site->name.
                        ' ('.$amount.')'.
                        '<br>';
            }
            return $sites;
        },
        'format'=>'html',
        'headerOptions' => ['class'=>'success'],
        'pageSummaryOptions'=>['style'=>'display:none'],
    ],
];
        
$striped=false;
foreach($supplierList as $i => $supplier_id)
{
    $striped=!$striped;
    $columns[] = [
        'label'=>Supplier::findOne($supplier_id)->name,
        'value'=>function ($model/*, $key, $index, $column*/)use ($supplier_id){
            $offerPrice = OfferPrice::findOne([
                'offer_id'=>$model->offer_id,
                'item_id'=>$model->item_id,
                'supplier_id'=>$supplier_id,
            ]);
            
            if ($offerPrice==null) return 'ไม่มีคำขอเสนอราคา';
            elseif ($offerPrice->price==null) return '-';
            else   return $offerPrice->price;
        },
        'contentOptions'=>function ($model/*, $key, $index, $column*/)use ($supplier_id){
            $ret = [];
            if($supplier_id == $model->selected_supplier_id) $ret['class'] = 'selected';
            return $ret;
        },
        'headerOptions' => ['class'=>'text-center Header'.($striped?1:2)],        
        'pageSummary'=>false,
        'pageSummaryOptions'=>[
            'style'=>'display:none;',
        ],
    ];
        
    //Dummy column to use as a summary (zero out all the none-selected supplier
    $columns[] = [
        'class' => '\kartik\grid\FormulaColumn',
        'label'=>'temp',
        'value' => function ($model, $key, $index, $widget)use ($supplier_id, $i){           
            $p = compact('model', 'key', 'index');
            $ret = 0;
            if($supplier_id == $model->selected_supplier_id) $ret = $widget->col(2*$i+4,$p)*$widget->col(2,$p);
            return $ret;
        }, 
        'headerOptions'=>[
            'style'=>'display:none;',
        ],
        'contentOptions'=>[
            'style'=>'display:none;',
        ],
        'pageSummary'=>true,
        'pageSummaryOptions'=>[
            'class'=>'gen-vat text-right',  //class gen-vat will be used by 1)genvat.js
        ],
    ];
}
$columns[]=[
    'label'=>'ร้านค้าที่ถูกเลือก',
    'value'=>'selectedSupplier.name',
    'contentOptions' => ['class'=>'info'],
    'headerOptions' => ['class'=>'info'],
    ];
$columns[]=[
    'attribute'=>'annotation',
    'contentOptions' => ['class'=>'info'],
    'headerOptions' => ['class'=>'info'],
    'enableSorting'=>false,
]
?>
<style>
    .selected {
        background-color: #97b6e9;
    }
</style>
<div class="offer-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <table width="100%">
        <tr>
            <td width="32%">
            <?= Html::a('<span class="glyphicon glyphicon-paste"></span> ส่งออกไฟล์ Excel ใบขอเสนอราคา', ['offer-print','id'=>$offer->id], ['class' => 'btn btn-success', 'style'=>'width:100%']) ?>  
            </td>
            <td width="2%"></td>
            <td width="32%">
            <?= Html::a('<span class="glyphicon glyphicon-sort-by-attributes"></span> ส่งออกไฟล์ Excel ใบเปรียบเทียบราคา', ['offer-compare-print','id'=>$offer->id, 'summarize'=>0], ['class' => 'btn btn-success', 'style'=>'width:100%']) ?>       
            </td>
            <td width="2%"></td>
            <td width="32%">
            <?= Html::a('<span class="glyphicon glyphicon-list-alt"></span> ส่งออกไฟล์ Excel ใบสรุปการขออนุมัติจัดซื้อ', ['offer-compare-print','id'=>$offer->id, 'summarize'=>1], ['class' => 'btn btn-success', 'style'=>'width:100%']) ?>     
            </td>  
        <tr> 
        </tr>  
            <td width="32%"><br>
            <?= Html::a('<span class="glyphicon glyphicon-bitcoin"></span> แก้ไขราคา และเลือกร้านค้า', ['update','id'=>$offer->id], ['class' => 'btn btn-primary', 'style'=>'width:100%']) ?>
            </td>
            <td width="2%"></td>
            <td width="32%"><br>
            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> แก้ไข ข้อมูลการติดต่อ', ['update-contact','id'=>$offer->id], ['class' => 'btn btn-primary', 'style'=>'width:100%']) ?>
            </td>
            <td width="2%"></td>
            <td width="32%"><br>
            <?= Html::a('<span class="glyphicon glyphicon-list-alt"></span> ไปยังหน้าสรุป และสร้างใบสั่งซื้อ', ['summary','id'=>$offer->id], ['class' => 'btn btn-info', 'style'=>'width:100%']) ?>  
            </td>    
        </tr>
    </table>
</div>
 </div><!-- Dummy "end DIV" so the gridview can extend more-->
   
    <div class="container-fluid">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $columns,
        
        //START OF MISC GRID CONFIGURATION
        'showPageSummary' => true,
        'summary' => 'แสดง <strong>{begin}-{end}</strong> จากทั้งหมด <strong>{totalCount}</strong> รายการ',
        'formatter'=> [ 
            'class' => 'yii\i18n\Formatter',
            'nullDisplay'=>'(ยังไม่ถูกเลือก)'
        ],
        'resizableColumns' => false,
        //END   OF MISC GRID CONFIGURATION
        
        //START OF PERFECTSCROLLBAR
        'floatHeader'=>true,
        'perfectScrollbar'=>true,
        'perfectScrollbarOptions'=>[
            'wheelPropagation'=>true,
        ],
        'containerOptions'=>['style'=>'height:600px;'],  //Set grid height
        //END   OF PERFECTSCROLLBAR
    ]); ?>
    </div>
 <div><!-- Dummy "start DIV" so the gridview can extend more-->
<?php 
$this->registerJsFile('@web/js/genvat.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile('@web/css/StripedHeader.css');

//Reset the perfect scrollbar in grid view. 
$this->registerJs("
    $(window).load(function(){
        $('.ps-container').scrollTop(0).scrollLeft(0);
    });
");
?>