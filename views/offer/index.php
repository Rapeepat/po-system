<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Lot;
/* @var $this yii\web\View */
/* @var $searchModel app\models\OfferSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'หน้าเลือก การสรุปเปรียบเทียบราคา';
$lot=Lot::findOne($lot_id);
$lotSites=$lot->lotSites;

$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;

$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = [
        'label' => $lotDesc,
        'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <br> 
    <div class='list-group'>
        <a class="list-group-item active">
            <h4 class="list-group-item-heading">เลขที่การสรุปเปรียบเทียบราคา</h4> 
            <p class="list-group-item-text">(เนื่องจากมีการสร้างหน้าสรุปเปรียบเทียบราคาหลายครั้ง กรุณาเลือกเลขที่ที่ต้องการ)</p>
        </a>
    <?php
        foreach ($models as $model)
        {
            echo Html::a(
                'การสรุปเปรียบเทียบราคา เลขที่# '.$model->id,
                ['view','id'=>$model->id],
                ['class'=>'list-group-item',]
            );
        }
    ?>
        
    </div>
</div>
