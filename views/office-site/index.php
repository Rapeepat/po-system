<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OfficeSiteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'หน้าจัดการหน่วยงาน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="office-site-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<span class="glyphicon glyphicon-plus-sign"> </span> เพิ่มหน่วยงานใหม่', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => 'แสดง <strong>{begin}-{end}</strong> จากทั้งหมด <strong>{totalCount}</strong> รายการ',
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'name',
            'tel',
            'contact_name',
            'address_1',
            [
                'class' => 'kartik\grid\ActionColumn',
                'header' => 'จัดการ',
                'deleteOptions' => ['title'=>'ลบหน่วยงาน'],
                'viewOptions' => ['title'=>'ดูหน่วยงาน'],
                'updateOptions' => ['title'=>'แก้ไขหน่วยงาน'],
            ],
        ],
    ]); ?>
</div>
