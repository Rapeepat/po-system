<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Request;
use app\models\Lot;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'หน้าจัดการคำขอสั่งซื้อสินค้า';
$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = [
        'label' => $lotDesc,
        'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = $this->title;

$lotSites=$lot->lotSites;

$columns = [
    [
        'class' => 'kartik\grid\SerialColumn',
        'pageSummary'=>'** ราคาที่แสดงเป็นเพียง การประมาณจากการซื้อล่าสุด **',
        'pageSummaryOptions'=>['class'=>'text-center','colspan'=>'2'],
    ],
    [   
        'attribute'=>'item.name',
        'enableSorting' => false,
        'pageSummaryOptions'=>['style'=>'display:none'],
        'headerOptions' => ['style'=>'min-width:200px;'],
    ],
    [
        'attribute'=>'item.unit',
        'pageSummaryOptions'=>['style'=>'display:none'],
    ],
    [
        'label'=>'ราคา/หน่วย',
        'value' => function ($model) use ($priceList){
            return $priceList[$model->item_id];
        },
        'pageSummary'=>'รวมเป็นเงิน<br>ภาษีมูลค่าเพิ่ม<br>ราคารวมภาษีมูลค่าเพิ่ม',
        'pageSummaryOptions'=>['colspan'=>'3','class'=>'text-right'],
    ],
    [
        'label'=>'ร้านค้า',
        'value' => function ($model) use ($supplierList){
            return $supplierList[$model->item_id];
        }, 
        'pageSummaryOptions'=>['style'=>'display:none'],
        'headerOptions' => ['style'=>'min-width:150px;'],
    ],
];
$beforeHeader = [
    ['content'=>'รายการ', 'options'=>['colspan'=>2, 'class'=>'text-center success']],
    ['content'=>'การจัดซื้อล่าสุด', 'options'=>['colspan'=>3, 'class'=>'text-center success']]
];
//CHANGE COLSPAN TO ANOTHER VALUE IF YOU'RE ADDING ADDITIONAL COLUMN S
$striped=false;
foreach ($lotSites as $lotSiteKey => $lotSite)
{
    $striped = !$striped;
    $columns[] = [
        'value' => function ($model/*, $key, $index, $column*/)use ($lotSite){
            $request = Request::findOne([   
                'item_id' => $model->item_id,
                'lot_id' => $model->lot_id,
                'site_id' => $lotSite->site_id,
            ]);
            return $request==null||$request->amount==0?'ไม่มีคำขอ':$request->amount;
        },
        'contentOptions' => function ($model/*, $key, $index, $column*/)use ($lotSite){
            $request = Request::findOne([   
                'item_id' => $model->item_id,
                'lot_id' => $model->lot_id,
                'site_id' => $lotSite->site_id,
            ]);
            return $request==null||$request->amount==0?['colspan'=>'2','class'=>'text-right','style'=>'border-left-width:3px;']:['style'=>'border-left-width:3px;'];
        },
        'headerOptions' => ['style'=>'border-left-width:3px;'],
        'label'=>'จำนวน',
        'pageSummaryOptions'=>['style'=>'display:none'],
    ];     
        
    $columns[] = [
        
        'class' => '\kartik\grid\FormulaColumn',
        'value' => function ($model, $key, $index, $widget) use ($lotSiteKey){
            $p = compact('model', 'key', 'index');
            $price = $widget->col(3, $p);
            $amount = $widget->col(5+2*$lotSiteKey, $p);
            
            return $amount==''||$price==''?'': $price*$amount;
        },
        'format'=>'decimal',
        'label'=>'เป็นเงิน',
        'contentOptions' => function ($model, $key, $index, $widget) use ($lotSiteKey){ 
            $p = compact('model', 'key', 'index');
            $amount = $widget->col(5+2*$lotSiteKey, $p);
            return $amount=='ไม่มีคำขอ'?['class'=>'text-right', 'style'=>'display:none']: ['class'=>'text-right'];
        },
        'pageSummaryOptions'=>[
            'class'=>'gen-vat text-right',  //class gen-vat will be used by 1)genvat.js and 2)budgetwarning.js
            'remaining-budget'=>$lotSite->remainingBudget,
            'warning-id'=>$lotSite->site_id,
            'colspan'=>'2',
        ],
        'pageSummary'=>true,
    ];  
    $beforeHeader[] = [
        'content'=>$lotSite->site->name, 'options'=>['colspan'=>2, 'style'=>'border-left-width:3px;', 'class'=>'text-center Header'.($striped?1:2)]
    ];
}
$columns[] = [
    'attribute'=>'sum_amount', 
    'contentOptions'=>['class'=> 'info'],
    'headerOptions'=>['class'=> 'info']
];
$columns[] = [
    'class'=>'\kartik\grid\ActionColumn',
    'header'=>'ลบ',
    'headerOptions'=>['class'=>'danger'],
    'contentOptions'=>['class'=>'danger'],
    'template'=>'{delete}',
];
$beforeHeader[] = [
    'content'=>'', 'options'=>['colspan'=>1, 'class'=>'text-center info']
];
$beforeHeader[] = [
    'content'=>'', 'options'=>['colspan'=>1, 'class'=>'text-center danger']
];
?>
<div class="request-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= $lotDesc ?> </h3>
    <h4><strong>ประเภท: </strong><?= $lot->itemType->detail ?> </h4>
    
    
    <div class="budget-warning" style="color:red">
        <?php foreach($lotSites as $lotSite){
            echo '<p id="'.$lotSite->site_id.'" style="display:none"><u>ราคาโดยประมาณ</u>ของหน่วยงาน <u>'.
                    $lotSite->site->name.
                    '</u> มีคำขอร้องซื้อสินค้าสูงกว่างบประมาณที่เหลือ งบที่ตั้งไว้ที่จำนวน '.
                    number_format($lotSite->main_budget+$lotSite->etc_budget,2).
                    ' บาท ปัจจุบันคงเหลือ '.  number_format($lotSite->remainingBudget,2).' บาท</p>';
        }?>
    </div>
    <br>
    <div class="container-fluid">
        <div class="row">
            
            <?= Html::a('<span class="glyphicon glyphicon-duplicate"></span> คัดลอกคำขอสั่งซื้อ', ['copy-request-select','lot_id'=>$lot->id], ['class' => 'btn btn-warning col-md-2']) ?>
            <a class="col-md-1"></a>
            <?= Html::a('<span class="glyphicon glyphicon-plus-sign"></span> สร้างคำขอสั่งซื้อสินค้าใหม่', ['create','lot_id'=>$lot->id], ['class' => 'btn btn-success col-md-2']) ?>
            <a class="col-md-1"></a>
            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> แก้ไขคำขอสั่งซื้อสินค้า', ['update','lot_id'=>$lot->id], ['class' => 'btn btn-primary col-md-2']) ?>
            <a class="col-md-1"></a>
            <div class="col-md-2" style="padding:0 0 0 0;">
            <?=
                $this->render('_select-site',['lotSites'=>$lotSites]);
            ?>
            </div>
        </div>
    </div>
</div>

</div><!-- Dummy "end DIV" so the gridview can extend more-->
<div class="container-fluid">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''], //set "(not set)" to space
        'showPageSummary' => true,
        'beforeHeader'=>[
            [
                'columns'=>$beforeHeader,
//                'options'=>['class'=>'skip-export'] // remove this row from export
            ]
        ],
        'columns' => $columns,
        'summary' => 'แสดง <strong>{begin}-{end}</strong> จากทั้งหมด <strong>{totalCount}</strong> รายการ',
        'emptyText' => 'ไม่มีรายการ คำขอสั่งซื้อ',
        'resizableColumns' => false,
        'floatHeader'=>true,
        'perfectScrollbar'=>true,
        'perfectScrollbarOptions'=>[
            'wheelPropagation'=>true,
        ],
        'containerOptions'=>['style'=>'height:600px;'],
        /*'autoXlFormat'=>true,
        'export'=>[
            'fontAwesome'=>true,
            'showConfirmAlert'=>false,
            'target'=>GridView::TARGET_BLANK
        ],
            'panel'=>[
            'type'=>'primary',
            'heading'=>'Products'
        ]*/
    ]); ?>
    <br>
</div>
 <div><!-- Dummy "start DIV" so the gridview can extend more-->
<?php 
$this->registerCssFile('@web/css/StripedHeader.css');
$this->registerJsFile('@web/js/budgetwarning.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/genvat.js', ['depends' => [\yii\web\JqueryAsset::className()]]);

//Reset the perfect scrollbar in grid view. 
$this->registerJs("
    $(window).load(function(){
        $('.ps-container').scrollTop(0).scrollLeft(0);
    });
");
?>