<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\bootstrap\Dropdown;
use kartik\checkbox\CheckBoxX;


$form = ActiveForm::begin([
    'action' => ['create-offer','lot_id'=>count($lotSites)==0?0:$lotSites[0]->lot_id],
    'method' => 'get',
]);

Modal::begin([
    'header' => '<h3>เลือกหน่วยงานที่จะสร้างใบเปรียบเทียบราคา</h3>',
    'toggleButton' => [
        'class' => 'btn btn-info',
        'style' => 'width:100%',
        'label' => '<span class="glyphicon glyphicon-indent-right"></span> สร้างใบเปรียบเทียบราคา',
    ],
]);
?>

<style>
    td,th
    {
        padding:5px 10px 5px 10px;
    }
</style>

<table  class="container-fluid" border="1" width="100%">
    <tr>
        <th>หน่วยงาน</th><th>เลือก</th>
    </tr>
        <?php

        foreach($lotSites as $lotSite)
        {
            echo '<tr><td>';
            echo $lotSite->site->name;
            echo '</td><td>';
            echo CheckboxX::widget([
                'name' => 'Site['.$lotSite->site_id.']',
                'pluginOptions'=>['size'=>'md','threeState'=>false],
                'value' => 1,
            ]);
            echo '</td></tr>';
        }
        ?>
</table>
<br>
<div>
    <p>เลือกที่อยู่สำหรับจัดส่งสินค้า (ตามที่อยู่ของหน่วยงาน): </p>
    <?= Html::dropDownList('shipping_site_id', null, ArrayHelper::map($lotSites,'site_id','site.name'), ['class'=>'form-control'])?>
    
</div>
<br>
<div class="form-group">
    <?=
        Html::submitButton('ไปยังหน้าเลือกร้านค้า', [
            'class' => 'btn btn-info',
            'disabled' => count($lotSites)==0?true:false,
            'title' => 'ไม่พบงบประมาณ กรุณาเพิ่มงบประมาณรายหน่วยงาน อย่างน้อย 1 หน่วยงาน',
        ]) 
    ?>
</div>
<?php
Modal::end();
ActiveForm::end();

?>



