<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Item;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Request */

$lot = $lotSites[0]->lot;
$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;

$this->title = 'แก้ไขคำขอสั่งซื้อ';
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = ['label' => $lotDesc,'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการคำขอสั่งซื้อสินค้า', 'url' => ['index', 'lot_id' => $lot->id]];
$this->params['breadcrumbs'][] = $this->title;

$maxCol = count($lotSites);
?>
<style>
    td,th
    {
        padding:5px 10px 5px 10px;
    }
</style>
<div class="request-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= $lotDesc ?></h3>
    <h4><strong>ประเภท: </strong><?= $lot->itemType->detail ?></h4>

    <div class="request-form">

        <?php $form = ActiveForm::begin(); ?>

        <table class="container">
            <tr>
                <th style="padding:5px 0px 5px 0px;">#</th><th style="min-width:200px;">สินค้า</th>
                <?php
                    for($col=0;$col<$maxCol;$col++)
                    {
                        echo '<th style="min-width:100px;">'.$lotSites[$col]->site->name.'</div>';
                    }
                ?>
            </tr>
        <?php
            $items = Item::getDropDown();
            $i=0;
            for($row=0;$row<$maxRow;$row++)
            {
                echo '<tr><td style="padding:5px 0px 5px 0px;">'.($row+1).'</td>';
                echo '<td>';
                echo $form->field($models[$i], '['.$i.']item_id', ['enableLabel'=>false])->widget(
                        Select2::className(),
                        [
                            'data' => $items,
                            'disabled' => true,
                        ]
                    );
                echo '</td>';
                for($col=0;$col<$maxCol;$col++)
                {
                    echo '<td>';
                    echo $form->field($models[$i], '['.$i.']amount', ['enableLabel'=>false,
                         'inputOptions' => [ 'autocomplete' => 'off', ]])->textInput();
                    $i++;
                    echo '</td>';
                }
                echo '</tr>';
            }
        ?>
        </table>

        <div class="form-group">
            <?= Html::submitButton('บันทึกการแก้ไข', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
