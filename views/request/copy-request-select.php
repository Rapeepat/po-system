<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\ItemType;
use \kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'คัดลอกคำสั่งซื้อสินค้า';
$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = [
        'label' => $lotDesc,
        'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = [
        'label' => 'หน้าจัดการคำขอสั่งซื้อสินค้า',
        'url' => ['index', 'lot_id' => $lot->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="copy-request-select">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'lot_date',
            /*[       
                'attribute'=>'item_type_id',
                'value'=>'itemType.detail',
                'filter'=>ArrayHelper::map(ItemType::find()->all(), 'id', 'detail'),
                'filterInputOptions'=>['class' => 'form-control','prompt'=> '(เลือกชนิดของสินค้า)'],
            ],*/
            [
                'class' => 'kartik\grid\ActionColumn',
                'header' => 'คัดลอก',
                'template' => '{copy}',
                
                'buttons' => [
                    'copy' => function ($url, $model) use ($lot){
                        return Html::a('<span class="glyphicon glyphicon-copy"></span>', ['copy-request','from_lot_id'=>$model->id,'to_lot_id'=>$lot->id], [
                            'title' => Yii::t('app', 'คัดลอกจากงบประมาณนี้'),
                            'data-confirm' => Yii::t('yii', 'กรุณายืนยันการคัดลอก'),
                            'data-method' => 'post',

                        ]);
                    }
                ],
                /*'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'copy') {
                        $url = Yii::$app->controller->createUrl('xx'); // your own url generation logic
                        return $url;
                    }
                }*/

            ],
        ],
        'summary' => 'แสดง <strong>{begin}-{end}</strong> จากทั้งหมด <strong>{totalCount}</strong> รายการ',
    ]); ?>
</div>