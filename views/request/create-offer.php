<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\checkbox\CheckBoxX;
/* @var $this yii\web\View */
/* @var $itemList app\models\Request */
/* @var $supplierList app\models\Supplier */
/* @var $siteList array */

$lot = \app\models\Lot::findOne(['id' => Yii::$app->request->get('lot_id')]);
$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;

$this->title = 'สร้างใบขอเสนอราคา และเปรียบเทียบราคา';
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = ['label' => $lotDesc,'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการคำขอสั่งซื้อสินค้า', 'url' => ['index', 'lot_id' => $lot->id]];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    td,th
    {
        padding:5px 10px 5px 10px;
    }
</style>
<div class="request-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= $lotDesc ?></h3>
    <h4><strong>ประเภท: </strong><?= $lot->itemType->detail ?></h4>
    
    <br>
    <p>เลือกสินค้าเพื่อนำไปสร้างใบเสนอราคา</p>
    <div class="request-form">

        <?php $form = ActiveForm::begin([
            'action' => ['/offer/create','lot_id'=>$lot->id],
        ]); 
        echo Html::hiddenInput('shipping_site_id',$shipping_site_id);
        foreach ($siteList as $site) //siteList is just an array
        {
            echo Html::hiddenInput('Site['.$site.']',$site);
        }
        ?>

        <table class="container" border="1">
            <tr>
                <th style="min-width:200px;">สินค้า</th>
                <?php
                    foreach($supplierList as $supplier)
                    {
                        echo '<th>'.$supplier->name.'</th>';
                    }
                ?>
            </tr>
            
            <tr>
                <th></th>
                <?php
                    foreach($supplierList as $supplier)
                    {
                        echo '<th><p>';
                        echo Html::button('เลือกทั้งหมด',[
                            'name' => 'select-all-'.$supplier->id, //name is used to select checkbox by class
                            'class'=>'btn btn-success',                       //class btn-success will trigger event
                            'style'=>'width:100%;',
                        ]);
                        echo '</p>';
                        echo Html::button('ไม่เลือกทั้งหมด',[
                            'name' => 'select-all-'.$supplier->id, //name is used to select checkbox by class
                            'class'=>'btn btn-danger',                        //class btn-danger will trigger event
                            'style'=>'width:100%;',
                        ]);
                        echo '</th>';
                    }
                ?>
            </tr>
        <?php
            $i=0;
            foreach($itemList as $request) //this $request model is usable only item_id
            {
                echo '<tr><td>'.$request->item->name.'</td>';
                foreach($supplierList as $supplier)
                {
                    echo '<td class="has-success">';
                    echo CheckboxX::widget([
                        'name' => 'Request['.$request->item_id.']['.$supplier->id.']',
                        'pluginOptions'=>['size'=>'md','threeState'=>false],
                        'options'=>['class'=>'select-all-'.$supplier->id], //this class will be triggered
                        'value' => 0,
                    ]);
                    $i++;
                    echo '</td>';
                }
                echo '</tr>';
            }
        ?>
        </table>
        
        <br>
        <div class="form-group">
            <?= Html::submitButton('สร้างใบขอเสนอราคา และเปรียบเทียบราคา', ['class' => 'btn btn-info']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
<?php
$this->registerJsFile('@web/js/offercheckbox.js', ['depends' => [\yii\web\JqueryAsset::className()]]);