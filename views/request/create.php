<?php

use yii\helpers\Html;
use app\models\Item;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Request */

$lot = $models[0]->lot;
$lotDesc = '#'.$lot->id.' งบวันที่ '.$lot->lot_date;

$this->title = 'สร้างคำขอสั่งซื้อสินค้าใหม่';
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการงบประมาณ', 'url' => ['/lot/index']];
$this->params['breadcrumbs'][] = ['label' => $lotDesc,'url' => ['/lot/view', 'id' => $lot->id]];
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการคำขอสั่งซื้อสินค้า', 'url' => ['index', 'lot_id' => $lot->id]];
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    td,th
    {
        padding:5px 10px 5px 10px;
    }
</style>
<div class="request-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3><?= $lotDesc ?></h3>
    <h4><strong>ประเภท: </strong><?= $lot->itemType->detail ?></h4>

    <div class="request-form">

        <?php $form = ActiveForm::begin(); ?>
        
        <table class="container">
            <tr>
                <th style="padding:5px 0px 5px 0px;">#</th><th style="min-width:200px;">สินค้า</th>
                <?php
                    for($j=0;$j<$lotSitesCount;$j++)
                    {
                        echo '<th style="min-width:100px;">'.$models[$j]->site->name.'</th>';
                    }
                ?>
            </tr>
            
            <?php
            $items = Item::getDropdown($models[0]->lot->item_type_id);
            for($repeat=0;$repeat<$maxRepeat;$repeat++)
            {
                $basei = $repeat*$lotSitesCount;
                echo '<tr>';
                echo '<td style="padding:5px 0px 5px 0px;">'.($repeat+1).'</td><td>';
                echo $form->field($models[$basei], '['.$basei.']item_id', ['enableLabel'=>false])->widget(
                        Select2::className(),
                        [    
                            'data' => $items,
                            'options' => ['placeholder' => '(เลือกสินค้า)'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'tags'=>true
                            ],
                        ]
                    ) ;
                echo '</td>';
                for($j=0;$j<$lotSitesCount;$j++)
                {
                    $i = $basei + $j;
                    echo '<td>'.$form->field($models[$i], '['.$i.']amount', [
                        'enableLabel'=>false,
                        'inputOptions' => [ 'autocomplete' => 'off'],
                        ])
                        ->textInput().'</td>';
                }
                echo '</tr>';
            }
            ?>
        </table>
        
        <div class="form-group">
            <?= Html::submitButton('สร้างคำขอสั่งซื้อ', ['class' => 'btn btn-success', 'onclick'=>'return new_item_check();']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
<?php
$this->registerJsFile('@web/js/requestnewitem.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
