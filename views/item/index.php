<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\ItemType;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'หน้าจัดการสินค้า';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="container-fluid"><div class="row">
        <?= Html::a('<span class="glyphicon glyphicon-plus-sign"></span> เพิ่มสินค้าใหม่', ['create'], ['class' => 'btn btn-success col-md-3']) ?>
        <a class="col-md-1"></a>
        <?= Html::a('<span class="glyphicon glyphicon-plus-sign"></span><span class="glyphicon glyphicon-plus-sign"></span> เพิ่มสินค้าหลายชิ้น', ['create-mass'], ['class' => 'btn btn-success col-md-3']) ?>
    </div></div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => 'แสดง <strong>{begin}-{end}</strong> จากทั้งหมด <strong>{totalCount}</strong> รายการ',
        'formatter'=> [ 
            'class' => 'yii\i18n\Formatter',
            'nullDisplay'=>''
        ],
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'name',
            'display_code',
            'unit',
            [
                'attribute'=>'item_type_id',
                'value'=>'itemType.detail',
                'filter'=>ItemType::getDropDown(true),
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'header' => 'จัดการ',
                'deleteOptions' => ['title'=>'ลบสินค้า'],
                'viewOptions' => ['title'=>'ดูสินค้า'],
                'updateOptions' => ['title'=>'แก้ไขสินค้า'],
            ],
        ],
    ]); ?>
</div>
