<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\ItemType;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Item */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'display_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'unit')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'item_type_id')
            ->dropDownList(ItemType::getDropDown(true),
            [
                'prompt' => '(เลือกชนิดของสินค้า สำหรับงบประมาณที่จะสร้าง)',
                //'disabled' => ($model->isNewRecord ? false:true),
            ]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '<span class="glyphicon glyphicon-floppy-open"></span> เพิ่มสินค้า' : '<span class="glyphicon glyphicon-floppy-save"></span> บันทึกการแก้ไข', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
