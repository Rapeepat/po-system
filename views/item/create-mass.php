<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\ItemType;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Item */

$this->title = 'เพิ่มสินค้าหลายชิ้น';
$this->params['breadcrumbs'][] = ['label' => 'หน้าจัดการสินค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    td,th
    {
        padding:5px 10px 5px 10px;
    }
</style>
<div class="item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="item-form">

    <?php $form = ActiveForm::begin(['enableClientValidation'=>false]); ?>
    <table class="container">   
        <tr>
            <th style="padding:5px 0px 5px 0px;">#</th>
            <th>ชื่อสินค้า</th>
            <th>รหัสสินค้า</th>
            <th>หน่วย</th>
            <th>ชนิดสินค้า</th>
        </tr>
    <?php
    foreach($models as $i=>$model)    
    {?>
        <tr>
            <td style="padding:5px 0px 5px 0px;"><?= $i+1 ?></td>
            <td>
                <?= $form->field($model, '['.$i.']name', ['enableLabel'=>false])->textInput(['maxlength' => true])?>
            </td>
            <td>
                <?= $form->field($model, '['.$i.']display_code', ['enableLabel'=>false])->textInput(['maxlength' => true])?>
            </td>
            <td>
                <?= $form->field($model, '['.$i.']unit', ['enableLabel'=>false])->textInput(['maxlength' => true])?>
            </td><td>
                <?= $form->field($model, '['.$i.']item_type_id', ['enableLabel'=>false])
                    ->dropDownList(ItemType::getDropDown(true),
            [
                'prompt' => '(เลือกชนิดของสินค้า สำหรับงบประมาณที่จะสร้าง)',
                'disabled' => ($model->isNewRecord ? false:true),
            ]) ?>
            </td>
        </tr>
    <?php
    }
    ?>
    </table>
        
    <div class="form-group">
        <?=Html::submitButton('<span class="glyphicon glyphicon-floppy-open"></span> เพิ่มสินค้า', ['class' => 'btn btn-success'])?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
